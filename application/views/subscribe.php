<!-- START SLIDER -->
<div id="rev_slider_44_wrapper wave" style="margin-top: 120px;" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 90% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->
                <img src="<?php echo base_url('assets-view/images/voru/subscribe/banner.png'); ?>" alt="" data-bgcolor="#ccc" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="margin-bottom: 80px; margin-top: -120px;">
    <div class="container">
        <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="color: #164B8A; font-size: 30px; font-weight: 700;">Keuntungan Menjadi Membership di VORU</div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/subscribe/ilustrasi/1.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            1. Prioritas dalam Pencarian Beserta Data Statistik</div>
                    </div>
                    <p style="color: black; text-align: justify;">Menyediakan Platform yang mudah digunakan dan dukungan untuk meningkatkan jumlah transaksi penjualan anda.</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/subscribe/ilustrasi/2.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            2. Pembuatan Website Dengan Kualitas Baik</div>
                    </div>
                    <p style="color: black; text-align: justify;">Dengan bergabung sebagai membership, branding perusahaan anda akan terlihat lebih professional dibanding dengan perusahaan lainnya.</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/subscribe/ilustrasi/3.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            3. Informasi Pembelajaran Seputar Komoditas</div>
                    </div>
                    <p style="color: black; text-align: justify;">Dengan bergabung sebagai membership, anda dapat mengikuti webinar yang akan diselenggarakan secara gratis.</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/subscribe/ilustrasi/4.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            4. Informasi Lengkap Seller</div>
                    </div>
                    <p style="color: black; text-align: justify;">Dengan bergabung sebagai membership, anda akan mendapatkan informasi lengkap tentang penjual (Alamat, NPWP, No Telp, dll).</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>

        <div class="title-section" style="margin-top: 50px; margin-bottom: 0px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="color: #164B8A; font-size: 30px; font-weight: 700;">Pilih Paket Membership dan <br> Nikmati Keuntungannya</div>
            </div>
        </div>

        <img src="<?php echo base_url(''); ?>assets-view/images/voru/subscribe/tabel.png" alt="" style="width: 100%; height: auto;">

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-3 full-width" href="<?php echo site_url('B2B/SubscribePayment') ?>">Mulai Berlangganan</a>
            </div>
            <div class="col-lg-4"></div>
        </div>

    </div>
</section>