<section style="padding-top: 130px;">
    <div class=" container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/logos.png" alt="images" style="width: 100%; height: auto; padding: 100px;">
                </div>

                <div class="col-lg-6">
                    <div class="title-section" style="margin-bottom: 25px;">
                        <div class="title-section text-center" style="margin-bottom: 25px;">
                            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Daftar VORU</div>
                        </div>
                    </div>
                    <div class="text-center">
                        Sudah Punya Akun VORU? <a href="<?php echo site_url('Account/masuk') ?>">Masuk</a>
                    </div>
                    <div class="text-center full-width" style="display: flex;">
                        <a class="btn_1 mb_5 mt-2" href="" style="width: 50%; margin: 10px;">Google</a>
                        <a class="btn_1 mb_5 mt-2" href="" style="width: 50%; margin: 10px;">Facebook</a>
                    </div>
                    <form class="mt-3">
                        <div class="sign-in-wrapper">
                            <div class="form-group">
                                <label>Email / No Telp</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label>Sandi / Password</label>
                                <input type="password" class="form-control" name="password" id="password" value="">
                            </div>
                            <div class="clearfix add_bottom_15">
                                <div class="checkboxes float-left">
                                    <label class="container_check">Saya Menyetujui Syarat & Ketentuan
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <input type="submit" value="Daftar" class="btn_1 full-width mb_5 mt-2">
                            <div class="mt-3">
                                Ingin Daftar Menjadi Penjual? <a href="<?php echo site_url('Account/daftar_seller') ?>">Daftar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>