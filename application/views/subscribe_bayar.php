<section style="margin-bottom: 80px; margin-top: 150px;">
    <div class="container">
        <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="color: #164B8A; font-size: 30px; font-weight: 700;">Pembayaran</div>
            </div>
        </div>

        <div class="payment" style="padding: 20px; border: 0.5px solid grey; border-radius: 10px;">
            <p>Total Tagihan</p>
            <p style="color: red; font-size: 20px; font-weight: 600;">Rp. 2.825.000</p>
            <hr>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-8">
                        <p>SUBSCRIPTION PRO MEMBER - Monthly</p>
                        <p>Administrasion 2%</p>
                        <p>Tax 11%</p>
                        <p style="color: blue;">Total Tagihan</p>
                    </div>

                    <div class="col-lg-4">
                        <p>Rp 2.500.000</p>
                        <p>Rp 50.000</p>
                        <p>Rp 275.000</p>
                        <p style="color: blue;">Rp 2.825.000</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="payment mt-3" style="padding: 20px; border: 0.5px solid grey; border-radius: 10px;">
            <p>M-Banking Virtual Account - BCA</p>
            <hr>
            <p>Anda Akan diarahkan menuju halaman ..</p>
            <p>Total Tagihan sudah termasuk biaya service</p>
        </div>

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-5 full-width" href="<?php echo site_url('B2B/SubscribeSuccess') ?>">Sudah Bayar</a>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</section>