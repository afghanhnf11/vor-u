<!-- START SLIDER -->
<div id="rev_slider_44_wrapper wave" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 90% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->
                <img src="<?php echo base_url('assets-view/images/voru/farmer/vorfund.jpg'); ?>" alt="" data-bgcolor="#ccc" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>

                <div class="rev-slider-mask"></div>
                <div class="container">
                    <section class="transparent-head transparent-head-style5">
                        <div class="wrap-transparent">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-6 static" style="text-align: center; margin-top: auto; margin-bottom: auto;">
                                    <div class="pd-lf">
                                        <div class="title" style="color: white;">
                                            VORFUND <br> Voru Founding
                                        </div>
                                        <p class="text" style="color: white;">
                                            Lahan Miliknya Menjadi Solusi Untuk Kami
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/wave.png" alt="images" style="background-size: inherit !important; background-position: inherit !important; margin-top: 32px;">
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="margin-top: -150px; margin-bottom: 80px;">
    <div class="container">
        <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="color: #164B8A; font-size: 30px; font-weight: 700;">Cara Kerja Vorfund</div>
            </div>
            <p style="margin-top: -20px; text-align: center; font-size: 17px; color: black;">Pendanaan ekuitas milik petani yang berfokus pada operasi lahan pertanian dan perternakan. Kami menawarkan peluang memiliki bisnis dengan berinvestasi lahan pertanian dan perternakan dengan petani dan peternak terpercaya.</p>
        </div>

        <div class="col-lg-12">
            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/1.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            1. Pilih Proyek</div>
                    </div>
                    <p style="color: black; text-align: justify;">Pilih lahan dengan program budidaya yang sesuai minat Anda.
                        Setiap lahan budidaya memiliki karateristik imbal hasil maupun resiko yang berbeda-beda. Silahkan mempelajari prospektus masing-masing untuk mendapatkan pemahaman mengenai proyek yang ada.</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            2. Hubungi VORU</div>
                    </div>
                    <p style="color: black; text-align: justify;">Setelah memilih proyek yang diminati, saatnya hubungi VORU!
                        Saat sudah yakin dan menyukasi salah satu lahan dengan program budidaya yang paling sesuai, silahkan hubungi Customer Service VORU untuk mendapatkan syarat dan ketentuan serta kelanjutan selanjutnya.</p>
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/2.png" alt="images">
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/3.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            3. Mulai Modalin</div>
                    </div>
                    <p style="color: black; text-align: justify;">Setelah melalui proses perjanjian, ketentuan dan tanda tangan kontrak selesai, saatnya bekerja!
                        Saat semua syarat dan pertanjian terpenuhi, para pelaku usaha akan memulai aktivitasnya sesuai dengan perjanjian kontrak. Seluruh aktivitas akan diawasi dan setiap informasi, kami juga membuka peluang bahwa kami dapat menjualkan produk yang akan dipanen oleh pelaku usaha!</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            4. Pantau Prosesnya</div>
                    </div>
                    <p style="color: black; text-align: justify;">Seluruh perkembangan yang terjadi di lapangan akan diinformasikan oleh pelaku usaha kepada Anda.
                        Para pelaku usaha yang dipercaya Voru akan memberikan laporan menggunakan format yang sudah disediakan oleh Voru, Anda dapat menerimanya sesuai dengan perjanjian.</p>
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/4.png" alt="images">
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/5.png" alt="images">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            5. Masa Panen & Penjualan</div>
                    </div>
                    <p style="color: black; text-align: justify;">Saat pelaku usaha sudah memasuki masa panen, Voru telah menyiapkan mitra yang akan membeli hasil panen para pelaku usaha.
                        Saat masa panen tiba, Voru menjamin bahwa seluruh hasil panen yang dilakukan oleh pelaku usaha dapat diserap seluruhnya melalui mitra yang sudah bekerja sama dengan Voru dan mendapatkan harga yang sudah disepakati atau dapat menjualnya sendiri.</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                            6. Nikmati Hasilnya</div>
                    </div>
                    <p style="color: black; text-align: justify;">Seluruh perkembangan yang terjadi di lapangan akan diinformasikan oleh pelaku usaha kepada Anda.
                        Para pelaku usaha yang dipercaya Voru akan memberikan laporan menggunakan format yang sudah disediakan oleh Voru, Anda dapat menerimanya sesuai dengan perjanjian.</p>
                </div>
                <div class="col-lg-5">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/6.png" alt="images">
                </div>
                <div class="col-lg-1">
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4" style="margin-top: auto; margin-bottom: auto;">
                    <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/ProyekVorfund') ?>" style="margin-left: auto; margin-right: auto; margin-top: 20px !important;">Lihat Proyeksi</a>
                    <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('Farmers/vorlab') ?>" style="margin-left: auto; margin-right: auto; margin-top: 20px !important;">Halaman Vorlab</a>
                </div>
                <div class="col-lg-4">
                </div>
            </div>

        </div>
    </div>
</section>