<section style="margin-bottom: 80px; margin-top: 150px;">
    <div class="container">

        <div class="payment" style="padding: 20px; border: 0.5px solid grey; border-radius: 10px; text-align: center;">
            <img class="mb-3" src="<?php echo base_url(''); ?>assets-view/images/voru/footer.png" alt="" style="width: 20%; height: auto;">
            <p style="font-size: 30px; font-weight: 700;">ASIK, PEMBAYARANMU BERHASIL!</p>
        </div>

        <div class="payment mt-3" style="padding: 20px; border: 0.5px solid grey; border-radius: 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-8">
                        <p>Total Pembelian</p>
                        <p>Ringkasan Pembayaran</p>
                        <p>SUBSCRIPTION PRO MEMBER - Monthly</p>
                        <p>Administrasion 2%</p>
                        <p>Tax 11%</p>
                        <p>Metode Pembayaran</p>
                    </div>

                    <div class="col-lg-4">
                        <p style="color: red;">Rp 2.825.000</p>
                        <p>-</p>
                        <p>Rp 2.500.000</p>
                        <p>Rp 50.000</p>
                        <p>Rp 275.000</p>
                        <p>Virtual Account - BCA</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <a class="btn_1 mt-5 full-width" href="<?php echo site_url('B2B') ?>">Belanja Lagi</a> <a class="btn_1 mt-3 full-width" href="">Print Kwitansi</a>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</section>