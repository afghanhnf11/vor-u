<!-- START SLIDER -->
<div id="rev_slider_44_wrapper" class="rev_slider_wrapper">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 50% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->

                <div class="container">
                    <section class="transparent-head transparent-head-style5">
                        <div class="wrap-transparent">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-5 static">
                                    <div class="pd-lf" style="margin-top: 20px;">
                                        <div class="title">
                                            Tingkatkan Derajat Komunitas Bersama VORU
                                        </div>
                                        <p class="text">
                                            SuperApp Commodity Terbesar di Indonesia
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-7 static">
                                    <div class="pd-rg">
                                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/slider.png" alt="images" style="width: 100%; border-radius: 10px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="padding-top: 40px; padding-bottom: 80px; margin-top: -170px;">
    <div class="container">
        <div class="container">
            <div class="title-section" style="margin-bottom: 5px;">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Explore Use Here</div>
                </div>
            </div>
            <!-- /main_title -->
            <div class="col-lg-12">
                <div class="flat-benefit-style2 clearfix">
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="<?php echo site_url('Farmers'); ?>">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/logoheader.png" alt="images" style="height: auto; width: 100%; margin-left: auto; margin-right: auto;">
                                    <h3 style="font-weight: 600; margin-top: 20px; font-size: 18px; color: #176004;">For Farmers</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="<?php echo site_url('B2B'); ?>">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/logoheader.png" alt="images" style="height: auto; width: 100%; margin-left: auto; margin-right: auto;">
                                    <h3 style="font-weight: 600; margin-top: 20px; font-size: 18px; color: #164B8A;">For Business</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                </div>
            </div>

            <div class="title-section" style="margin-top: 70px; margin-bottom: 30px;">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">About VORU for Business</div>
                </div>
                <p style="margin-top: -20px; color: black; text-align: center; font-size: 17px;">VORU for Business adalah fitur yang memberikan kemudahan proses transaksi melalui fitur teknologi dan fitur yang dapat membantu pelaku komoditas lebih mudah dan memberikan keuntungan yang maksimal. Voru menjadi jembatan bagi pembeli baik untuk transaksi dalam jumlah grosir dari ratusan supplier terbaik dan terpercaya.</p>
            </div>

            <div class="col-lg-12">
                <div class="flat-benefit-style2 clearfix">
                    <div class="row">
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/1.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/2.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/3.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/4.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/5.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/6.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/7.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col">
                            <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                                <a href="#">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/8.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Our Service</div>
                </div>
                <p style="margin-top: -20px; color: black; text-align: center; font-size: 17px;">VORU for Business memberikan pelayanan untuk setiap pelaku usaha seperti PT, CV, UD, Koperasi dan badan usaha lainnya yang bergerak dibidang komoditas untuk melakukan penjualan langsung maupun secara lelang, VORU juga memberikan layanan pembayaran digital serta layanan pengiriman logistik dengan menggunakan partner kerja kami yang dapat dipercaya.</p>
            </div>

            <div class="col-lg-12">

                <div class="row mb-4">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/b2b.png" alt="images">
                    </div>
                    <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                        <div class="title-section" style="margin-bottom: 20px;">
                            <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                                B2B</div>
                        </div>
                        <p style="color: black; text-align: justify;">Business to business merupakan salah satu cara untuk melakukan penjualan antar penjual dan penyedia barang, VORU hadir untuk menjembatani kedua belah pihak yang ingin bertransaksi semua produk komoditas serta alat untuk komoditas dengan kuantitas besar</p>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                        <div class="title-section" style="margin-bottom: 20px;">
                            <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                                Logistics</div>
                        </div>
                        <p style="color: black; text-align: justify;">Setiap logistik yang berpartner dengan kami sudah memiliki asuransi dan kami sudah memastikan bahwa semua partner dapat dipercaya mulai dari awal pengiriman hingga produk yang ditransaksikan sampai ketempat tujuan. Pengiriman logistik kami beroperasi udara, laut, dan daratan.</p>
                    </div>
                    <div class="col-lg-5">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/logistic.png" alt="images">
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="row mb-5">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/payment.png" alt="images">
                    </div>
                    <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                        <div class="title-section" style="margin-bottom: 20px;">
                            <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                                Payment</div>
                        </div>
                        <p style="color: black; text-align: justify;">Pembayaran yang dilakukan didalam website VORU dijamin aman dan di rahasiakan, setiap transaksi yang dilakukan sudah teregulasi lengkap dan diawasi oleh badan keuangan pemerintah. Setiap partner pembayaran digital kami, sangat dapat dipercaya dan memiliki reputasi tinggi.</p>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-1">
                    </div>

                    <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                        <div class="title-section" style="margin-bottom: 20px;">
                            <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                                VORU E-Commerce</div>
                        </div>
                        <p style="color: black; text-align: justify;">VORU E-Commerce merupakan layanan berbasis Superapp yang dapat menjembatani penjual dan penyedia barang, mengenal lebih jauh tentang, membuat lelang, serta membuat pendanaan bagi petani atau peternak yang membutuhkan modal kerja dan telah diawasi oleh badan pemerintahan.</p>
                    </div>
                    <div class="col-lg-5">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/mackbook.png" alt="images">
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>
            </div>
        </div>
        <!-- /container -->
</section>

<style>
    /* The Close Button */
    .closes {
        color: white;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .closes:hover,
    .closes:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 425px) {
        .container-popup {
            margin-top: 50px !important;
        }

        .container-popup2 {
            margin-top: 50px !important;
        }

        .container-popupg {
            margin-top: 100px !important;
        }

        .imgt {
            width: 30%;
            height: auto;
            object-fit: cover;
            border-radius: 5px;
        }

        /* Modal Content */
        .modal-contentt {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 90%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }
    }

    #close {
        background-color: rgba(64, 68, 65, 0.5);
        position: fixed;
        z-index: 999999;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    #close:target {
        -webkit-transition: all 1s;
        -moz-transition: all 1s;
        transition: all 1s;
        opacity: 0;
        visibility: hidden;
    }

    @media (min-width: 768px) {
        .container-popup {
            width: 35%;
        }

        .container-popup2 {
            width: 65%;
        }

        .container-popupg {
            width: 65%;
        }
    }

    @media (max-width: 767px) {
        .container-popup {
            width: 90%;
        }

        .container-popup2 {
            width: 90%;
        }

        .tab button {
            width: 50%;
        }

        .container-popupg {
            width: 80%;
        }
    }

    .container-popup {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popup img {
        width: 100%
    }

    .container-popup2 {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popup2 img {
        width: 100%
    }

    .container-popupg {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popupg img {
        width: 100%
    }

    .close {
        position: absolute;
        top: 3px;
        right: 3px;
        background-color: #33898B;
        padding: 7px 10px;
        font-size: 15px;
        text-decoration: none;
        line-height: 1;
        color: #fff;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 2s;
        animation-name: fade;
        animation-duration: 2s;
    }

    @-webkit-keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

    @keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }
</style>

<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/plugins.js"></script>

<script type="text/javascript">
    $(function() {
        $('a[href="#light-box"]').on('click', function(event) {
            event.preventDefault();
            $('#light-box').addClass('open');
            $('#light-box > form > input[type="light-box"]').focus();
        });

        $('#light-box, #light-box button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    });

    // init Isotope
    var portfolioGrid = $('#portfolio-grid');

    portfolioGrid.imagesLoaded(function() {
        portfolioGrid.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows',
            "masonry": {
                "columnWidth": ".portfolio-grid-sizer"
            }
        });
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };

    // bind filter button click
    $('#projects-filter').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        portfolioGrid.isotope({
            filter: filterValue
        });
        return false;
    });

    // change is-checked class on buttons
    $('#projects-filter').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // GALERI
    $('#galleryModal').on('show.bs.modal', function(e) {
        $('#galleryImage').attr("src", $(e.relatedTarget).data("large-src"));
    });
</script>