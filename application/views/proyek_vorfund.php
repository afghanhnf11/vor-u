<section style="margin-bottom: 50px; margin-top: 150px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                <a href="" class="mb-3" style="font-size: 16px;"><i class="fa fa-arrow-left" style="color: #369967;"></i>&nbsp; Kembali</a>
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9 mb-3" style="font-weight: 700; font-size: 35px; color: #164B8A;">
                        Proyek Vorfund</div>
                </div>
                <p style="color: black;">Setiap proyek yang Anda investasikan, turut membantu kesejahteraan petani, peternak dan perbaikan alam.</p>
                <p style="color: black;"><i class="fa fa-check-circle" style="color: #369967;"></i>&nbsp; Pemberdayaan Pelaku Komoditas</p>
                <p style="color: black;"><i class="fa fa-check-circle" style="color: #369967;"></i>&nbsp; Menguntungkan untuk Anda dan Pelaku Komoditas</p>
                <p style="color: black;"><i class="fa fa-check-circle" style="color: #369967;"></i>&nbsp; Aman dan terpercaya</p>
                <p style="color: black;"><i class="fa fa-check-circle" style="color: #369967;"></i>&nbsp; Berdampak positif bagi lingkungan</p>
            </div>

            <div class="col-lg-7" style="margin-top: auto; margin-bottom: auto;">
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/slide.jpg" alt="images" style="width: 100% !important; height: 400px; object-fit: cover; border-radius: 10px;">
            </div>
        </div>

        <div class="title-section" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">List Proyek Vorfund</div>
            </div>
        </div>

        <div class="row latest-blog latest-blog-type2 latest-blog-style2" style="padding-top: 20px;">
            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/1.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Pertanian Jagung Pak Udin ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Kabupaten Tanah Laut, Kalimantan Selatan</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/2.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Perkebunan Sawit Pak Anto ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Tanah Grogot, Kalimantan Timur</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/3.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Peternakan Sapi Kang Syarip ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Batubadak, Lampung Timur</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/4.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Perkebunan Cabai Pak Asep ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Banyuwangi Selatan, Jawa Timur</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/5.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Perkebunan Jahe Pak Wowo ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Cianjur Selatan, Jawa Barat</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/6.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Pertanian Bawang Pak Didan ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Solok, Sumatera Barat</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/7.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Pertanian Kacang Tanah Pak Wawan ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Lombok Barat, Nusa Tenggara barat</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/8.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Budidaya Lobster Air Tawar Pak Ilham ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Kabupaten Tanggamus, Lampung Utara</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-4 col-md-4">
                <article class="post post-style2 box-shadow-type1">
                    <div class="featured-post">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/9.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                    </div>
                    <div class="post-content" style="padding: 10px;">
                        <div class="post-title" style="margin: 10px 0px;">
                            <h5>
                                <a href="<?php echo site_url('B2B/DetailVorfund') ?>" style="font-size: 20px !important;">Budidaya Ikan Nila Pak Rizal ...</a>
                            </h5>
                        </div>
                        <div class="category" style="width: 250px;">
                            <a href=""><i class="fa fa-map-pin"></i>&nbsp; Tomohon, Sulawesi Selatanr</a>
                        </div>
                        <div class="post-link mt-2 mb-2">
                            <a href="<?php echo site_url('B2B/DetailVorfund') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="pagination_fg">
            <a href="#">&laquo;</a>
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">&raquo;</a>
        </div>
    </div>
</section>