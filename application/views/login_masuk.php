<section style="padding-top: 130px; padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6" style="border: solid grey; padding: 30px;">
                <div class="title-section" style="margin-bottom: 25px;">
                    <div class="title-section text-center" style="margin-bottom: 25px;">
                        <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Masuk VORU</div>
                    </div>
                </div>
                <form class="mt-3">
                    <div class="sign-in-wrapper">
                        <div class="form-group">
                            <label>Email / No Telp</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label>Sandi / Password</label>
                            <input type="password" class="form-control" name="password" id="password" value="">
                        </div>

                        <p style="text-align: center;">Atau Bisa Masuk Dengan</p>
                        <div class="text-center full-width" style="display: flex;">
                            <a class="btn_1 mb_5 mt-2" href="" style="width: 50%; margin: 10px;">Google</a>
                            <a class="btn_1 mb_5 mt-2" href="" style="width: 50%; margin: 10px;">Facebook</a>
                        </div>

                        <input type="submit" value="Masuk" class="btn_1 full-width mb_5 mt-2">
                        <div class="mt-3">
                            Mengalami Kendala? <a href="">Ngobrol dengan CS VORU</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3"></div>
        </div>

</section>