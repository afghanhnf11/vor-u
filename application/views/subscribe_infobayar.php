<section style="margin-bottom: 80px; margin-top: 150px;">
    <div class="container">
        <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="color: #164B8A; font-size: 30px; font-weight: 700;">Metode Pembayaran</div>
            </div>
        </div>

        <div class="payment">
            <button class="accordion">BRI</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">BCA</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
                <p>M-Banking (BCA Virtual Account)
                    Periksa kembali data pembayaran Anda sebelum melanjutkan transaksi
                    Anda akan mendapatkan kode pembayaran BCA Virtual Accpunt yang akan digunakan untuk membayar transaksi ini
                    Silahkan periksa Nomor Handphone yang terdaftar lalu lanjutkan ke BCA Virtual Account

                    Klik BCA
                    Masuk ke situs www.klikbca.com, masukkan User ID dan Password Anda.
                    Pilih Menu Pembayaran e-Commerce, lalu pilih kategori Marketplace, pilih pilihan Voru, dan klik Lanjutkan</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">MANDIRI</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">BNI</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">CIMB NIAGA</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">PERMATA BANK</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">OVO</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>

            <button class="accordion" style="margin-top: 20px;">GOPAY</button>
            <div class="panel">
                <p>Cara Pembayaran</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-5 full-width" href="<?php echo site_url('B2B/SubscribePay') ?>">Mulai Berlangganan</a>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</section>

<style>
    .accordion {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    .active,
    .accordion:hover {
        background-color: #ccc;
    }

    .panel {
        padding: 0 18px;
        display: none;
        background-color: white;
        overflow: hidden;
    }
</style>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
</script>