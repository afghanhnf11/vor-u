<section style="margin-bottom: 50px; margin-top: 150px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-7" style="margin-top: auto; margin-bottom: auto;">
                <a href="" class="mb-3" style="font-size: 16px;"><i class="fa fa-arrow-left" style="color: #369967;"></i>&nbsp; Kembali</a>
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9 mb-3" style="font-weight: 700; font-size: 35px; color: #164B8A;">
                        Pertanian Jagung Pak Udin</div>
                </div>
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/1.png" alt="images" style="width: 100% !important; height: 300px; object-fit: cover; border-radius: 10px;">
                <div class="row mt-4 text-center" style="border: 0.5px solid grey; padding: 20px; border-radius: 10px; margin: 3px 0px;">
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Penerima Pendanaan</h3>
                        <p style="color: black;">Pak Bachrudin</p>
                    </div>
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Estimated Return</h3>
                        <p style="color: black;">20% Per Panen</p>
                    </div>
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Luas Lahan</h3>
                        <p style="color: black;">8 Hektare</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                <a class="btn_1 mb-4 mt-5" href="" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">Chat Customer Service Voru</a>
                <div class="card mt-1" style="padding: 10px; border-radius: 10px; border: 0.5px solid grey;">
                    <div class="carousel-hobby carousel-hobby-style4">
                        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="1" data-column2="1" data-column3="1" data-column4="1" data-dots="false" data-auto="true" data-nav="true">
                            <div class="owl-carousel">
                                <a href="#">
                                    <div class="imagebox-hobby">
                                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/1.png" alt="images" style="border-radius: 10px;">
                                    </div>
                                </a>

                                <a href="#">
                                    <div class="imagebox-hobby">
                                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/1.png" alt="images" style="border-radius: 10px;">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title-section mt-4" style="margin-bottom: 20px;">
                <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 25px; color: #164B8A;">
                    Deskripsi Proyek</div>
            </div>

            <div class="deskripsi" style="padding: 20px; border: 0.5px solid grey; border-radius: 10px;">
                <p style="color: black;">Salah satu komoditi pertanian yang saat ini sedang naik daun adalah tanaman jagung. Tanaman ini belakangan menjadi primadona pertanian karena memiliki nilai eksport yang sangat tinggi. Hal ini terjadi dikarenakan permintaan yang cukup tinggi dari beberapa negara Asia seperti China, Jepang, Korea, Taiwan, dan sebagainya. Banyak para petani yang beralih membudidayakan tanaman jagung tersebut khusus nya di daerah Tanah Laut, Kalimantan Selatan, hal ini dilakukan pelaku usaha semata – mata untuk meningkatkan kesejahteraan.

                    Perlu diketahui, awal 2021, pemerintah melalui Kementerian Pertanian (Kementan) memastikan ketersediaan komoditas pangan mencukupi kebutuhan nasional. Salah satunya jagung untuk pakan ternak terus digenjot produksinya sehingga pasokannya aman atau bahkan mencapai surplus untuk ekspor.

                    Direktur Jenderal Tanaman Pangan Kementerian Pertanian (Kementan) Suwandi, saat itu, mengatakan bahwa beberapa sentra produksi jagung sudah bisa mencapai target produktivitas 8 hingga 9 ton per hektare. Peningkatan produktivitas dapat menjamin tercukupinya kebutuhan jagung.

                    Bagi masyarakat yang tertarik dalam pengembangan budidaya jagung ini dapat berkontribusi dengan memberikan dana untuk investasi di semua atau sebagian lahan pelaku usaha. Pendanaan digunakan sebagai modal untuk pelaku usaha seperti pupuk, bibit, air ataupun operasional lainnya yang menunjang pelaku usaha dari awal membangun usaha sampai menjual hasil panen usaha sesuai kontrak yang tertera.</p>
            </div>

            <div class="title-section mt-4" style="margin-bottom: 20px;">
                <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 25px; color: #164B8A;">
                    Garis Waktu Investasi</div>
            </div>

            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/invest.png" alt="">

            <div class="col-lg-12">
                <div class="row mt-4 text-center" style="border: 0.5px solid grey; padding: 20px; border-radius: 10px; margin: 3px 0px;">
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Estimasi Panen Pertahun</h3>
                        <p style="color: black;">2 - 4 Kali</p>
                    </div>
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Estimasi Modal Dibutuhkan</h3>
                        <p style="color: black;">Rp. 1.750.000.000 - 2.500.000.000</p>
                    </div>
                    <div class="col-lg-4">
                        <h3 style="color: #5BC08A; font-weight: bold;">Estimasi Lama Kontrak</h3>
                        <p style="color: black;">3 Tahun</p>
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                        <a class="btn_1 mb_5 mt-5" href="" style="margin-left: auto; margin-right: auto; margin-top: 20px !important;">Lihat Prospektus Lengkap</a>
                        <a class="btn_1 mb_5 mt-5" href="" style="margin-left: auto; margin-right: auto; margin-top: 20px !important;">Chat CS Voru</a>
                    </div>
                    <div class="col-lg-3">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/plugins.js"></script>

<script type="text/javascript">
    $(function() {
        $('a[href="#light-box"]').on('click', function(event) {
            event.preventDefault();
            $('#light-box').addClass('open');
            $('#light-box > form > input[type="light-box"]').focus();
        });

        $('#light-box, #light-box button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    });

    // init Isotope
    var portfolioGrid = $('#portfolio-grid');

    portfolioGrid.imagesLoaded(function() {
        portfolioGrid.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows',
            "masonry": {
                "columnWidth": ".portfolio-grid-sizer"
            }
        });
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };

    // bind filter button click
    $('#projects-filter').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        portfolioGrid.isotope({
            filter: filterValue
        });
        return false;
    });

    // change is-checked class on buttons
    $('#projects-filter').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // GALERI
    $('#galleryModal').on('show.bs.modal', function(e) {
        $('#galleryImage').attr("src", $(e.relatedTarget).data("large-src"));
    });
</script>