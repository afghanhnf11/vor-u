<!-- START SLIDER -->
<div id="rev_slider_44_wrapper wave" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 90% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->
                <img src="<?php echo base_url('assets-view/images/voru/farmer/slider.jpg'); ?>" alt="" data-bgcolor="#ccc" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>

                <div class="rev-slider-mask"></div>
                <div class="container">
                    <section class="transparent-head transparent-head-style5">
                        <div class="wrap-transparent">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-6 static" style="text-align: center; margin-top: auto; margin-bottom: auto;">
                                    <div class="pd-lf">
                                        <img src="<?php echo base_url('assets-view/images/voru/logoheader.png'); ?>" alt="" style="width: 40%; margin-bottom: 5px;">
                                        <div class="title" style="color: white;">
                                            FARMERS
                                        </div>
                                        <p class="text" style="color: white;">
                                            Tingkat Kebutuhan Pangan Tidak Akan Turun, Kami Siap Tumbuh Bersama
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/wave.png" alt="images" style="background-size: inherit !important; background-position: inherit !important; margin-top: 32px;">
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="margin-top: -90px; margin-bottom: 80px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6" style="margin-right: auto; margin-left: auto;">
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/1.png" alt="images">
            </div>
            <div class="col-lg-6" style="margin-top: auto; margin-bottom: auto;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                        Merealisasikan setiap produk yang dihasilkan From you, Vor-U</div>
                </div>
                <a class="btn_1 mb_5 mt-2 full-width" href="<?php echo site_url('Farmers/vorlab'); ?>">Lihat Solusi</a>
            </div>
        </div>
        <p style="color: black; text-align: center; margin-top: 30px;">Setiap pelaku usaha petani maupun peternak memiliki peluang yang sama untuk dapat merasakan kesuksesan, jerih payah dan keringat yang dikeluarkan menjadi titik dimana VORU bergerak untuk dapat meningkatkan derajat komoditas di seluruh Indonesia dengan mempertemukan pelaku usaha dengan yang membutuhkannya secara baik.</p>

        <div class="title-section" style="margin-top: 60px; margin-bottom: 30px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">PARTNERSHIP AND COLLABORATION IS THE KEY TO SUCCESS <br> Meet Our Partner</div>
            </div>
            <p style="margin-top: -20px; color: black; text-align: center; font-size: 17px;">VORU for Business adalah fitur yang memberikan kemudahan proses transaksi melalui fitur teknologi dan fitur yang dapat membantu pelaku komoditas lebih mudah dan memberikan keuntungan yang maksimal. Voru menjadi jembatan bagi pembeli baik untuk transaksi dalam jumlah grosir dari ratusan supplier terbaik dan terpercaya.</p>
        </div>

        <div class="col-lg-12">
            <div class="flat-benefit-style2 clearfix">
                <div class="row">
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/1.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/2.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/3.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/4.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/5.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/6.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/7.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center; border: solid #C3C3C3; padding: 20px; border-radius: 15px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/welcomepage/bisnis/8.png" alt="images" style="height: 30px; width: 100%; margin-left: auto; margin-right: auto; object-fit: contain;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('Account'); ?>" style="width: 30%; margin-left: 35%; margin-right: auto;">Bergabung Sekarang</a>

        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-6" style="margin-top: auto; margin-bottom: auto;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px; color: #164B8A;">
                        BUMDES siap membantu untuk meningkatkan perekonomian
                        VORU siap menjadi tangan kanan</div>
                </div>
            </div>
            <div class="col-lg-6" style="margin-right: auto; margin-left: auto;">
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/2.png" alt="images">
            </div>
        </div>

    </div>
</section>

<div class="container mb-5">
    <div class="title-section" style="margin-top: 70px; margin-bottom: 30px;">
        <div class="title-section text-center">
            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Fitur App</div>
        </div>
    </div>
</div>

<section class="mt-5">
    <div class="bg" style="background: #589442 url('assets-view/images/voru/farmer/fitur.png') no-repeat center center; height: 450px;">
        <div class="bgimages" style="background: rgba(15, 97, 55, 0.6); height: 450px;">
            <div class="container">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4" style="margin-top: 4%; margin-bottom: auto; text-align: center; padding: 0px 30px;">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/icon/1.png" alt="images">
                            <h3 style="font-weight: bold; color: white; font-size: 30px; margin-top: 10px; margin-bottom: 10px;">VORMERS</h3>
                            <p style="color: white;">VORU membantu para petani dan peternak memiliki toko di
                                e-commerce VORU dan dapat melakukan transaksi dalam volume besar sehingga bisa mendapatkan keuntungan yang besar</p>
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">Menuju VORMERS</a>
                        </div>

                        <div class="col-lg-4" style="margin-top: 4%; margin-bottom: auto; text-align: center; padding: 0px 30px;">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/icon/2.png" alt="images">
                            <h3 style="font-weight: bold; color: white; font-size: 30px; margin-top: 10px; margin-bottom: 10px;">VORFUND</h3>
                            <p style="color: white;">VORU meberikan fitur layanan untuk para petani dan peternak yang membutuhkan modal untuk usaha yang dimana terdapat keuntungan untuk kedua belah pihak yang mengikuti.</p>
                            <a class="btn_1 mb_5 mt-5" href="" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">Chat Mimin Voru</a>
                        </div>

                        <div class="col-lg-4" style="margin-top: 4%; margin-bottom: auto; text-align: center; padding: 0px 30px;">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/icon/3.png" alt="images">
                            <h3 style="font-weight: bold; color: white; font-size: 30px; margin-top: 10px; margin-bottom: 10px;">VORLAB</h3>
                            <p style="color: white;">VORU bekerjasama dengan berbagai perusahaan diberbagai sektor yang dimana kolaborasi ini dilakukan untuk perusahaan tersebut mau untuk bertransaksi dengan mitra petani dan peternak yang terdaftar di VORU.</p>
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('Farmers/vorlab'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">Lihat Solusi</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/plugins.js"></script>

<script type="text/javascript">
    $(function() {
        $('a[href="#light-box"]').on('click', function(event) {
            event.preventDefault();
            $('#light-box').addClass('open');
            $('#light-box > form > input[type="light-box"]').focus();
        });

        $('#light-box, #light-box button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    });

    // init Isotope
    var portfolioGrid = $('#portfolio-grid');

    portfolioGrid.imagesLoaded(function() {
        portfolioGrid.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows',
            "masonry": {
                "columnWidth": ".portfolio-grid-sizer"
            }
        });
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };

    // bind filter button click
    $('#projects-filter').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        portfolioGrid.isotope({
            filter: filterValue
        });
        return false;
    });

    // change is-checked class on buttons
    $('#projects-filter').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // GALERI
    $('#galleryModal').on('show.bs.modal', function(e) {
        $('#galleryImage').attr("src", $(e.relatedTarget).data("large-src"));
    });
</script>

<style>
    #wave {
        position: relative;
        height: 70px;
        width: 600px;
        background: #e0efe3;
    }

    #wave:before {
        content: "";
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 340px;
        height: 80px;
        background-color: white;
        right: -5px;
        top: 40px;
    }

    #wave:after {
        content: "";
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 300px;
        height: 70px;
        background-color: #e0efe3;
        left: 0;
        top: 27px;
    }

    .bannerwave {
        height: 100% !important;
        width: 100%;
        position: absolute;
        left: 0;
        top: 32px;
        bottom: 0;
        background-repeat: no-repeat;
    }
</style>