<footer id="footer" class="footer-type2">
    <div id="footer-widget" style="margin-bottom: 10px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo-footer">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/footer.png" alt="images">
                    </div>
                </div>
                <div class="col-lg-8">
                    <p style="color: black; font-size: 15px; text-align: justify;">Voru adalah Online Marketplace berbasis Superapp yang memberikan kemudahan proses transaksi melalui fitur teknologi dan fitur yang dapat membantu pelaku komoditas lebih mudah dan memberikan keuntungan yang maksimal. Voru menjadi jembatan bagi pembeli baik untuk transaksi dalam jumlah grosir maupun eceran dari ratusan supplier terbaik dan terpercaya.</p>
                </div>

                <div class="col-lg-2 col-link" style="text-align: center;">
                    <h3 class="widget widget-title" style="color: #164b8a !important;">
                        Ikuti Kami
                    </h3>
                    <ul class="widget-social-media" style="margin-top: 20px;">
                        <li><a href=""><i class="fa fa-facebook" aria-hidden="true" style="color: #164b8a !important;"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram" aria-hidden="true" style="color: #164b8a !important;"></i></a></li>
                        <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true" style="color: #164b8a !important;"></i></a></li>
                    </ul>
                </div>
            </div>
            <hr style="color: black; margin-top: 0px;">
            <div class="row">
                <div class="col-lg-4">
                    <p style="color: black; font-weight: 500;">Metode Pembayaran</p>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/1.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/2.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/3.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/4.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/5.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/66.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/7.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/8.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/9.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                        <div class="col-lg-2-5">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pembayaran/10.png" alt="images" style="width: 100%; height: auto; padding: 5px;">
                        </div>
                    </div>

                    <p style="color: black; margin-top: 20px; font-weight: 500;">Metode Pengiriman</p>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-2">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pengiriman/1.png" alt="images" style="width: 100%; height: auto;">
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pengiriman/2.png" alt="images" style="width: 100%; height: auto;">
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pengiriman/3.png" alt="images" style="width: 100%; height: auto;">
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pengiriman/4.png" alt="images" style="width: 100%; height: auto;">
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/pengiriman/5.png" alt="images" style="width: 100%; height: auto;">
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-link">
                    <p style="color: black; font-weight: 500;">
                        Informasi
                    </p>
                    <ul class="widget-nav-menu">
                        <li><a href="#">Bantuan</a></li>
                        <li><a href="#">Berita</a></li>
                        <li><a href="#">Pengiriman</a></li>
                        <li><a href="#">Subscription</a></li>
                        <li><a href="#">Syarat & Ketentuan</a></li>
                        <li><a href="#">Tentang Kami</a></li>
                    </ul>
                </div>

                <div class="col-lg-2 col-link">
                    <p style="color: black; font-weight: 500;">
                        Pembeli
                    </p>
                    <ul class="widget-nav-menu">
                        <li><a href="#">Cara Belanja</a></li>
                        <li><a href="#">Garansi Pembeli</a></li>
                        <li><a href="#">Lacak Pengiriman</a></li>
                        <li><a href="#">Lihat Promo</a></li>
                        <li><a href="#">Metode Pembayaran</a></li>
                    </ul>
                </div>

                <div class="col-lg-2 col-link">
                    <p style="color: black; font-weight: 500;">
                        Penjual
                    </p>
                    <ul class="widget-nav-menu">
                        <li><a href="#">Cara Berjualan</a></li>
                        <li><a href="#">Kebijakan Mitra</a></li>
                        <li><a href="#">Mitra B2B</a></li>
                    </ul>
                </div>

                <div class="col-lg-2 col-link">
                    <p style="color: black; font-weight: 500;">
                        Hubungi Kami
                    </p>
                    <ul class="widget-nav-menu">
                        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> &nbsp; Voru Whatsapp 1</a></li>
                        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> &nbsp; Voru Whatsapp 2</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div id="bottom" class="bottom-type2 clearfix has-spacer">
        <div id="bottom-bar-inner" class="container">
            <div class="bottom-bar-inner-wrap">
                <div class="bottom-bar-content">
                    <div id="copyright" style="color: WHITE !important;">
                        ©
                        <span class="text-year" style="color: WHITE !important;">
                            <?php echo date("Y"); ?>
                        </span>
                        <span class="text-name" style="color: WHITE !important;">
                            VORU 2022
                        </span>
                    </div>
                </div>
                <div class="bottom-bar-menu">
                    <ul class="bottom-nav">
                        <li class="menu-item"><a href="<?php echo site_url('Home/Profil'); ?>" style="color: WHITE !important;">Profil</a></li>
                        <li class="menu-item"><a href="#" style="color: WHITE !important;">Kebijakan Keamanan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer -->

<a href="" class="float" style="color: white;" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>

<div class="label-container">
    <div class="label-text">Informasi Selengkapnya</div>
    <i class="fa fa-play label-arrow"></i>
</div>

<style>
    .label-container {
        position: fixed;
        bottom: 35px;
        right: 95px;
        display: table;
        visibility: hidden;
        z-index: 99999999;
    }

    .label-text {
        color: #FFF;
        font-size: 15px;
        background: rgba(51, 51, 51, 0.5);
        display: table-cell;
        vertical-align: middle;
        padding: 10px;
        border-radius: 3px;
        z-index: 99999999;
    }

    .label-arrow {
        display: table-cell;
        vertical-align: middle;
        color: #333;
        opacity: 0.5;
        z-index: 99999999;
    }

    .float {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 30px;
        right: 30px;
        background-color: #76c99f;
        color: white;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
        z-index: 99999999;
    }

    .my-float {
        font-size: 25px;
        margin-top: 18px;
    }

    a.float+div.label-container {
        visibility: hidden;
        opacity: 0;
        transition: visibility 0s, opacity 0.5s ease;
    }

    a.float:hover+div.label-container {
        visibility: visible;
        opacity: 1;
    }
</style>