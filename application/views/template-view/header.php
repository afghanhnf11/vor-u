<div class="wrap-header">
    <div class="flat-header flat-header-style2">
        <div class="top-bar clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <ul class="information">
                            <li class="youtube">
                                <a href="" class="topp"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                            </li>
                            <li class="facebook">
                                <a href="" class="topp"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li class="twitter">
                                <a href="" class="topp"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li class="instagram">
                                <a href="" class="topp"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <ul class="nav-sing">
                            <li><a href="" target="_blank" class="login"> <i class="fa fa-whatsapp" aria-hidden="true"></i> &nbsp; Voru CS WhatsApp 1</a></li>
                            <li><a href="" target="_blank" class="login">Voru CS WhatsApp 2</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div id="logo" class="logo">
                        <a href="<?php echo site_url('/'); ?>"><img src="<?php echo base_url(''); ?>assets-view/images/voru/logoheader.png" alt="images"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu" style="width: 80%;">
                        <nav id="main-nav" class="main-nav">
                            <div class="row">
                                <div class="col-lg-7">
                                    <ul class="menu" style="padding-right: 20px;">
                                        <li><a href="<?php echo site_url('B2B'); ?>">VORU For Bussiness</a></li>
                                        <li><a href="<?php echo site_url('Farmers'); ?>">VORU For Farmers</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-5">
                                    <ul class="menu" style="padding-right: 20px; text-align: end;">
                                        <li><a href="<?php echo site_url('Account') ?>">Masuk/Daftar</a></li>
                                        <li><a href="<?php echo site_url('Subscribe'); ?>">Subscribe</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    </ul>
                    </nav>
                </div>
            </div>
    </div>
    </header>
</div><!-- header -->

<style>
    .topp {
        color: white;
        font-size: 15px;
    }
</style>

</div><!-- wrap-header -->