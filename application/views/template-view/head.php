<head>
    <meta charset="UTF-8">

    <title>VORU</title>
    <meta name="description" content="Tingkatkan Derajat Komuditas Bersama VORU">

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons-->
    <link href="<?php echo base_url(''); ?>assets-view/images/voru/footer.png" rel="shortcut icon">

    <!-- Bootstrap-->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/bootstrap.css">

    <!-- Template Style-->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/stylecs.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/shortcodess.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/jquery-fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/responsives.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/stylesheet/jquery.mCustomScrollbar.min.css">

    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(''); ?>assets-view/revolution-slider/fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(''); ?>assets-view/revolution-slider/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(''); ?>assets-view/revolution-slider/css/layerssz.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(''); ?>assets-view/revolution-slider/css/navigation.css">

    <!-- BASE CSS -->
    <link href="<?php echo base_url(''); ?>assets-view/css/styles.css" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="<?php echo base_url(''); ?>assets-view/css/homee.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url(''); ?>assets-view/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets-view/testi/css/educaa.css">

</head>