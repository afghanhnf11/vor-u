<div class="wrap-header">
    <div class="flat-header flat-header-style2">
        <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div id="logo" class="logo">
                        <a href="<?php echo site_url('/'); ?>"><img style="margin-top: -80px;" src="<?php echo base_url(''); ?>assets-view/images/voru/logoheader.png" alt="images"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu" style="width: 80%;">
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li style="width: 100%; padding-bottom: 0px;">
                                    <form class="example" action="/action_page.php" style="margin: -10px;">
                                        <select name="cars" id="cars" class="select">
                                            <option value="produk">Produk</option>
                                            <option value=""></option>
                                        </select>
                                        <input type="text" placeholder="Search.." name="search">
                                        <button type="submit" class="search"><i class="icon_search"></i></button>
                                        <button type="submit" class="cart"> <span class="icon_cart"></span></button>
                                        <a href="<?php echo site_url('B2B/Subscribe') ?>" class="subs"> <span class="icon_tag"></span> &nbsp; Subscribe</a>
                                        <a href="<?php echo site_url('Account') ?>" class="subs"><i class="fa fa-user"></i> &nbsp; Joe Biden</a>
                                    </form>
                                </li>
                                <li style="padding-top: 0px; padding-bottom: 0px;"><a href="" style="font-size: 13px;">Komoditas</a></li>
                                <li style="padding-top: 0px; padding-bottom: 0px;"><a href="" style="font-size: 13px;">Logam</a></li>
                                <li style="padding-top: 0px; padding-bottom: 0px;"><a href="" style="font-size: 13px;">Susu</a></li>
                                <li style="padding-top: 0px; padding-bottom: 0px;"><a href="" style="font-size: 13px;">Kertas</a></li>
                            </ul>
                        </nav>
                    </div>
                    </ul>
                    </nav>
                </div>
            </div>
    </div>
    </header>
</div><!-- header -->

<style>
    .topp {
        color: white;
        font-size: 15px;
    }

    .select {
        float: left;
        width: 13%;
        padding: 7.5px;
        font-size: 14px;
        border: 1px solid #ddd;
        background: #f1f1f1;
        border-right: none;
        border-radius: 10px 0px 0px 10px;
    }

    .rights {
        text-align: right;
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 425px) {
        .rights {
            text-align: left;
        }

        #myDIV {
            height: 360px;
            width: 100%;
            overflow: auto;
        }

        .Disabled {
            color: currentColor;
            cursor: not-allowed;
            opacity: 0.7;
            text-decoration: none;
        }

        /* width */
        #myDIV::-webkit-scrollbar {
            width: 3px;
        }

        /* Track */
        #myDIV::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 10px;
        }

        /* Handle */
        #myDIV::-webkit-scrollbar-thumb {
            background: #72D291 !important;
            border-radius: 10px;
        }

        /* Handle on hover */
        #myDIV::-webkit-scrollbar-thumb:hover {
            background: #b30000;
        }

        /* Style the tab */
        .tab {
            overflow: hidden;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            text-align: center;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
            border-radius: 5px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
            border-radius: 5px;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
            border-radius: 5px;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 40px 20px 0px 0px;
            border-top: none;
        }
    }

    form.example input[type=text] {
        padding: 8px;
        font-size: 14px;
        border: 1px solid #ddd;
        float: left;
        width: 42%;
    }

    .search {
        float: left;
        width: 8%;
        padding: 8px;
        background: #53b6e0;
        color: white;
        font-size: 15px;
        border-radius: 0px 10px 10px 0px;
        border: 1px solid #ddd;
        border-left: none;
        cursor: pointer;
    }

    .cart {
        float: left;
        width: 7%;
        padding: 8px;
        background: none;
        color: black;
        font-size: 20px;
        border: none;
        border-left: none;
        cursor: pointer;
    }

    .subs {
        float: left;
        width: 15%;
        padding: 8px;
        background: none;
        color: black;
        font-size: 15px;
        border: none;
        border-left: none;
        cursor: pointer;
    }

    form.example button:hover {
        background: #0b7dda;
    }

    form.example::after {
        content: "";
        clear: both;
        display: table;
    }
</style>

</div><!-- wrap-header -->