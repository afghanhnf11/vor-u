<script>
    $(document).ready(function() {
        $(".preloader").delay(1000).fadeOut();
    })
</script>

<!--CSS Preloader GIF-->
<style type="text/css">
    .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999999999;
        background-color: #3f8cff;
    }

    .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        font: 14px arial;
    }
</style>

<div class="preloader">
    <div class="loading">
        <img src="https://cdn.dribbble.com/users/433927/screenshots/2109489/media/49dd7f07718940bc99147adc6c52155e.gif" width="400" />
    </div>
</div>
</div>