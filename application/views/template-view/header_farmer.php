<div class="wrap-header">
    <div class="flat-header flat-header-style2">
        <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu" style="width: 100%;">
                        <nav id="main-nav" class="main-nav" style="padding-top: 20px; padding-bottom: 20px;">
                            <div class="row">
                                <div class="col-lg-7">
                                    <ul class="menu" style="padding-right: 20px;">
                                        <li><a href="<?php echo site_url('/'); ?>">Home</a></li>
                                        <li><a href="<?php echo site_url('B2B'); ?>">VORU For Bussiness</a></li>
                                        <li><a href="<?php echo site_url('Farmers'); ?>">VORU For Farmers</a></li>
                                        <li><a href="<?php echo site_url('B2B/About'); ?>">About Us</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-5">
                                    <ul class="menu" style="padding-right: 20px; text-align: end;">
                                        <li><a href="<?php echo site_url('Account') ?>">Masuk/Daftar</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    </ul>
                    </nav>
                </div>
            </div>
    </div>
    </header>
</div><!-- header -->

<style>
    .topp {
        color: white;
        font-size: 15px;
    }
</style>

</div><!-- wrap-header -->