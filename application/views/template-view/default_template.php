<!DOCTYPE html>
<html lang="en">

<?php echo $page["head"]; ?>

<style type="text/css">
    <?php if (isset($css)) {
        echo $css;
    } ?>
</style>

<?php echo $page["header"]; ?>

<?php echo $page['main_js']; ?>
<script type="text/javascript">
    <?php if (isset($javascript)) {
        echo $javascript;
    } ?>
</script>

<body>
    <?php echo $page["preloader"]; ?>

    <?php echo $content; ?>

    <?php echo $page["footer"]; ?>
</body>

</html>