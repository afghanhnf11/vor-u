<section style="padding-top: 130px; background-color: #f8f8f8;">
    <div class=" container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/logos.png" alt="images" style="width: 100%; height: auto; padding: 100px;">
                </div>

                <div class="col-lg-6">
                    <div class="title-section" style="margin-bottom: 25px;">
                        <div class="title-section text-center" style="margin-bottom: 25px;">
                            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Daftar Sebagai Penjual</div>
                        </div>
                    </div>
                    <div class="text-center">
                        Sudah Punya Akun Seller? <a href="<?php echo site_url('Account/masuk') ?>">Masuk</a>
                    </div>
                    <form class="mt-3">
                        <div class="sign-in-wrapper">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label>Sandi / Password</label>
                                <input type="password" class="form-control" name="password" id="password" value="">
                            </div>
                            <div class="clearfix add_bottom_15">
                                <div class="checkboxes float-left">
                                    <label class="container_check">Saya Menyetujui Syarat & Ketentuan
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <input type="submit" value="Daftar" class="btn_1 full-width mb_5 mt-2">
                            <div class="mt-3">
                                Mengalami Kendala? <a href="">Ngobrol dengan CS VORU</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section style="padding-top: 50px;">
    <div class="container">
        <div class="title-section" style="margin-bottom: 5px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Kenapa Bergabung Dengan Kami?</div>
            </div>
        </div>
        <!-- /main_title -->
        <div class="col-lg-12">
            <div class="flat-benefit-style2 clearfix">
                <div class="row">
                    <div class="col">
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/1.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Marketplace Komoditas Terbesar di Indonesia</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Terhubung dengan komoditas Pertanian/Perkebunan/Perikanan di seluruh wilayah indonesia</p>
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/2.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Keuntungan Menarik</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Daftarkan produkmu di Voru dan dapatkan keuntungan untuk dapat free pencarian pelanggan selama 5 kali</p>
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/3.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Jaminan Keamanan Bertransaksi</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Jaminan Keamanan dalam bertransaksi karena kami sudah berizin usaha dan diawasi oleh OJK</p>
                            </a>
                        </div>
                    </div>
                    <div class="col">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/4.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Kemudahan Bertransaksi</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Menyediakan berbagai jenis metode pembayaran dari pembayaran offline hingga Pembayaran online.</p>
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/5.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Kemudahan Akses Untuk Bertemu Pelanggan</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Voru memberikan kemudahan akses bagi seluruh pelaku komoditas untuk mendapatkan lebih banyak pelanggan</p>
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/why/6.png" alt="images" style="height: 200px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <h3 style="font-weight: 600; font-size: 18px;">Membuka Akses untuk Sejahtera Bersama</h3>
                                <p style="margin-top: 10px; line-height: 1.5;">Memberikan pendanaan kepada para pelaku komoditas yang membutuhkan modal tambahan</p>
                            </a>
                        </div>
                    </div>
                    <div class="col">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section style="padding-top: 50px;">
    <div class="container">
        <div class="title-section" style="margin-bottom: 5px;">
            <div class="title-section text-center">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Testimoni Pelanggan</div>
            </div>
        </div>
        <!-- /main_title -->
        <div class="col-lg-12">
            <div class="flat-benefit-style2 clearfix">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="item" style="text-align: center; border-radius: 10px; border: solid grey 0.5px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/testi/1.png" alt="images" style="height: 130px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <p style="margin-top: 10px; margin-bottom: 15px; line-height: 1.5;">Data Belum Ada</p>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item" style="text-align: center; border-radius: 10px; border: solid grey 0.5px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/testi/2.png" alt="images" style="height: 130px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <p style="margin-top: 10px; margin-bottom: 15px; line-height: 1.5;">Data Belum Ada</p>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item" style="text-align: center; border-radius: 10px; border: solid grey 0.5px;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/testi/3.png" alt="images" style="height: 130px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                                <p style="margin-top: 10px; margin-bottom: 15px; line-height: 1.5;">Data Belum Ada</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section style="padding-top: 50px; padding-bottom: 60px;">
    <div class="container">
        <div class="title-section" style="margin-bottom: 5px;">
            <div class="title-section text-center" style="margin-bottom: 35px;">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Partner Kami</div>
            </div>
        </div>
        <!-- /main_title -->
        <div class="col-lg-12">
            <div class="flat-benefit-style2 clearfix">
                <div class="row">
                    <div class="col">
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/1.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/2.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/3.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                    </div>

                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/4.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/5.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item" style="text-align: center;">
                            <a href="#">
                                <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/partner/6.png" alt="images" style="height: 100px; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
            </div>
        </div>
        <div class="title-section" style="margin-bottom: 5px; margin-top: 70px;">
            <div class="title-section text-center" style="margin-bottom: 50px;">
                <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Memulai Berjualan di VORU dengan Sangat Mudah</div>
            </div>
        </div>

        <img src="<?php echo base_url(''); ?>assets-view/images/voru/login/alur.png" alt="images" style="height: auto; object-fit: contain; width: 100%; margin-left: auto; margin-right: auto;">
    </div>
</section>