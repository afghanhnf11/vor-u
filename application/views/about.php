<!-- START SLIDER -->
<div id="rev_slider_44_wrapper wave" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 90% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->
                <img src="<?php echo base_url('assets-view/images/voru/farmer/slider.jpg'); ?>" alt="" data-bgcolor="#ccc" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>

                <div class="rev-slider-mask"></div>
                <div class="container">
                    <section class="transparent-head transparent-head-style5">
                        <div class="wrap-transparent">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-6 static" style="text-align: center; margin-top: auto; margin-bottom: auto;">
                                    <div class="pd-lf">
                                        <div class="title" style="color: white;">
                                            Tentang Kami
                                        </div>
                                        <p class="text" style="color: white;">
                                            FROM VORU, Vor-U
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/wave.png" alt="images" style="background-size: inherit !important; background-position: inherit !important; margin-top: 32px;">
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="margin-top: -90px; margin-bottom: 80px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6" style="margin-top: auto; margin-bottom: auto;">
                <img class="mb-4" src="<?php echo base_url('assets-view/images/voru/logoheader.png'); ?>" alt="" style="width: 40%; margin-bottom: 5px;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                        From VORU, Vor-U</div>
                </div>
            </div>
            <div class="col-lg-6" style="margin-top: auto; margin-bottom: auto; color: black;">
                <p>VORU adalah Online Marketplace berbasis Superapp yang memberikan kemudahan proses transaksi melalui fitur teknologi dan fitur yang dapat membantu pelaku komoditas lebih mudah dan memberikan keuntungan yang maksimal. Voru menjadi jembatan bagi pembeli baik untuk transaksi dalam jumlah grosir maupun eceran dari ratusan supplier terbaik dan terpercaya.</p><br>
                <p>Lebih lanjut, kami berupaya untuk membantu dan mensejahterakan para pelaku komoditas seperti petani dan nelayan yang ada di Indonesia.
                    Besar harapan apa yang kami lakukan mampu meningkatkan perbaikan dan pengembangan perekonomian daerah serta membawa dampak
                    yang lebih luas.</p>
            </div>

            <hr>

            <div class="col lg-6 mt-5" style="color: black;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                        Visi Kami</div>
                </div>
                <p>Menjadi platform komoditas terbesar di Indonesia yang membantu meningkatkan derajat
                    para pelaku komoditas seluruh Indonesia.</p>
            </div>

            <div class="col lg-6 mt-5" style="color: black;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 30px;">
                        Misi</div>
                </div>
                <p>Memberikan pelayanan terbaik bagi pelanggan
                    Memberikan akses yang mudah ke seluruh pelaku komoditas
                    untuk mendapatkan keuntungan yang maksimal</p>
            </div>
        </div>
    </div>
</section>

<section class="flat-team mg-flat-team pt-5 pb-5" style="background-color: #f6faff; padding: 20px;">
    <div class="container">
        <div class="title-section text-center">
            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Founder VORU</div>
        </div>

        <div class="pd-list-team pt-5 pb-3">
            <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="4" data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="false" data-nav="false">
                <div class="owl-carousel">
                    <div class="team-box-layout-h1">
                        <div class="item-img">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/about/team/1.png" alt="images" class="img-fluid" style="height: 340px; object-fit: cover;">
                        </div>
                        <div class="item-content">
                            <div class="item-title">
                                <a href="#">Laurensius Hansfel</a>
                            </div>
                            <div class="item-subtitle">Founder dan CEO Voru</div>
                            <ul class="item-social">
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="mailto:"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="team-box-layout-h1">
                        <div class="item-img">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/about/team/2.png" alt="images" class="img-fluid" style="height: 340px; object-fit: cover;">
                        </div>
                        <div class="item-content">
                            <div class="item-title">
                                <a href="#">M. Fauzul Akbar</a>
                            </div>
                            <div class="item-subtitle">Founder dan COO Voru</div>
                            <ul class="item-social">
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="mailto:"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="team-box-layout-h1">
                        <div class="item-img">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/about/team/3.png" alt="images" class="img-fluid" style="height: 340px; object-fit: cover;">
                        </div>
                        <div class="item-content">
                            <div class="item-title">
                                <a href="#">Sarah Nabilla</a>
                            </div>
                            <div class="item-subtitle">Founder dan CMO Voru</div>
                            <ul class="item-social">
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="mailto:"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="team-box-layout-h1">
                        <div class="item-img">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/about/team/4.png" alt="images" class="img-fluid" style="height: 340px; object-fit: cover;">
                        </div>
                        <div class="item-content">
                            <div class="item-title">
                                <a href="#">Axel Gozali</a>
                            </div>
                            <div class="item-subtitle">Founder dan CFO Voru</div>
                            <ul class="item-social">
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="mailto:"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="title-section text-center mt-5">
            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Lokasi VORU</div>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto; color: black;">
                    <p style="font-size: 25px;"><strong>Apartemen Paddington Heights by Alam Sutera</strong></p> <br>
                    <p>Jl. Lkr. Barat Alam Sutera, Panunggangan, Kec. Pinang, Kota Tangerang, Banten</p>
                </div>
                <div class="col-lg-6">
                    <iframe style="border-radius: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2902806535917!2d106.64797721371438!3d-6.225404762700463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fbe0ff88f8d1%3A0x5cf7f555cc6f828!2sPaddington%20Heights%20by%20Alam%20Sutera!5e0!3m2!1sen!2sid!4v1652120565390!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>