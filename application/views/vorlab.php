<!-- START SLIDER -->
<div id="rev_slider_44_wrapper wave" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery">
    <!-- Start revolution slider 5.4.8 fullscreen mode -->
    <div id="rev_slider_44" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8" style="height: 90% !important;">
        <ul>
            <!-- start slide 01 -->
            <li data-index="rs-73" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="01" data-param1="01" data-description="">
                <!-- main image -->
                <img src="<?php echo base_url('assets-view/images/voru/farmer/vorlab.jpg'); ?>" alt="" data-bgcolor="#ccc" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>

                <div class="rev-slider-mask"></div>
                <div class="container">
                    <section class="transparent-head transparent-head-style5">
                        <div class="wrap-transparent">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-6 static" style="text-align: center; margin-top: auto; margin-bottom: auto;">
                                    <div class="pd-lf">
                                        <div class="title" style="color: white;">
                                            VORLAB <br> Voru Collaborate
                                        </div>
                                        <p class="text" style="color: white;">
                                            YANG MENJADI MASALAHMU MENJADI SEBUAH SOLUSIKU
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/wave.png" alt="images" style="background-size: inherit !important; background-position: inherit !important; margin-top: 32px;">
            </li>
            <!-- end slide 01 -->
        </ul>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<section style="margin-top: -90px; margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-3" style="margin-top: auto; margin-bottom: auto;">
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/logoheader.png" alt="images" style="width: 100% !important;">
            </div>
            <div class="col-lg-5" style="margin-top: auto; margin-bottom: auto;">
                <div class="title-section" style="margin-bottom: 20px;">
                    <div class="flat-title larger heading-type9" style="font-weight: 700; font-size: 25px; color: #164B8A; text-align: end;">
                        VORU Percaya dengan mengembangkan sektor komoditas tidak hanya mengembangkan para pelaku komoditas, namun dapat meningkatkan perekonomian</div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</section>

<section class="mt-3 mb-5 pb-4" style="background-color: #369967; border-radius: 150px 0px;">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4" style="text-align: center; padding: 0px 30px;">
                    <div class="title-section" style="margin-top: 70px; margin-bottom: 50px;">
                        <div class="title-section text-center">
                            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700; color: white;">Solusi Untuk Petani & Peternak</div>
                        </div>
                        <p style="margin-top: -20px; color: black; text-align: center; font-size: 17px; color: white;">Layanan lengkap, dari hulu ke hilir untuk menjembatani pelaku komoditas. Rasakan kemudahan transaksi, program voru funding, serta pembelajaran lengkap untuk mengenal lebih dalam mengenai komoditas dengan menjadi voruman.</p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="<?php echo site_url('B2B'); ?>">
                                <div class="card" style="height: 205px; margin-top: 25px; border-radius: 10px; padding: 15px; background-color: white; text-align: center; color: black;">
                                    <h3 class="mb-3" style="font-size: 20px; font-weight: 700;">VORMERS</h3>
                                    <p>Tempat dipasarkannya hasil panen komoditas, memastikan transaksi & distribusi produk yang cepat, langsung dan andal</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-6">
                            <a href="<?php echo site_url('Farmers/vorfund'); ?>">
                                <div class="card" style="margin-top: 25px; border-radius: 10px; padding: 15px; background-color: white; text-align: center; color: black;">
                                    <h3 class="mb-3" style="font-size: 20px; font-weight: 700;">VORFUND</h3>
                                    <p>Sebuah layanan bisnis terbaru dari VORU, dengan konsep pendanaan secara penuh terhadap pemilik lahan bisnis komoditas oleh perusahaan atau individu dengan perjanjian.</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-6">
                            <div class="card" style="margin-top: 15px; border-radius: 10px; padding: 15px; background-color: white; text-align: center; color: black;">
                                <h3 class="mb-3" style="font-size: 20px; font-weight: 700;">VORLAB</h3>
                                <p>Sebuah layanan terbaru dari VORU, dengan konsep menjamin petani dan peternak menjual hasil panen komoditas terjual kepada perusahaan yang bekerjasama dengan VORU.</p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card" style="height: 205px; margin-top: 15px; border-radius: 10px; padding: 15px; background-color: white; text-align: center; color: black;">
                                <h3 class="mb-3" style="font-size: 20px; font-weight: 700;">VORUMAN</h3>
                                <p>Bantuan staff seperti koperasi, bumdes dan lainnya yang dimana bekerjasama dengan VORU untuk menjembatani kami dengan petani dan peternak.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mt-3 pb-4">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">

                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card" style="height: 260px; border-radius: 10px; padding: 15px; background-color: #369967; text-align: center; color: white;">
                                <h3 class="mb-3" style="font-size: 20px; font-weight: 700; margin-top: 12%;">Hasil Ternak & Tani</h3>
                                <p>Hasil ternak dan tani yang telah dipacking rapih, didistribusikan ke pelaku bisnis dengan teliti dan sistem terbaik untuk melindungi kualitas produk</p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <a href="<?php echo site_url('Farmers/vorfund'); ?>">
                                <div class="card" style="border-radius: 10px; padding: 15px; background-color: #369967; text-align: center; color: white;">
                                    <h3 class="mb-3" style="font-size: 20px; font-weight: 700;">Investasi</h3>
                                    <p>Melakukan kerjasama dengan pemilik lahan pertanian dan perternakan dengan konsep melakukan investasi terhadap keseluruhan lahan dan mendapatkan keuntungan dari penjualan hasil pertanian dan peterenakan</p>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4" style="text-align: center; padding: 0px 30px;">
                    <div class="title-section">
                        <div class="title-section text-center">
                            <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700; color: black;">Solusi Untuk Konsumen</div>
                        </div>
                        <p style="margin-top: -20px; color: black; text-align: center; font-size: 17px; color: black;">Layanan dan hasil lengkap yang ditawarkan oleh VORU yang dapat menjembatani antara perusahaan atau individu yang melakukan bisnis komoditas untuk mendapatkan hasil komoditas dan investasi dalam bidang komoditas.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<style>
    /* The Close Button */
    .closes {
        color: white;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .closes:hover,
    .closes:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 425px) {
        .container-popup {
            margin-top: 50px !important;
        }

        .container-popup2 {
            margin-top: 50px !important;
        }

        .container-popupg {
            margin-top: 100px !important;
        }

        .imgt {
            width: 30%;
            height: auto;
            object-fit: cover;
            border-radius: 5px;
        }

        /* Modal Content */
        .modal-contentt {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 90%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }
    }

    #close {
        background-color: rgba(64, 68, 65, 0.5);
        position: fixed;
        z-index: 999999;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    #close:target {
        -webkit-transition: all 1s;
        -moz-transition: all 1s;
        transition: all 1s;
        opacity: 0;
        visibility: hidden;
    }

    @media (min-width: 768px) {
        .container-popup {
            width: 35%;
        }

        .container-popup2 {
            width: 65%;
        }

        .container-popupg {
            width: 65%;
        }
    }

    @media (max-width: 767px) {
        .container-popup {
            width: 90%;
        }

        .container-popup2 {
            width: 90%;
        }

        .tab button {
            width: 50%;
        }

        .container-popupg {
            width: 80%;
        }
    }

    .container-popup {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popup img {
        width: 100%
    }

    .container-popup2 {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popup2 img {
        width: 100%
    }

    .container-popupg {
        position: relative;
        margin: 5% auto;
        margin-top: 10%;
        background-color: #fff;
        color: #333;
        border-radius: 8px;
    }

    .container-popupg img {
        width: 100%
    }

    .close {
        position: absolute;
        top: 3px;
        right: 3px;
        background-color: #33898B;
        padding: 7px 10px;
        font-size: 15px;
        text-decoration: none;
        line-height: 1;
        color: #fff;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 2s;
        animation-name: fade;
        animation-duration: 2s;
    }

    @-webkit-keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

    @keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }
</style>

<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/plugins.js"></script>

<script type="text/javascript">
    $(function() {
        $('a[href="#light-box"]').on('click', function(event) {
            event.preventDefault();
            $('#light-box').addClass('open');
            $('#light-box > form > input[type="light-box"]').focus();
        });

        $('#light-box, #light-box button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    });

    // init Isotope
    var portfolioGrid = $('#portfolio-grid');

    portfolioGrid.imagesLoaded(function() {
        portfolioGrid.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows',
            "masonry": {
                "columnWidth": ".portfolio-grid-sizer"
            }
        });
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };

    // bind filter button click
    $('#projects-filter').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        portfolioGrid.isotope({
            filter: filterValue
        });
        return false;
    });

    // change is-checked class on buttons
    $('#projects-filter').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // GALERI
    $('#galleryModal').on('show.bs.modal', function(e) {
        $('#galleryImage').attr("src", $(e.relatedTarget).data("large-src"));
    });
</script>