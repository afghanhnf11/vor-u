<section style="background: linear-gradient(180deg, #164A89 0%, #249BD1 100%); margin-bottom: 30px; margin-top: 130px; padding: 40px 40px 20px;">
    <div class="carousel-hobby carousel-hobby-style4">
        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="3" data-column3="3" data-column4="3" data-dots="false" data-auto="true" data-nav="true">
            <div class="owl-carousel">
                <a href="#">
                    <div class="imagebox-hobby">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/slide1.png" alt="images" style="border-radius: 10px;">
                    </div>
                </a>

                <a href="#">
                    <div class="imagebox-hobby">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/slide2.png" alt="images" style="border-radius: 10px;">
                    </div>
                </a>

                <a href="#">
                    <div class="imagebox-hobby">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/slide1.png" alt="images" style="border-radius: 10px;">
                    </div>
                </a>

                <a href="#">
                    <div class="imagebox-hobby">
                        <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/slide2.png" alt="images" style="border-radius: 10px;">
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="col-lg-12 text-center mt-4">
        <a href="" style="font-size: 16px; color: white;"><i class="fa fa-arrow-left" style="color: white;"></i>&nbsp; Lihat Promo Menarik Lainnya &nbsp;<i class="fa fa-arrow-left" style="color: white;"></i></a>
    </div>
</section>

<section>
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('Farmers/vorfund'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">VORU Funding</a>
            </div>
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/About'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">Tentang VORU</a>
            </div>
            <div class="col-lg-4">
                <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/Voruauction'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 20px !important;">VORU Auction</a>
            </div>
        </div>

        <div class="kategori mb-4" style="background: linear-gradient(180deg, #82C7E6 0%, #E8F8FF 100%); padding: 30px; border-radius: 10px;">
            <div class="row">
                <div class="col-lg-5">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9 mb-3" style="font-weight: 700; font-size: 25px; color: #164B8A;">
                            Kategori Pilihan</div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/KategoriProduk'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 10px !important;">Semua Produk</a>
                        </div>
                        <div class="col-lg-6">
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/KategoriProduk'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 10px !important;">Perkebunan</a>
                        </div>
                        <div class="col-lg-6">
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/KategoriProduk'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 10px !important;">Peternakan</a>
                        </div>
                        <div class="col-lg-6">
                            <a class="btn_1 mb_5 mt-5" href="<?php echo site_url('B2B/KategoriProduk'); ?>" style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 10px !important;">Logam Berharga</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7">
                    <div class="online-courses-wrap">
                        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="true" data-nav="false">
                            <div class="owl-carousel">
                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/kategori/1.png" alt="images" style="height: 200px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                        <div class="package">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Kertas</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/kategori/2.png" alt="images" style="height: 200px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                        <div class="package">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Ternak Sapi</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/kategori/3.png" alt="images" style="height: 200px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                        <div class="package">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Logam Berharga</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/kategori/4.png" alt="images" style="height: 200px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                        <div class="package">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Biji Padi</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="promo mb-5" style="border-radius: 10px; background: linear-gradient(180deg, #FFD953 3.13%, #FAB632 100%); padding: 20px;">
            <div class="row">
                <div class="col-lg-4" style="margin-bottom: auto; margin-top: auto;">
                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo.png" alt="" style="width: 100%;">
                </div>

                <div class="col-lg-8">
                    <div class="title-section" style="margin-bottom: 20px;">
                        <div class="flat-title larger heading-type9 mb-3" style="font-weight: 700; font-size: 35px; color: #164B8A;">
                            Rekomendasi Pilihan Voru Hari Ini</div>
                    </div>

                    <div class="online-courses-wrap">
                        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="true" data-nav="false">
                            <div class="owl-carousel">
                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/1.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                        <div class="package">
                                            <a href="#">Premium Seller</a>
                                        </div>
                                        <div class="courses-name pd-courses-name">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Biji Kopi Toraja <br> 1 Ton</a>
                                        </div>
                                    </div>
                                    <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                        <div class="price" style="font-size: 20px;">
                                            Rp. 47.500.000
                                        </div>
                                        <div class="coret"><br>

                                            <p><span style="color: red; font-weight: bold;">10%</span>
                                                <s>Rp. 52.250.000</s>
                                            </p>
                                            <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Toraja Selatan</p>
                                            <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/2.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                        <div class="package">
                                            <a href="#">Premium Seller</a>
                                        </div>
                                        <div class="courses-name pd-courses-name">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Buah Sawit <br> 30 Ton</a>
                                        </div>
                                    </div>
                                    <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                        <div class="price" style="font-size: 20px;">
                                            Rp. 87.550.000
                                        </div>
                                        <div class="coret"><br>

                                            <p><span style="color: red; font-weight: bold;">15%</span>
                                                <s>Rp. 103.000.000</s>
                                            </p>
                                            <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Penajam Paser Utara</p>
                                            <p style="text-align: end;"> Pre-Order</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/3.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                        <div class="package">
                                            <a href="#">Premium Seller</a>
                                        </div>
                                        <div class="courses-name pd-courses-name">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">150 Ayam <br> Kampung</a>
                                        </div>
                                    </div>
                                    <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                        <div class="price" style="font-size: 20px;">
                                            Rp. 7.950.000
                                        </div>
                                        <div class="coret"><br>

                                            <p><span style="color: red; font-weight: bold;">8%</span>
                                                <s>Rp. 9.640.000</s>
                                            </p>
                                            <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Tanggerang Kota</p>
                                            <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="imagebox-courses-type2">
                                    <div class="featured data-effect-item has-effect-icon">
                                        <div class="featured-post">
                                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/4.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                        </div>
                                        <div class="overlay-effect bg-cl333"></div>
                                        <div class="elm-link elm-link-style3">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                        <div class="package">
                                            <a href="#">Premium Seller</a>
                                        </div>
                                        <div class="courses-name pd-courses-name">
                                            <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">Daging Sapi <br> 700 Kg</a>
                                        </div>
                                    </div>
                                    <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                        <div class="price" style="font-size: 20px;">
                                            Rp. 96.425.000
                                        </div>
                                        <div class="coret"><br>

                                            <p><span style="color: red; font-weight: bold;">5%</span>
                                                <s>Rp. 101.500.000</s>
                                            </p>
                                            <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Bandung Selatan</p>
                                            <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?php echo site_url('B2B/Voucher'); ?>">
            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/bannerpromo.png" alt="" class="mt-0 mb-5">
        </a>
    </div>

    <div class="komoditas mb-5">
        <div class="container">
            <div class="title-section" style="margin-bottom: 20px; margin-top: 20px;">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Pilihan Komoditas</div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/1.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Kopi</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/3.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Teh</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/2.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Daging Sapi</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/4.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Cocoa</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/5.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Susu</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/6.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Kertas</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/7.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Daging Ayam</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/8.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Perak</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/9.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Emas</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/10.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Aluminium</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/11.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Sawit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/komoditas/12.png" alt="images" style="
                                            object-fit: cover; border-radius: 10px 10px 0px 0px;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/KategoriProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="background-color: transparent; padding: 0px !important; margin-top: -20px;">
                                <div class="package">
                                    <a href="#" style="margin-left: 10px;">Tembaga</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title-section" style="margin-bottom: 20px; margin-top: 90px;">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20" style="font-size: 30px; font-weight: 700;">Pilihan Komoditas Lainnya</div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/1.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Biji Kopi Toraja <br> 1 Ton</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 47.500.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">10%</span>
                                        <s>Rp. 52.250.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Toraja Selatan</p>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/2.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Buah Sawit <br> 30 Ton</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 87.550.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">15%</span>
                                        <s>Rp. 103.000.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Penajam Paser Utara</p>
                                    <p style="text-align: end;"> Pre-Order</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/3.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">150 Ayam <br> Kampung</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 7.950.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">8%</span>
                                        <s>Rp. 9.640.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Tanggerang Kota</p>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/4.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Daging Sapi <br> 700 Kg</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 96.425.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">5%</span>
                                        <s>Rp. 101.500.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Bandung Selatan</p>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/5.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Salmon Utuh <br> 5 Lusin</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 68.400.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">5%</span>
                                        <s>Rp. 72.000.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Jakarta Utara</p>
                                    <p style="text-align: end;"> Pre-Order</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/6.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">10 Kwintal <br> Gandum</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 28.150.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">3%</span>
                                        <s>Rp. 29.000.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Jakarta Timur</p>
                                    <p style="text-align: end;"> Pre-Order</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/7.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Biji Kopi Gayo <br> 1 Ton</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 49.500.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">1%</span>
                                        <s>Rp. 55.000.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Toraja Selatan</p>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/promo/8.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="package">
                                    <a href="#">Premium Seller</a>
                                </div>
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Buah Sawit <br> 40 Kg</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 13px 20px 12px 20px;">
                                <div class="price" style="font-size: 20px;">
                                    Rp. 125.500.000
                                </div>
                                <div class="coret"><br>

                                    <p><span style="color: red; font-weight: bold;">15%</span>
                                        <s>Rp. 135.500.000</s>
                                    </p>
                                    <p style="text-align: left;"><i class="fa fa-map-pin"></i>&nbsp; Penajem Paser</p>
                                    <p style="text-align: end;"> Pre-Order</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="title-section" style="margin-bottom: 0px; margin-top: 30px;">
                <div class="title-section" style="margin-bottom: 30px;">
                    <div class="flat-title medium heading-type19" style="font-size: 30px; font-weight: 700;">VORU Auction</div>
                </div>
            </div>

            <a href="<?php echo site_url('B2B/Voruauction'); ?>">
                <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/voru.png" alt="" class="mt-0 mb-5">
            </a>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/voruauction/1.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Kopi Bali</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 20px;">
                                <div class="price" style="font-size: 20px;">
                                    <span style="font-weight: normal;">-+</span> &nbsp; Rp. 50.000.000
                                </div>
                                <div class="coret"><br>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/voruauction/2.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Beras Grade AAA</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 20px;">
                                <div class="price" style="font-size: 20px;">
                                    <span style="font-weight: normal;">-+</span> &nbsp; Rp. 90.000.000
                                </div>
                                <div class="coret"><br>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/voruauction/3.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Telur Ayam</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 20px;">
                                <div class="price" style="font-size: 20px;">
                                    <span style="font-weight: normal;">-+</span> &nbsp; Rp. 35.000.000
                                </div>
                                <div class="coret"><br>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="imagebox-courses-type2">
                            <div class="featured data-effect-item has-effect-icon">
                                <div class="featured-post">
                                    <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/voruauction/4.png" alt="images" style="height: 150px;
                                            object-fit: cover; border-radius: 10px 10px 0px 0px; width: 100%;">
                                </div>
                                <div class="overlay-effect bg-cl333"></div>
                                <div class="elm-link elm-link-style3">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info-courses" style="padding: 28px 20px 10px 20px;">
                                <div class="courses-name pd-courses-name">
                                    <a href="<?php echo site_url('B2B/DetailProduk'); ?>">Kacang Tanah</a>
                                </div>
                            </div>
                            <div class="evaluate clearfix" style="padding: 20px;">
                                <div class="price" style="font-size: 20px;">
                                    <span style="font-weight: normal;">-+</span> &nbsp; Rp. 20.000.000
                                </div>
                                <div class="coret"><br>
                                    <p style="text-align: end;"><i class="fa fa-check"></i>&nbsp; Ready Stock</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title-section" style="margin-bottom: 0px; margin-top: 30px;">
                <div class="title-section" style="margin-bottom: 0px;">
                    <div class="flat-title medium heading-type19" style="font-size: 30px; font-weight: 700;">VORU FUNDING</div>
                </div>
            </div>

            <div class="row latest-blog latest-blog-type2 latest-blog-style2" style="padding: 40px 0 40px 0;">
                <div class="col-lg-4 col-md-4">
                    <article class="post post-style2 box-shadow-type1">
                        <div class="featured-post">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/produk/1.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                        </div>
                        <div class="post-content" style="padding: 10px 20px; background-color: #b2dac6; color: black; border-radius: 0;">
                            <div class="post-title" style="margin: 10px 0px;">
                                <h5>
                                    <a href="<?php echo site_url('Farmers/vorfund'); ?>" style="font-size: 20px !important; font-weight: 700;">Pertanian Jagung Pak Udin</a>
                                </h5>
                            </div>
                            <div class="category" style="width: 280px;">
                                <a href="" style="color: black;"><i class="fa fa-map-pin"></i>&nbsp; Kabupaten Tanah Laut, Kalimantan Selatan 8 Hektare</a>
                                <p style="color: black;">Estimasi Panen Per Tahun : 2-4 Kali</p>
                                <p style="color: black;">Estimasi Lama Kontrak : 2-4 Tahun</p>
                            </div>
                        </div>
                        <div class="foot" style="text-align: center; padding: 10px; background-color: #66B68E; color: black; border-radius: 0px 0px 10px 10px;">
                            <p style="font-weight: bold;">1.750.000.000 - 2.500.000.000</p>
                        </div>
                    </article>
                </div>

                <div class="col-lg-4 col-md-4">
                    <article class="post post-style2 box-shadow-type1">
                        <div class="featured-post">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/vorufunding/2.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                        </div>
                        <div class="post-content" style="padding: 10px 20px; background-color: #b2dac6; color: black; border-radius: 0;">
                            <div class="post-title" style="margin: 10px 0px;">
                                <h5>
                                    <a href="<?php echo site_url('Farmers/vorfund'); ?>" style="font-size: 20px !important; font-weight: 700;">Perkebunan Cabai Pak Asep</a>
                                </h5>
                            </div>
                            <div class="category" style="width: 280px;">
                                <a href="" style="color: black;"><i class="fa fa-map-pin"></i>&nbsp; Banyuwangi Selatan, Jawa Timur 12 Hektare</a>
                                <p style="color: black;">Estimasi Panen Per Tahun : >20 Kali</p>
                                <p style="color: black;">Estimasi Lama Kontrak : 1-3 Tahun</p>
                            </div>
                        </div>
                        <div class="foot" style="text-align: center; padding: 10px; background-color: #66B68E; color: black; border-radius: 0px 0px 10px 10px;">
                            <p style="font-weight: bold;">2.000.000.000 - 32.500.000.000</p>
                        </div>
                    </article>
                </div>

                <div class="col-lg-4 col-md-4">
                    <article class="post post-style2 box-shadow-type1">
                        <div class="featured-post">
                            <img src="<?php echo base_url(''); ?>assets-view/images/voru/farmer/vorfund/vorufunding/3.png" alt="images" style="height: 220px; object-fit: cover; object-position: unset;">
                        </div>
                        <div class="post-content" style="padding: 10px 20px; background-color: #b2dac6; color: black; border-radius: 0;">
                            <div class="post-title" style="margin: 10px 0px;">
                                <h5>
                                    <a href="<?php echo site_url('Farmers/vorfund'); ?>" style="font-size: 20px !important; font-weight: 700;">Pertanian Kacang Tanah Pak Wawan</a>
                                </h5>
                            </div>
                            <div class="category" style="width: 280px;">
                                <a href="" style="color: black;"><i class="fa fa-map-pin"></i>&nbsp; Lombok Barat, NTB 9 Hektare</a>
                                <p style="color: black;">Estimasi Panen Per Tahun : 2-4 Kali</p>
                                <p style="color: black;">Estimasi Lama Kontrak : 1-4 Tahun</p>
                            </div>
                        </div>
                        <div class="foot" style="text-align: center; padding: 10px; background-color: #66B68E; color: black; border-radius: 0px 0px 10px 10px;">
                            <p style="font-weight: bold;">1.900.000.000 - 2.650.000.000</p>
                        </div>
                    </article>
                </div>
            </div>
            <!-- CONTAINER -->
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(''); ?>assets-view/testi/js/plugins.js"></script>

<script type="text/javascript">
    $(function() {
        $('a[href="#light-box"]').on('click', function(event) {
            event.preventDefault();
            $('#light-box').addClass('open');
            $('#light-box > form > input[type="light-box"]').focus();
        });

        $('#light-box, #light-box button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    });

    // init Isotope
    var portfolioGrid = $('#portfolio-grid');

    portfolioGrid.imagesLoaded(function() {
        portfolioGrid.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows',
            "masonry": {
                "columnWidth": ".portfolio-grid-sizer"
            }
        });
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };

    // bind filter button click
    $('#projects-filter').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        portfolioGrid.isotope({
            filter: filterValue
        });
        return false;
    });

    // change is-checked class on buttons
    $('#projects-filter').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // GALERI
    $('#galleryModal').on('show.bs.modal', function(e) {
        $('#galleryImage').attr("src", $(e.relatedTarget).data("large-src"));
    });
</script>