<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Wilayah extends CI_Model {

	public function get_all() {
		$query = $this->db->query('SELECT * FROM `wilayah` GROUP BY kd_kecamatan');
    return $query->result_array();
	}

	// get kecamatan
	function get_kecamatan_pasien(){
    $query = $this->db->query("SELECT * FROM mapwil WHERE kd_kabupaten = '3276' GROUP BY kd_kecamatan ORDER BY kecamatan_nama");
    return $query->result_array();
  }

	function get_kelurahan($idkota, $id_kecamatan){
    $query = $this->db->query("SELECT * FROM mapwil WHERE kd_kabupaten = '$idkota' and kecamatan = '$id_kecamatan' ");
    return $query->result_array();
  }

	function get_kota($id_provinsi){
    $query = $this->db->query("SELECT * FROM mapwil WHERE kd_propinsi = '$id_provinsi' GROUP BY kd_kabupaten");
    return $query->result_array();
  }

	function get_kecamatan_ktp($id_kota){
    $query = $this->db->query("SELECT * FROM mapwil WHERE kd_kabupaten = '$id_kota' GROUP BY kd_kecamatan");
    return $query->result_array();
  }

	function get_kelurahan_ktp($idkota, $id_kecamatan_ktp){
    $query = $this->db->query("SELECT * FROM mapwil WHERE kd_kabupaten = '$idkota' AND kd_kecamatan = '$id_kecamatan_ktp'");
    return $query->result_array();
  }

	public function get_prov() {
		$query = $this->db->query("SELECT * FROM mapwil GROUP BY kd_propinsi");
    return $query->result_array();
	}

	function get_kota_ktp_by_provinsi_ktp($ProvinsiKTP){
		if ($ProvinsiKTP != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE propinsi = '$ProvinsiKTP' GROUP BY kd_kabupaten");
	    return $query->result_array();
		} else {
			return '';
		}
  }

	function get_kecamatan_ktp_by_kota_ktp($ProvinsiKTP, $KotaKTP){
		if ($KotaKTP != '' && $ProvinsiKTP != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE propinsi = '$ProvinsiKTP' AND kabupaten LIKE '%$KotaKTP%' GROUP BY kecamatan");
	    return $query->result_array();
		} else {
			return '';
		}
  }

	function get_kelurahan_ktp_by_kecamatan_ktp($KotaKTP, $KecamatanKTP){
		if ($KotaKTP != '' && $KecamatanKTP != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE kabupaten LIKE '%$KotaKTP%' AND kecamatan = '$KecamatanKTP'");
	    return $query->result_array();
		} else {
			return '';
		}
  }

	function get_kota_tinggal_by_provinsi_tinggal($ProvinsiTingal){
		if ($ProvinsiTingal != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE propinsi = '$ProvinsiTingal' GROUP BY kd_kabupaten");
	    return $query->result_array();
		} else {
			return '';
		}
  }

	function get_kecamatan_tinggal_by_kota_tinggal($ProvinsiTinggal, $KotaTinggal){
		if ($KotaTinggal != '' && $ProvinsiTinggal != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE propinsi = '$ProvinsiTinggal' AND kabupaten LIKE '%$KotaTinggal%' GROUP BY kecamatan");
	    return $query->result_array();
		} else {
			return '';
		}
  }

  function get_kelurahan_tinggal($KecamatanTinggal){
	if ($KecamatanTinggal != '') {
		$query = $this->db->query("SELECT * FROM mapwil WHERE kabupaten LIKE '%Kota Depok%' AND kecamatan = '$KecamatanTinggal'");
	return $query->result_array();
	} else {
		return '';
	}
}

	function get_kelurahan_tinggal_by_kecamatan_tinggal($KotaTinggal, $KecamatanTinggal){
		if ($KotaTinggal != '' && $KecamatanTinggal != '') {
			$query = $this->db->query("SELECT * FROM mapwil WHERE kabupaten LIKE '%$KotaTinggal%' AND kecamatan = '$KecamatanTinggal'");
	    return $query->result_array();
		} else {
			return '';
		}
  }

}
