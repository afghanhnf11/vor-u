<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Quiz extends CI_Model {

	public function getPertanyaan()
	{
		$this->db->where('TypeReview ', 'R002');
		$data = $this->db->get('groupquestion');
		return $data;
	}

	public function getOpsi()
	{
	    $this->db->select('Opsi, PertanyaanId, OpsiId, Bobot');
		$this->db->from('kuisioner');
		$this->db->join('opsi','kuisioner.OpsiId=opsi.Id');
		$query = $this->db->get();
		return $query;
	}

	public function getOpsiBobot($opsiid)
	{
	    $this->db->where('Id', $opsiid);
	    $query = $this->db->get('opsi');
	    return $query;
	}

	public function getOpsiId($id)
	{
	    $this->db->select('Opsi, PertanyaanId, OpsiId, Bobot');
		$this->db->from('kuisioner');
		$this->db->join('opsi','kuisioner.OpsiId=opsi.Id');
		$this->db->where('PertanyaanId', $id);
		$query = $this->db->get();
		return $query;
	}

	public function getTotalBobot($id, $siteid, $tanggal)
	{
	    $this->db->select('Count(bobot1.Id) as total1, Count(bobot2.Id) as total2, Count(bobot3.Id) as total3, Count(bobot4.Id) as total4');
	    $this->db->from('kuisioner_form kf');
	    $this->db->join('(SELECT Id FROM kuisioner_form WHERE bobot=1)bobot1', 'bobot1.Id = kf.Id', 'left');
	    $this->db->join('(SELECT Id FROM kuisioner_form WHERE bobot=2)bobot2', 'bobot2.Id = kf.Id', 'left');
	    $this->db->join('(SELECT Id FROM kuisioner_form WHERE bobot=3)bobot3', 'bobot3.Id = kf.Id', 'left');
	    $this->db->join('(SELECT Id FROM kuisioner_form WHERE bobot=4)bobot4', 'bobot4.Id = kf.Id', 'left');
	    $this->db->where('kf.PertanyaanId', $id);
	    $this->db->where('kf.SiteId', $siteid);
	    $this->db->where('date(CreateDate)', $tanggal);
	    $data = $this->db->get();
	    return $data;
	}

	public function getITM($SiteId)
	{
        $today = date('Y-m-d');
		$this->db->select('Pertanyaan1.sum1 AS unsur1, Pertanyaan2.sum2 AS unsur2,Pertanyaan3.sum3 AS unsur3, Pertanyaan4.sum4 AS unsur4, Pertanyaan5.sum5 AS unsur5,Pertanyaan6.sum6 AS unsur6,Pertanyaan7.sum7 AS unsur7,Pertanyaan8.sum8 AS unsur8, Pertanyaan9.sum9 AS unsur9, (Pertanyaan1.sum1/Pertanyaan1.count1) as nrr1, (Pertanyaan2.sum2/Pertanyaan2.count2) as nrr2, (Pertanyaan3.sum3/Pertanyaan3.count3) as nrr3, (Pertanyaan4.sum4/Pertanyaan4.count4) as nrr4, (Pertanyaan5.sum5/Pertanyaan5.count5) as nrr5, (Pertanyaan6.sum6/Pertanyaan6.count6) as nrr6, (Pertanyaan7.sum7/Pertanyaan7.count7) as nrr7, (Pertanyaan8.sum8/Pertanyaan8.count8) as nrr8, (Pertanyaan9.sum9/Pertanyaan9.count9) as nrr9');
	    $this->db->from('kuisioner_form');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum1,COUNT(Bobot) as count1 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 2)Pertanyaan1', 'Pertanyaan1.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum2,COUNT(Bobot) as count2 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 3)Pertanyaan2', 'Pertanyaan2.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum3,COUNT(Bobot) as count3 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 4)Pertanyaan3', 'Pertanyaan3.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum4,COUNT(Bobot) as count4 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 5)Pertanyaan4', 'Pertanyaan4.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum5,COUNT(Bobot) as count5 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 6)Pertanyaan5', 'Pertanyaan5.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum6,COUNT(Bobot) as count6 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 7)Pertanyaan6', 'Pertanyaan6.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum7,COUNT(Bobot) as count7 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 8)Pertanyaan7', 'Pertanyaan7.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum8,COUNT(Bobot) as count8 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 9)Pertanyaan8', 'Pertanyaan8.SiteId = kuisioner_form.SiteId','left');
		$this->db->join('(SELECT SiteId,CreateDate,SUM(Bobot) as sum9,COUNT(Bobot) as count9 FROM kuisioner_form WHERE DATE(kuisioner_form.CreateDate) = "'.$today.'" AND PertanyaanId = 10)Pertanyaan9', 'Pertanyaan9.SiteId = kuisioner_form.SiteId','left');
		$this->db->where('kuisioner_form.SiteId', $SiteId);
		$this->db->GROUP_BY('kuisioner_form.SiteId');
		$query = $this->db->get();
		return $query;
	}

}

/* End of file M_Quiz.php */
/* Location: ./application/models/M_Quiz.php */