<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Feedback extends CI_Model {

	public function datafeed($code)
	{
		$this->db->select('b.Id, o.NamaObject, so.Name, q.Question');
		$this->db->from('batch b');
		$this->db->join('object o', 'o.Id = b.ObjectId', 'left');
		$this->db->join('subobject so', 'so.Id = b.SubObjectId', 'left');
		$this->db->join('groupquestion q', 'q.Id = b.QuestionId', 'left');
		$this->db->where('RandomString', $code);
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function thisday($siteId, $filterulasan = null, $objlist = null)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('date(f.CreateDate)', 'CURDATE()', FALSE);
		$this->db->where('b.SiteId', $siteId);
		if ($filterulasan !='') {
			$this->db->where('fv.FeedbackRating', $filterulasan);
		}
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		$data = $this->db->get()->row();
		return $data;
	}
	public function thismonth($siteId, $filterulasan = null, $objlist = null)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('Month(f.CreateDate)', 'Month(CURDATE())', FALSE);
		$this->db->where('year(f.CreateDate)', 'year(CURDATE())', FALSE);
		$this->db->where('b.SiteId', $siteId);
		if ($filterulasan !='') {
			$this->db->where('fv.FeedbackRating', $filterulasan);
		}
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		$data = $this->db->get()->row();
		return $data;
	}
	public function thisyear($siteId, $filterulasan = null, $objlist = null)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('year(f.CreateDate)', 'year(CURDATE())', FALSE);
		$this->db->where('b.SiteId', $siteId);
		if ($filterulasan !='') {
			$this->db->where('fv.FeedbackRating', $filterulasan);
		}
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		$data = $this->db->get()->row();
		return $data;
	}
	public function total($siteId, $filterulasan = null, $mindate = null, $maxdate = null, $objlist = null)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('b.SiteId', $siteId);
		if ($filterulasan !='') {
			$this->db->where('fv.FeedbackRating', $filterulasan);
		}
		if (!empty($mindate) && !empty($maxdate)) {
			$this->db->where('date(f.CreateDate) >=', $mindate);
			$this->db->where('date(f.CreateDate) <=', $maxdate);
		}
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		$data = $this->db->get()->row();
		return $data;
	}
	public function countpositif($siteId)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f');
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('fv.FeedbackRating = ', 1);
		$this->db->where('b.SiteId', $siteId);
		$data = $this->db->get()->row();
		return $data;
	}
	public function countnegatif($siteId)
	{
		$this->db->select('count(*) as total');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('fv.FeedbackRating = ', 0);
		$this->db->where('b.SiteId', $siteId);
		$data = $this->db->get()->row();
		return $data;
	}
	public function trenpositif($siteId)
	{
		$this->db->select('IFNULL(count(*), "0") as total, date(f.CreateDate) as day');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('fv.FeedbackRating = ', 1);
		$this->db->where('b.SiteId', $siteId);
		$this->db->group_by('date(f.CreateDate)');
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function trennegatif($siteId)
	{
		$this->db->select('count(*) as total, date(f.CreateDate) as day');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('fv.FeedbackRating = ', 0);
		$this->db->where('b.SiteId', $siteId);
		$this->db->group_by('date(f.CreateDate), fv.FeedbackRating');
		$this->db->order_by('date(f.CreateDate)', 'asc');
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function trenfeedback($siteId, $periode, $objlist)
	{
		$now_year = date("Y");
		$now_month = date("m");
		// if (!empty($periode)) {
		if ($periode == 'bulan') {
			$date = 'month(f.CreateDate)';
			$where = $this->db->where('year(f.CreateDate) = ', $now_year);
			$limit = $this->db->limit(12);
		}else if ($periode == 'tahun') {
			$date = 'year(f.CreateDate)';			
			$where = $this->db->where('year(f.CreateDate) = ', $now_year);
			$limit = $this->db->limit(5);
		}else{
			$date = 'date(f.CreateDate)';			
			$where = $this->db->where('month(f.CreateDate)', $now_month);
			$limit = $this->db->limit(7);
		}
		// }
		$this->db->select('fv.FeedbackRating as rating, count(*) as total, '.$date.' as day');
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('b.SiteId', $siteId);
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		$where;
		$this->db->group_by($date.' ,fv.FeedbackRating');
		$this->db->order_by($date , 'asc');
		$limit;
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function feedbackcomment($rowperpage, $siteId, $start, $filterulasan, $mindate, $maxdate, $sorting, $objlist)
	{
		$this->db->select('fv.FeedbackRating as rating, date(f.CreateDate) as day, CASE WHEN r.Name is NULL THEN "Guest" ELSE r.Name END AS name, fv.feedbackcomment as comment', false);
		$this->db->from('feedback f'); 
		$this->db->join('batch b', 'b.Id = f.BatchId', 'left'); 
		$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('b.SiteId', $siteId);
		if ($filterulasan !='') {
			$this->db->where('fv.FeedbackRating', $filterulasan);
		}
		if (!empty($mindate) && !empty($maxdate)) {
			$this->db->where('date(f.CreateDate) >=', $mindate);
			$this->db->where('date(f.CreateDate) <=', $maxdate);
		}
		if (!empty($objlist)) {
			$this->db->where('b.ObjectId', $objlist);
		}
		// $this->db->group_by('"name", fv.FeedbackRating, date(f.CreateDate)');
		$this->db->order_by('f.CreateDate', $sorting);
		$this->db->limit($rowperpage, $start);
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function objfeedback($siteId, $mindate, $maxdate)
	{
		$this->db->select('fv.FeedbackRating as rating, count(*) as total, sobj.Name as subobject, obj.NamaObject as object');
		$this->db->from('batch b'); 
		$this->db->join('object obj', 'obj.Id = b.ObjectId', 'left');
		$this->db->join('subobject sobj', 'b.SubObjectId = sobj.Id', 'left');
		$this->db->join('feedback f', 'b.Id = f.BatchId', 'left');
		$this->db->join('feedbackvalue fv', 'fv.FeedbackId = f.Id', 'left');
		$this->db->where('b.SiteId', $siteId);
		if (!empty($mindate) && !empty($maxdate)) {
			$this->db->where('date(f.CreateDate) >=', $mindate);
			$this->db->where('date(f.CreateDate) <=', $maxdate);
		}
		$this->db->group_by('obj.NamaObject, fv.FeedbackRating, sobj.Name');
		$this->db->order_by('obj.NamaObject', 'asc');
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function objlist($siteId)
	{
		$this->db->select('o.Id, o.NamaObject as object');
		$this->db->from('object o');
		$this->db->join('batch b', 'o.Id = b.ObjectId', 'left');
		$this->db->where('siteId', $siteId);
		$data = $this->db->get()->result_array();
		return $data;
	}
	public function getSite($codeSite)
    {
        $this->db->select('*');
        $this->db->from('site');
        $this->db->like('Code', $codeSite);
        // $query = $this->db->get();
		// return $query->row();
		$query = $this->db->get()->result_array();
		return $query;
	}
	public function databatch($code)
	{
		$this->db->select('b.Id as BatchId,b.RandomString, b.SiteId as Id , o.NamaObject, so.Name as NamaSubObject, q.Name, q.Code');
		$this->db->from('batch b');
		$this->db->join('object o', 'o.Id = b.ObjectId', 'left');
		$this->db->join('subobject so', 'so.Id = b.SubObjectId', 'left');
		$this->db->join('site q', 'q.Id = b.SiteId', 'left');
		$this->db->where('RandomString', $code);
		$data = $this->db->get()->result_array();
		return $data;
	}
	// public function averageTren()
	// {
	// 	$this->db->select('ROUND(AVG(fv.FeedbackRating))');
	// 	$this->db->from('feedback f');
	// 	$this->db->join('feedbackvalue fv', 'f.Id = fv.FeedbackId', 'left');
	// 	$this->db->join('reviewers r', 'f.ReviewersId = r.Id', 'left');
	// 	$this->db->where('fv.FeedbackRating = ', 1);
	// }

}

/* End of file M_Feedback.php */
/* Location: ./application/models/M_Feedback.php */