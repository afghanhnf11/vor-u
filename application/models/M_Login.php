<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_Login extends CI_Model
{
    // var $TABLE = "users";
    // var $COLUMN = array(
    //     "id",
    //     "nama_lengkap",
    //     "email",
    //     "organisasi_id",
    //     "level_id",
    //     "username",
    //     "password",
    //     "site_id",
    // );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function read()
    {
        try {
            $this->db->select("*");
            $this->db->from($this->TABLE);
            $query = $this->db->get();
            return $query->result();
        }
        catch (Exception $e) {
            return null;
        }
        
    }
    
    public function readBy($username)
    {
        // $domain = $_SERVER['HTTP_HOST'];
        // echo $domain;die;
        try {
            $this->db->select('
    Id, Name, Email, ContactId, Password');
            $this->db->from('user');
            $this->db->where('Email', $username);
            $this->db->limit(1);
            $query = $this->db->get();
            return $query->row();
        }
        catch (Exception $e) {
            return null;
        }
    }
    public function getSite($codeSite)
    {
        $this->db->select('*');
        $this->db->from('site');
        $this->db->like('Code', $codeSite);
        $query = $this->db->get();
        return $query->row();
    }
    public function getMember($Id)
    {
        $this->db->select('member.Id, OrganizationId, member.UserId, member.SiteId');
        $this->db->from('member');
        $this->db->join('organization', 'organization.Id = member.OrganizationId', 'left');
        $this->db->where('organization.Name IS NOT NULL', NULL, FALSE);
        $this->db->where('UserId', $Id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getMemberSite($Id)
    {
        $this->db->select('member.Id, member.OrganizationId, member.UserId, member.SiteId, site.Name , site.Code');
        $this->db->from('member');
        $this->db->join('user', 'member.UserId = user.Id ', 'left');
        $this->db->join('site', 'member.SiteId = site.Id','left');
        $this->db->join('organization', 'organization.Id = member.OrganizationId', 'left');
        $this->db->where('user.Email', $Id);
        $this->db->where('organization.Name IS NOT NULL', NULL, FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getOrganization($IdOrganization)
    {
        $this->db->select('Id, Name, SiteId, ParentId, TypeId');
        $this->db->from('organization');
        $this->db->where('Id', $IdOrganization);
        $query = $this->db->get();
        return $query->row();
    }
    public function getListRoleMenu($siteId, $userId)
    {
        $this->db->select('mn.Id, mn.Code, mn.TypeId, mn.Description, mn.GroupId, mn.ParentId, mn.LevelMenu');
        $this->db->from('user');
        $this->db->join('userrole ur', 'ur.UserId = user.Id and ur.SiteId= ' . $siteId, 'left');
        $this->db->join('rolemenu rm', 'rm.RoleId = ur.RoleId', 'left');
        $this->db->join('menu mn', 'mn.Id = rm.MenuId', 'left');
        $this->db->where('user.Id', $userId);
        $this->db->group_by(array(
            'mn.Id',
            'mn.TypeId',
            'mn.Description',
            'mn.GroupId',
            'mn.ParentId',
            'mn.LevelMenu'
        ));
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUserRole($siteId, $userId)
    {
        $this->db->select('UserId, RoleId, SiteId');
        $this->db->from('userrole');
        $this->db->where('UserId', $userId);
        $this->db->where('SiteId', $siteId);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
    public function getRole($RoleId)
    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id', $RoleId);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
}