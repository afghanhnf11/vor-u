<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_SmartPortal extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function profilsite($siteId)
	{
		$this->db->select('*');
		$this->db->from('site');
		$this->db->where('Id', $siteId);
		$query = $this->db->get();
		return $query->result();
	}

	public function domainsite($domain)
	{
		// $this->db->select('*');
		// $this->db->from('site');
		// $this->db->where('domain', $domain);
		// $query = $this->db->get();
		// return $query->result();


		// $site = $this->db->query("SELECT * FROM site WHERE domain= '$domain'");
		// $site = $this->db->query("SELECT *, organization.Name as Organisasi FROM organization left join site on site.Id=organization.SiteId  WHERE site.domain= '$domain'");
		$site = $this->db->query("SELECT *,site.Id as IdSite,site.Name as NamaSite, organization.Name as Organisasi FROM site left join organization on site.pic = organization.Id   WHERE site.domain= '$domain'");

		$siteId = null;

    /*
    //maaf mas unggull saya disable dlu. nyangkut kalau didevelopment.
    //host saya ngak bisa diganti, walau sudah administrator
    //
        if ($site) {
            $siteId = $site->Id + 0;
        } else {
      die('SITE NOT FOUND!!!!!!!!!!!');
    }
    */

    if ($site->num_rows() != 0) {
      //var_dump($site->result());
    	foreach($site->result() as $subdomain){
    		$siteId['Id'] = $subdomain->IdSite;
    		$siteId['Code'] = $subdomain->Code;
    		$siteId['Name'] = $subdomain->NamaSite;
    		$siteId['imageSite'] = $subdomain->imageSite;
			$siteId['Alamat'] = $subdomain->Alamat;
			$siteId['Kecamatan'] = $subdomain->kecamatan;
			$siteId['Kelurahan'] = $subdomain->kelurahan;
			$siteId['Organisasi'] = $subdomain->Organisasi;
			$siteId['Domain'] = $subdomain->Domain;



    	}
    }
    else
    {
    	$siteId['Id'] = '0';
    	$siteId['Code'] = '404 Not Found!';
    	$siteId['Name'] = 'portal@depok.go.id';
    	$siteId['imageSite'] = '-';
    	$siteId['Alamat'] = 'Depok, Indonesia';
		$siteId['Domain'] = 'cms.depok.go.id';
		$siteId['Organisasi'] = '-';

    }

    return $siteId;


}


public function getgroupContent($siteId, $kanalType, $groupId, $status, $parent, $limit, $offset)
{
	// $this->db->distinct();
	$this->db->select('content.content_id, content.title, category.Description, content.ParentId');
	$this->db->from('content');
	$this->db->join('category', 'category.Id = content.category_id', 'left');
	$this->db->where('content.site_id', $siteId);
	$this->db->where('content.type_id', $kanalType);
	if (!empty($groupId)) {
		$this->db->like('category.Description', $groupId, 'after');
	}
	$this->db->where('content.ParentId', NULL);
	$this->db->where('content.status', "ST01");
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$query = $this->db->get()->result();
	return $query;
}

public function content($siteId, $status, $kanalType, $groupId, $parent, $limit, $offset, $category, $typelayanan = '', $slug, $key, $subsite) {
	$this->db->select('content.*,st.Name as SiteName, st.Domain as DomainSite, reference.Description as Kanal, category.Description as Category,user.Name as Author,  tl.typelayanan');
	$this->db->from('content');
	$this->db->join('reference', 'reference.Code = content.type_id', 'left');
	$this->db->join('category', 'category.Id = content.category_id', 'left');
	$this->db->join('user', 'user.Id = content.author', 'left');
	$this->db->join('typelayanan tl', 'tl.Id = content.JenisLayanan', 'left');
	$this->db->join('site st', 'st.Id = content.site_id', 'left');
	// $this->db->where_in('site._id', $siteId);
	$this->db->where('type_id', $kanalType);
	$this->db->where('status', "ST01");
		// if (!empty($status)) {
		// 	$this->db->where('status', $status);
		// }
	if (!empty($siteId)) {
			$this->db->where('site_id', $siteId);
	}
	if (!empty($groupId)) {
		$this->db->like('category.Description', $groupId, 'after');
	}
	if (!empty($parent)) {
		$this->db->where('content.ParentId', $parent);
	}
	if (!empty($category)) {
		$this->db->where_in('category.Id', $category);
	}
	if (!empty($slug)) {
		$this->db->where('slug_title', $slug);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	if (!empty($typelayanan)) {
		$this->db->where('JenisLayanan', $typelayanan);
	}
	if (!empty($key)) {
		$this->db->like('content.title', $key);
			//$this->db->or_like('content.content', $key);
			//$this->db->having('site_id = '.$siteId);
			// $this->db->having('type_id = '.$kanalType);
			// $this->db->having('status = '.$status);
	}
	if (!empty($subsite)) {
		$this->db->where('st.ParentId', $subsite);
	}
	$this->db->order_by('created_at', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function contentcategory($siteId, $status, $Id, $kanalType, $parentId)
{
	$this->db->select('content.category_id as Id, category.Description as Category, count(content.category_id) as Total');
	$this->db->from('content');
	$this->db->join('reference', 'reference.Code = content.type_id', 'left');
	$this->db->join('category', 'category.Id = content.category_id', 'left');
	$this->db->join('user', 'user.Id = content.author', 'left');
	$this->db->where_in('site_id', $siteId);
		// $this->db->where('site_id', $siteId);
	$this->db->where('type_id', $kanalType);
	$this->db->where('category.Description is not null',  null, false);
	$this->db->where('status', "ST01");
		// if (!empty($status)) {
		// 	$this->db->where('status', $status);
		// }
	if (!empty($parentId)) {
		$this->db->where('category.parent_id', $parentId);
	}
	if (!empty($Id)) {
		$this->db->where('content.category_id', $Id); 
	}
	$this->db->group_by('category.Id');
	$this->db->group_by('category.Description');
		// $this->db->order_by('created_at', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
	// public function ContentTags($siteId, $contentId)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('tags');
	// 	$this->db->where('SiteId', $siteId);
	// 	if (!empty($contentId)) {
	// 		$this->db->where('ContentId', $contentId);
	// 	}
	// 	$query = $this->db->get()->result();
	// 	return $query;
	// }
public function ContentTags($siteId, $contentId, $kanalType)
{
	$this->db->select('TagsName');
	$this->db->from('tags');
	$this->db->where('SiteId', $siteId);
	if (!empty($contentId)) {
		$this->db->where('ContentId', $contentId);
	}
	if (!empty($kanalType)) {
		$this->db->where('KanalType', $kanalType);
	}
	$query = $this->db->get()->row();
	return $query;
}
public function SearchTags($siteId, $kanalType, $key)
{
	$this->db->select('ContentId');
	$this->db->from('tags');
	$this->db->where('KanalType', $kanalType);
	$this->db->where('SiteId', $siteId);
	$this->db->like('TagsName', $key);
	$query = $this->db->get()->result();
	return $query;
}
public function BeritaTag($siteId, $kanalType, $ContentId)
{
	$this->db->select('content.*, reference.Description as Kanal, category.Description as Category,user.Name as Author');
	$this->db->from('content');
	$this->db->join('reference', 'reference.Code = content.type_id', 'left');
	$this->db->join('category', 'category.Id = content.category_id', 'left');
	$this->db->join('user', 'user.Id = content.author', 'left');
	$this->db->where('status', "ST01");
	$this->db->where('site_id', $siteId);
	$this->db->where('type_id', $kanalType);
	$this->db->where_in('content_id', $ContentId);
	$this->db->order_by('created_at', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function countContent($siteId, $status, $kanalType, $groupId, $category,$subsite)
{
	$this->db->select('count(*) as total');
	$this->db->from('content');
	$this->db->join('category', 'category.Id = content.category_id', 'left');
	$this->db->join('site st', 'st.Id = content.site_id', 'left');
	// $this->db->where_in('content.site_id', $siteId);
		// $this->db->where('site_id', $siteId);
	$this->db->where('content.type_id', $kanalType);
		// if (!empty($status)) {
		// 	$this->db->where('status', $status);
		// }
	if (!empty($siteId)) {
		$this->db->where('content.site_id', $siteId);
	}
	$this->db->where('content.status', "ST01");
	if (!empty($groupId)) {
		$this->db->like('category.Description', $groupId, 'after');
	}
	if (!empty($category)) {
		$this->db->where('content.category_id', $category);
	}
	if (!empty($subsite)) {
		$this->db->where('st.ParentId', $subsite);
	}
	$query = $this->db->get()->row();
	return $query;
}
public function getGallery($siteId, $categoryId, $type, $limit, $offset)
{
	$this->db->select('gallery.*, category.Description');
	$this->db->from('gallery');
	$this->db->join('category', 'category.Id = gallery.CategoryId', 'left');
	$this->db->where('gallery.SiteId', $siteId);
	if (!empty($categoryId)) {
		$this->db->where('gallery.CategoryId', $categoryId);
	}
	if (!empty($type)) {
		$this->db->where('gallery.MediaType', $type);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$this->db->order_by('Id', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getPlace($siteId, $typeId, $Id,  $limit, $offset)
{
	$this->db->select('place.*, categoryplace.Description as Type');
	$this->db->from('place');
		//$this->db->join('reference', 'place.TypeId = reference.Code', 'left');
	$this->db->join('categoryplace', 'place.TypeId = categoryplace.Id', 'left');
	$this->db->where('place.SiteId', $siteId);
	if (!empty($Id)) {
		$this->db->where('place.Id', $Id);
	}
	if (!empty($typeId)) {
		$this->db->where('TypeId', $typeId);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$this->db->order_by('Id', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getPlaceCat($siteId, $limit, $offset)
{
	$this->db->select('place.TypeId as Id, categoryplace.Description as Type');
	$this->db->from('place');
		//$this->db->join('reference', 'place.TypeId = reference.Code', 'left');
	$this->db->join('categoryplace', 'place.TypeId = categoryplace.Id', 'left');
	$this->db->where('place.SiteId', $siteId);
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$this->db->group_by('place.TypeId');
	$query = $this->db->get()->result();
	return $query;
}
public function getSlider($siteId, $status, $typeId, $fileType)
{
	$this->db->select('slider.*, FileType.Description as FileType, Type.Description as Type');
	$this->db->from('slider');
	$this->db->join('reference as FileType', 'slider.Type = FileType.Code', 'left');
	$this->db->join('reference as Type', 'slider.GroupSlider = Type.Code', 'left');
	$this->db->where('SiteId', $siteId);
		// if (!empty($status)) {
		// 	$this->db->where('Status', $status);
		// }
	$this->db->where('Status', "ST01");
	if (!empty($typeId)) {
		$this->db->where('GroupSlider', $typeId);
	}
	if (!empty($fileType)) {
		$this->db->where('Type', $fileType);
	}
	$this->db->order_by('Sorting', 'asc');
	$query = $this->db->get()->result();
	return $query;
}
public function getSliderDSW($siteId, $status, $typeId, $fileType)
{
	$this->db->select('slider.id as id, slider.Title as name , CONCAT("https://cms.depok.go.id/upload/slider/",slider.File) as image, 1 as status , slider.url as URL , slider.CreatedDate as created_at  , slider.ModifierDate as updated_at , FileType.Description as FileType, Type.Description as Type');
	$this->db->from('slider');
	$this->db->join('reference as FileType', 'slider.Type = FileType.Code', 'left');
	$this->db->join('reference as Type', 'slider.GroupSlider = Type.Code', 'left');
	$this->db->where('SiteId', $siteId);
		// if (!empty($status)) {
		// 	$this->db->where('Status', $status);
		// }
	$this->db->where('Status', "ST01");
	if (!empty($typeId)) {
		$this->db->where('GroupSlider', $typeId);
	}
	if (!empty($fileType)) {
		$this->db->where('Type', $fileType);
	}
	$this->db->order_by('Sorting', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function Slider($siteId, $status, $typeId, $fileType)
{
	$this->db->select('slider.*, FileType.Description as FileType, Type.Description as Type');
	$this->db->from('slider');
	$this->db->join('reference as FileType', 'slider.Type = FileType.Code', 'left');
	$this->db->join('reference as Type', 'slider.GroupSlider = Type.Code', 'left');
	$this->db->where('SiteId', $siteId);
		// if (!empty($status)) {
		// 	$this->db->where('Status', $status);
		// }
	$this->db->where('Status', "ST01");
	if (!empty($typeId)) {
		$this->db->where('GroupSlider', $typeId);
	}
	if (!empty($fileType)) {
		$this->db->where('Type', $fileType);
	}
	$this->db->order_by('Sorting', 'asc');
	$query = $this->db->get()->result();
	return $query;
	
}
public function getContact($contactId, $siteId, $organisasiId, $userId)
{
	$this->db->select('contact.*, organization.Name as Org, jabatan.Jabatan, jabatan.TugasPokok, jabatan.UraianTugas');
	$this->db->from('contact');
	$this->db->join('jabatan', 'contact.Occupation = jabatan.Id', 'left');
	$this->db->join('organization', 'contact.OrganizationId = organization.Id', 'left');
	$this->db->where('contact.SiteId', $siteId);
	if (!empty($contactId)) {
		$this->db->where('contact.Id', $contactId);
	}
	if (!empty($organisasiId)) {
		$this->db->where('contact.OrganizationId', $organisasiId);
	}
	if (!empty($userId)) {
		$this->db->where('contact.UserId', $userId);
	}
	$this->db->order_by('CreateDate', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getExLink($siteId, $code, $groupId, $typeId, $limit, $offset, $slug, $groupcat)
{
	$this->db->select('*');
	$this->db->from('externalLink');
	$this->db->where('externalLink.SiteId', $siteId);
	if (!empty($code)) {
		$this->db->where('Code', $code);
	}
	if (!empty($groupcat)) {
		$this->db->like('GroupId', $groupId, 'after');
	}
	if (!empty($groupId)) {
		$this->db->where('GroupId', $groupId);
	}
	if (!empty($typeId)) {
		$this->db->where('TypeId', $typeId);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	if (!empty($slug)) {
		$this->db->where('SlugTitle', $slug);
	}
	$this->db->order_by('CreateDate', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getgroupExlink($siteId, $code, $groupId, $TypeId, $limit, $offset)
{
	$this->db->distinct();
	$this->db->select('GroupId');
	$this->db->from('externalLink');
	$this->db->where('externalLink.SiteId', $siteId);
	if (!empty($code)) {
		$this->db->where('Code', $code);
	}
	if (!empty($groupId)) {
		$this->db->like('GroupId', $groupId, 'after');
	}
	if (!empty($typeId)) {
		$this->db->where('TypeId', $typeId);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$query = $this->db->get()->result();
	return $query;
}
public function CountExlink($siteId, $code, $groupId, $typeId)
{
	$this->db->select('count(*) as total');
	$this->db->from('externalLink');
	$this->db->where('externalLink.SiteId', $siteId);
	if (!empty($code)) {
		$this->db->where('Code', $code);
	}
	if (!empty($groupId)) {
		$this->db->where('GroupId', $groupId);
	}
	if (!empty($typeId)) {
		$this->db->where('TypeId', $typeId);
	}
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$query = $this->db->get()->row();
	return $query;
}
public function getJabatan($siteId, $limit, $offset)
{
	$this->db->select('*');
	$this->db->from('jabatan');
	$this->db->where('SiteId', $siteId);
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$this->db->order_by('Id', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getSite($siteId)
{
	$this->db->select('Id, ParentId');
	$this->db->from('site');
	$this->db->where('ParentId is NOT NULL', NULL, FALSE);
	$this->db->where('Id', $siteId);
	$this->db->order_by('Id', 'desc');
	$query = $this->db->get()->row();
	return $query;
}
public function getGalleryCat($siteId, $limit, $offset)
{
	$this->db->select('gallery.CategoryId as CategoryId, category.Description as Category');
	$this->db->from('gallery');
	$this->db->join('category', 'category.Id = gallery.CategoryId', 'left');
	$this->db->where('gallery.SiteId', $siteId);
	if (!empty($limit)) {
		$this->db->limit($limit,$offset);
	}
	$this->db->group_by('gallery.CategoryId');
	$query = $this->db->get()->result();
	return $query;
}
public function CountGallery($siteId, $category)
{
	$this->db->select('count(*) as total');
	$this->db->from('gallery');
	$this->db->where('SiteId', $siteId);
	if (!empty($category)) {
		$this->db->where('CategoryId', $category);
	}
	$query = $this->db->get()->row();
	return $query;
}
public function CountPlace($siteId, $typeId)
{
	$this->db->select('count(*) as total');
	$this->db->from('place');
	$this->db->where('SiteId', $siteId);
	if (!empty($typeId)) {
		$this->db->where('TypeId', $typeId);
	}
	$query = $this->db->get()->row();
	return $query;
}
public function getEvent($siteId, $type,  $limit)
{
	$this->db->select('*');
	$this->db->from('event');
	$this->db->where('SiteId', $siteId);
	if (!empty($type)) {
		$this->db->where('Type', $type);
	}
	if (!empty($limit)) {
		$this->db->limit($limit);
	}
	$this->db->order_by('TanggalAwal', 'desc');
	$query = $this->db->get()->result();
	return $query;
}
public function getStruktur($siteId, $jabatanId)
{
	$this->db->select('strukturorganisasi.Id, Nama, Deskripsi, Foto, JabatanId, jabatan.Jabatan, jabatan.UraianTugas, jabatan.TugasPokok');
	$this->db->from('strukturorganisasi');
	$this->db->join('jabatan', 'strukturorganisasi.JabatanId = jabatan.Id', 'left');
	if (!empty($jabatanId)) {
		$this->db->where('JabatanId', $jabatanId);
	}
	$this->db->where('strukturorganisasi.SiteId', $siteId);
	$query = $this->db->get()->result();
	return $query;
}

public function postKotakMasuk($data,$table)
{
        $this->db->insert($table,$data);

	// return $query;
}



}

/* End of file M_SmartPortal.php */
/* Location: ./application/models/M_SmartPortal.php */