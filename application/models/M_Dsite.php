<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Dsite extends CI_Model {
    public function sitePopuler($siteId) {
        $this->db->select('s.Name, COUNT(l.siteid) AS jumlah');
        $this->db->from('site s');
        $this->db->join('logview l', 'l.siteid = s.Id', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $this->db->group_by('s.Id');
        $this->db->order_by('jumlah', 'DESC');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function allsitePopuler($siteId) {
        $this->db->select('COUNT(*) AS jumlah');
        $this->db->from('logview l');
        $this->db->join('site s', 's.Id = l.siteid', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungBeritamulti($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('content l');
        $this->db->join('site s', 's.Id = l.site_id', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungGalerimulti($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('gallery l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungKontakmulti($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('contact l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungAgendamulti($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('event l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungExlinkmulti($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('externalLink l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

	public function multiTahunini($siteId) {
        $this->db->select('FORMAT(COUNT(*), "N") AS Jumlah');
        $this->db->from('logview l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $this->db->where('YEAR(l.create_at) = YEAR(CURRENT_DATE())');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function multiBulanini($siteId) {
        $this->db->select('FORMAT(COUNT(*), "N") AS Jumlah');
        $this->db->from('logview l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $this->db->where('MONTH(l.create_at) = MONTH(CURRENT_DATE())');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function multiHariini($siteId) {
        $this->db->select('COUNT(*) AS Jumlah');
        $this->db->from('logview l');
        $this->db->join('site s', 's.Id = l.SiteId', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $this->db->where('DATE(l.create_at) = CURRENT_DATE()');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function totalSite($siteId) {
        $this->db->select('COUNT(*) AS Total');
        $this->db->from('site');
        $this->db->where('ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function totalContentsite($siteId) {
        $this->db->select('COUNT(*) AS Total');
        $this->db->from('content l');
        $this->db->join('site s', 's.Id = l.site_id', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    // public function terupdate($siteId) {
    //     $this->db->select('s.Name as Site, c.updated_at, DATE_FORMAT(max(c.updated_at), "%d-%m-%Y") AS Waktu');
    //     $this->db->from('site s');
    //     $this->db->join('content c', 'c.site_id = s.Id', 'LEFT');
    //     $this->db->where('s.ParentId', $siteId);
    //     $this->db->group_by(array("Site", "c.updated_at"));
    //     $this->db->order_by('MONTH(updated_at)', 'DESC');
    //     $this->db->limit(1);
    //     $query = $this->db->get()->result_array();
    //     return $query;
    // }

    public function tb_update($siteId) {
        $this->db->select('s.Name as Site, DATE_FORMAT(max(c.updated_at), "%d-%m-%Y") AS Waktu, DATEDIFF(CURDATE(), max(c.updated_at)) AS Selisih');
        $this->db->from('site s');
        $this->db->join('content c', 'c.site_id = s.Id', 'LEFT');
        $this->db->where('s.ParentId', $siteId);
        $this->db->group_by("Site");
        $this->db->order_by('MAX(updated_at)', 'DESC');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function lineSite($siteId) {
        $query = "SELECT month.namemonth, IFNULL( b.Jumlah, 0 ) as Jumlah
        FROM (
            SELECT 1 AS month, 'Jan' AS namemonth
            UNION SELECT 2 AS month, 'Feb' AS namemonth
            UNION SELECT 3 AS month, 'Mar' AS namemonth
            UNION SELECT 4 AS month, 'Apr' AS namemonth
            UNION SELECT 5 AS month, 'May' AS namemonth
            UNION SELECT 6 AS month, 'Jun' AS namemonth
            UNION SELECT 7 AS month, 'Jul' AS namemonth
            UNION SELECT 8 AS month, 'Aug' AS namemonth
            UNION SELECT 9 AS month, 'Sep' AS namemonth
            UNION SELECT 10 AS month, 'Oct' AS namemonth
            UNION SELECT 11 AS month, 'Nov' AS namemonth
            UNION SELECT 12 AS month, 'Des' AS namemonth
        ) as month
        LEFT JOIN (SELECT COUNT(*) as Jumlah, month(l.create_at) AS Bulan FROM logview l LEFT JOIN site s ON s.Id = l.siteid WHERE YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND s.ParentId = ".$siteId." GROUP by Bulan) b ON b.Bulan = month.month";
        $hasil = $this->db->query($query)->result_array();

            return $hasil;
    }

    public function linemultiKanal($siteId) {
        $query = "SELECT l.Name as Nama, j.Jumlah as Januari, f.Jumlah as Februari, r.Jumlah as Maret, p.Jumlah as April, m.Jumlah as Mei, u.Jumlah as Juni, i.Jumlah as Juli, g.Jumlah as Agustus, s.Jumlah as September, o.Jumlah as Oktober, n.Jumlah as November, d.Jumlah as Desember
FROM (SELECT Id AS siteId, Name FROM site WHERE ParentId = ".$siteId." GROUP BY Id) AS l
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 1 GROUP by s.Name) j ON j.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 2 GROUP by s.Name) f ON f.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 3 GROUP by s.Name) r ON r.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 4 GROUP by s.Name) p ON p.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 5 GROUP by s.Name) m ON m.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 6 GROUP by s.Name) u ON u.siteId = l.siteId

LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 7 GROUP by s.Name) i ON i.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 8 GROUP by s.Name) g ON g.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 9 GROUP by s.Name) s ON s.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 10 GROUP by s.Name) o ON o.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 11 GROUP by s.Name) n ON n.siteId = l.siteId
LEFT JOIN (SELECT l.siteId, s.Name, COUNT(*) AS Jumlah FROM logview l LEFT JOIN site s ON s.Id = l.siteId WHERE s.ParentId = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE()) AND MONTH(l.create_at) = 12 GROUP by s.Name) d ON d.siteId = l.siteId";

        $hasil = $this->db->query($query)->result_array();

        $string[] = "";
        for ($i=0; $i < count($hasil); $i++) { 
            $januari  = isset($hasil[$i]["Januari"]) ? (integer)$hasil[$i]["Januari"]  : 0;
            $februari = isset($hasil[$i]["Februari"])? (integer)$hasil[$i]["Februari"] : 0;
            $maret    = isset($hasil[$i]["Maret"])   ? (integer)$hasil[$i]["Maret"]    : 0;
            $april    = isset($hasil[$i]["April"])   ? (integer)$hasil[$i]["April"]    : 0;
            $mei      = isset($hasil[$i]["Mei"])     ? (integer)$hasil[$i]["Mei"]      : 0;
            $juni     = isset($hasil[$i]["Juni"])    ? (integer)$hasil[$i]["Juni"]     : 0;
            $juli     = isset($hasil[$i]["Juli"])    ? (integer)$hasil[$i]["Juli"]     : 0;
            $agustus  = isset($hasil[$i]["Agustus"]) ? (integer)$hasil[$i]["Agustus"]  : 0;
            $september= isset($hasil[$i]["September"])? (integer)$hasil[$i]["September"] : 0;
            $oktober  = isset($hasil[$i]["Oktober"]) ? (integer)$hasil[$i]["Oktober"]  : 0;
            $november = isset($hasil[$i]["November"])? (integer)$hasil[$i]["November"] : 0;
            $desember = isset($hasil[$i]["Desember"])? (integer)$hasil[$i]["Desember"] : 0;

            $data = [$januari,$februari,$maret,$april,$mei,$juni,$juli,$agustus,$september,$oktober,$november,$desember];

            $string[] = ["name" => $hasil[$i]["Nama"], "data" => $data];
            
        }
        unset($string[0]);  
        return $string;
    }

    // ============================================================================================================= //

    public function kunjungTahunini($siteId) {
        $this->db->select('COUNT(codekanal) AS Jumlah');
        $this->db->from('logview');
        $this->db->where('siteid', $siteId);
        $this->db->where('YEAR(create_at) = YEAR(CURRENT_DATE())');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function kunjungBulanini($siteId) {
        $this->db->select('COUNT(codekanal) AS Jumlah');
        $this->db->from('logview');
        $this->db->where('siteid', $siteId);
        $this->db->where('MONTH(create_at) = MONTH(CURRENT_DATE())');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function kunjungHariini($siteId) {
        $this->db->select('COUNT(codekanal) AS Jumlah');
        $this->db->from('logview');
        $this->db->where('siteid', $siteId);
        $this->db->where('DATE(create_at) = CURRENT_DATE()');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function akhirUpdate($siteId) {
        $this->db->select('site_id, DATE_FORMAT(max(updated_at), "%d-%m-%Y") AS Waktu');
        $this->db->from('content');
        $this->db->where('site_id', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function totalContent($siteId) {
        $this->db->select('COUNT(*) AS Total');
        $this->db->from('content');
        $this->db->where('site_id', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungBerita($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('content');
        $this->db->where('site_id', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungGaleri($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('gallery');
        $this->db->where('SiteId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungKontak($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('contact');
        $this->db->where('SiteId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungAgenda($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('event');
        $this->db->where('SiteId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function hitungExlink($siteId) {  
        $this->db->select('count(*) as jumlah'); 
        $this->db->from('externalLink');
        $this->db->where('SiteId', $siteId);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function lineBulan($siteId) {
        $query = "SELECT l.codekanal AS Kanal, COUNT(*) AS Jumlah, j.Jumlah as Januari, f.Jumlah as Februari, r.Jumlah as Maret, p.Jumlah as April, m.Jumlah as Mei, u.Jumlah as Juni, i.Jumlah as Juli, g.Jumlah as Agustus, s.Jumlah as September, o.Jumlah as Oktober, n.Jumlah as November, d.Jumlah as Desember
    FROM logview l
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 1 GROUP BY codekanal) j ON j.siteid = l.siteid AND j.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 2 GROUP BY codekanal) f ON f.siteid = l.siteid AND f.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 3 GROUP BY codekanal) r ON r.siteid = l.siteid AND r.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 4 GROUP BY codekanal) p ON p.siteid = l.siteid AND p.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 5 GROUP BY codekanal) m ON m.siteid = l.siteid AND m.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 6 GROUP BY codekanal) u ON u.siteid = l.siteid AND u.codekanal = l.codekanal

    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 7 GROUP BY codekanal) i ON i.siteid = l.siteid AND i.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 8 GROUP BY codekanal) g ON g.siteid = l.siteid AND g.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 9 GROUP BY codekanal) s ON s.siteid = l.siteid AND s.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 10 GROUP BY codekanal) o ON o.siteid = l.siteid AND o.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 11 GROUP BY codekanal) n ON n.siteid = l.siteid AND n.codekanal = l.codekanal
    LEFT JOIN (SELECT codekanal, siteid, COUNT(*) AS Jumlah FROM `logview` WHERE siteid = ".$siteId." AND MONTH(create_at) = 12 GROUP BY codekanal) d ON d.siteid = l.siteid AND d.codekanal = l.codekanal
    WHERE l.siteid = ".$siteId." AND YEAR(l.create_at) = YEAR(CURRENT_DATE())
    GROUP BY l.codekanal";

        $hasil = $this->db->query($query)->result_array();


        $string[] = "";
        for ($i=0; $i < count($hasil); $i++) { 
            $januari  = isset($hasil[$i]["Januari"]) ? (integer)$hasil[$i]["Januari"]  : 0;
            $februari = isset($hasil[$i]["Februari"])? (integer)$hasil[$i]["Februari"] : 0;
            $maret    = isset($hasil[$i]["Maret"])   ? (integer)$hasil[$i]["Maret"]    : 0;
            $april    = isset($hasil[$i]["April"])   ? (integer)$hasil[$i]["April"]    : 0;
            $mei      = isset($hasil[$i]["Mei"])     ? (integer)$hasil[$i]["Mei"]      : 0;
            $juni     = isset($hasil[$i]["Juni"])    ? (integer)$hasil[$i]["Juni"]     : 0;
            $juli     = isset($hasil[$i]["Juli"])    ? (integer)$hasil[$i]["Juli"]     : 0;
            $agustus  = isset($hasil[$i]["Agustus"]) ? (integer)$hasil[$i]["Agustus"]  : 0;
            $september= isset($hasil[$i]["September"])? (integer)$hasil[$i]["September"] : 0;
            $oktober  = isset($hasil[$i]["Oktober"]) ? (integer)$hasil[$i]["Oktober"]  : 0;
            $november = isset($hasil[$i]["November"])? (integer)$hasil[$i]["November"] : 0;
            $desember = isset($hasil[$i]["Desember"])? (integer)$hasil[$i]["Desember"] : 0;

            $data = [$januari,$februari,$maret,$april,$mei,$juni,$juli,$agustus,$september,$oktober,$november,$desember];

            $string[] = ["name" => $hasil[$i]["Kanal"], "data" => $data];
            
        }
        unset($string[0]);  
        return $string;

    }

    public function linekunjungKanal($siteId) {
        $query = "SELECT month.namemonth, IFNULL( b.Jumlah, 0 ) as Jumlah
        FROM (
            SELECT 1 AS month, 'Jan' AS namemonth
            UNION SELECT 2 AS month, 'Feb' AS namemonth
            UNION SELECT 3 AS month, 'Mar' AS namemonth
            UNION SELECT 4 AS month, 'Apr' AS namemonth
            UNION SELECT 5 AS month, 'May' AS namemonth
            UNION SELECT 6 AS month, 'Jun' AS namemonth
            UNION SELECT 7 AS month, 'Jul' AS namemonth
            UNION SELECT 8 AS month, 'Aug' AS namemonth
            UNION SELECT 9 AS month, 'Sep' AS namemonth
            UNION SELECT 10 AS month, 'Oct' AS namemonth
            UNION SELECT 11 AS month, 'Nov' AS namemonth
            UNION SELECT 12 AS month, 'Des' AS namemonth
        ) as month
        LEFT JOIN (SELECT COUNT(*) as Jumlah, month(create_at) AS Bulan FROM logview WHERE siteid = ".$siteId." AND YEAR(create_at) = YEAR(CURRENT_DATE()) GROUP by Bulan) b ON b.Bulan = month.month";

        $hasil = $this->db->query($query)->result_array();
        return $hasil;
    }
}
