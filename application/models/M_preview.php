<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_preview extends CI_Model {

	public function jumlah_data($site_id, $type_id) {
		$this->db->select('*');
		$this->db->from('content');
		if (!empty($site_id)) {
			$this->db->where('site_id', $site_id);
		}
		if (!empty($type_id)) {
			$this->db->where('type_id', $type_id);
		}
		$query = $this->db->get()->num_rows();
		return $query;
	}
	public function v_list_berita($site_id, $type_id, $limit, $offset) {
		$this->db->select('content.* , category.Description as kategori');
		$this->db->from('content');
		$this->db->join('category', 'category.Id = content.category_id', 'left');

		if (!empty($site_id)) {
			$this->db->where('site_id', $site_id);
		}
		if (!empty($type_id)) {
			$this->db->where('type_id', $type_id);
		}
		if ($limit != '') {
			$this->db->limit($limit,$offset);
		}
		$query = $this->db->get()->result_array();
		return $query;
	}
	    public function site($site_id) {
        $this->db->select('*');
        $this->db->from('site');
        $this->db->where('Id',$site_id);
        $query = $this->db->get()->row();
        return $query;
    }
     public function v_d_berita($slug_title, $site_id, $type_id, $organisasi_id) {
        $this->db->order_by('content.created_at', 'desc');
        $this->db->select('content.* , reference.Description as type_name, category.Description as kategori');
        // $this->cms->join('users', 'users.users_id = content.author', 'left');
        $this->db->join('reference', 'reference.Id = content.type_id', 'left');
        $this->db->join('category', 'category.Id = content.category_id', 'left');
        $this->db->where('content.site_id', $site_id);
        $this->db->where('content.organisasi_id', $organisasi_id);
        $this->db->where('content.type_id', $type_id);
        $this->db->where('content.slug_title', $slug_title);
        $this->db->from('content');
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function v_ar_berita($content_id, $site_id, $type_id, $organisasi_id) {
        $this->db->order_by('content.created_at', 'desc');
        $this->db->select('content.* , reference.Description as type_name, category.Description as kategori');
        // $this->cms->join('users', 'users.users_id = content.author', 'left');
        $this->db->join('reference', 'reference.Id = content.type_id', 'left');
        $this->db->join('category', 'category.Id = content.category_id', 'left');
        $this->db->where('content.site_id', $site_id);
        $this->db->where('content.organisasi_id', $organisasi_id);
        $this->db->where('content.type_id', $type_id);
        $this->db->from('content');
        if (!empty($content_id)) {
            $this->db->where('content.content_id', $content_id);
        }
        if (empty($content_id)) {
            $this->db->limit(3);   
        }
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function v_k_berita($slug_title, $site_id, $type_id, $organisasi_id) {
        $this->db->order_by('category.Description', 'asc');
        $this->db->group_by('category.Description');
        $this->db->select('category.Description as kategori, count(category.Description) as jml');
        $this->db->join('reference', 'reference.Id = content.type_id', 'left');
        $this->db->join('category', 'category.Id = content.category_id', 'left');
        $this->db->where('content.site_id', $site_id);
        $this->db->where('content.organisasi_id', $organisasi_id);
        $this->db->where('content.type_id', $type_id);
        $this->db->from('content');

        $query = $this->db->get()->result_array();
        return $query;
    }
    public function exlink($siteId, $type)
    {
        $query = $this->db->query("SELECT Code FROM externalLink where TypeId = '".$type."' ORDER BY Id DESC LIMIT 1;")->row();
        return $query;
    }

}

/* End of file M_preview.php */
/* Location: .//C/Users/amiana/AppData/Local/Temp/fz3temp-2/M_preview.php */