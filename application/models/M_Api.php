<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Api extends CI_Model {

	function __construct(){
		parent::__construct();
		$CI = &get_instance();      // sesuai dengan nama nya get_instance agar variable $CI dapat digunakan secara instan hehehehhe....
        
				$this->databaseDW = $CI->load->database('databaseDW', TRUE);


	}


	public function get_all($Nip,$instansi,$jabatan,$limit,$offset) {
		
		$tahun = date("Y");
		$this->databaseDW->select('d_pegawai.NIP,d_pegawai.nama_pegawai,d_pegawai.jabatan,d_pegawai.instansi, d_opd.alamatopd,d_opd.notelp');
		$this->databaseDW->from('f_pegawai');
		$this->databaseDW->join('d_opd', 'd_opd.key_opd = f_pegawai.key_opd');
		$this->databaseDW->join('d_pegawai', 'd_pegawai.key_pegawai = f_pegawai.key_pegawai');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		$this->databaseDW->where('year(d_pegawai.tgl_ubah)', $tahun);
		$this->databaseDW->where('d_pegawai.status_pensiun','Pegawai Negeri Sipil');

	
		// $this->db->limit($limit, $offset);

		if (!empty($instansi)) {
			$this->databaseDW->where('d_pegawai.instansi',$instansi);
		}

		if (!empty($jabatan)) {
			$this->databaseDW->where('d_pegawai.jabatan',$jabatan);
		}

		if (!empty($Nip)) {
			$this->databaseDW->where('d_pegawai.NIP',$Nip);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}

	public function get_instansi() {
		
		$tahun = date("Y");
		$this->databaseDW->select('d_pegawai.instansi');
		$this->databaseDW->from('f_pegawai');
		$this->databaseDW->join('d_opd', 'd_opd.key_opd = f_pegawai.key_opd');
		$this->databaseDW->join('d_pegawai', 'd_pegawai.key_pegawai = f_pegawai.key_pegawai');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		$this->databaseDW->where('year(d_pegawai.tgl_ubah)', $tahun);
		$this->databaseDW->where('d_pegawai.status_pensiun','Pegawai Negeri Sipil');

	
		// $this->db->limit($limit, $offset);

		// if (!empty($instansi)) {
		// 	$this->databaseDW->where('d_pegawai.instansi',$instansi);
		// }

		// if (!empty($jabatan)) {
		// 	$this->databaseDW->where('d_pegawai.jabatan',$jabatan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('d_pegawai.instansi');
		$query = $this->databaseDW->get()->result_array();
		return $query;
	}
	
	public function get_penduduk($Kecamatan,$Kelurahan,$dimensi ,$subdimensi,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_aggregate_capil.dimensi,d_aggregate_capil.subdimensi,d_aggregate_capil.kecamatan,d_aggregate_capil.kelurahan, d_aggregate_capil.jumlah ');
		$this->databaseDW->from('f_aggregate_capil');
		$this->databaseDW->join('d_aggregate_capil', 'd_aggregate_capil.key_agg = f_aggregate_capil.key_aggregate');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		// $this->databaseDW->group_by('d_aggregate_capil.dimensi,d_aggregate_capil.subdimensi,d_aggregate_capil.kecamatan,d_aggregate_capil.kelurahan')
		$this->databaseDW->where('d_aggregate_capil.tahun', '2019');
		$this->databaseDW->where_not_in('d_aggregate_capil.subdimensi ', 'Persentase' , 'PERSENTASE (%)');

	
		// $this->db->limit($limit, $offset);

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		}

		
		if (!empty($dimensi)) {
			$this->databaseDW->where('d_aggregate_capil.dimensi',$dimensi);
		}

		if (!empty($subdimensi)) {
			$this->databaseDW->where('d_aggregate_capil.subdimensi',$subdimensi);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}
	public function get_penduduk_sum($Kecamatan,$Kelurahan,$dimensi ,$subdimensi,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('sum(d_aggregate_capil.jumlah) Total');
		$this->databaseDW->from('f_aggregate_capil');
		$this->databaseDW->join('d_aggregate_capil', 'd_aggregate_capil.key_agg = f_aggregate_capil.key_aggregate');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		// $this->databaseDW->group_by('d_aggregate_capil.dimensi,d_aggregate_capil.subdimensi,d_aggregate_capil.kecamatan,d_aggregate_capil.kelurahan')
		$this->databaseDW->where('d_aggregate_capil.tahun', '2019');
		$this->databaseDW->where_not_in('d_aggregate_capil.subdimensi ', 'Persentase' , 'PERSENTASE (%)');

	
		// $this->db->limit($limit, $offset);

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		}

		
		if (!empty($dimensi)) {
			$this->databaseDW->where('d_aggregate_capil.dimensi',$dimensi);
		}

		if (!empty($subdimensi)) {
			$this->databaseDW->where('d_aggregate_capil.subdimensi',$subdimensi);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}
	public function get_pendudukMaster($Kecamatan,$Kelurahan,$dimensi ,$subdimensi,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_aggregate_capil.dimensi');
		$this->databaseDW->from('f_aggregate_capil');
		$this->databaseDW->join('d_aggregate_capil', 'd_aggregate_capil.key_agg = f_aggregate_capil.key_aggregate');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		// $this->databaseDW->group_by('d_aggregate_capil.dimensi,d_aggregate_capil.subdimensi,d_aggregate_capil.kecamatan,d_aggregate_capil.kelurahan')
		$this->databaseDW->where('d_aggregate_capil.tahun', '2019');
		$this->databaseDW->where_not_in('d_aggregate_capil.subdimensi ', 'Persentase' , 'PERSENTASE (%)');


	
		// $this->db->limit($limit, $offset);

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		}

		
		if (!empty($dimensi)) {
			$this->databaseDW->where('d_aggregate_capil.dimensi',$dimensi);
		}

		if (!empty($subdimensi)) {
			$this->databaseDW->where('d_aggregate_capil.subdimensi',$subdimensi);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('d_aggregate_capil.dimensi',TRUE);
		$query = $this->databaseDW->get()->result_array();
		return $query;
	}
	public function get_pendudukMasterSubdimensi($Kecamatan,$Kelurahan,$dimensi ,$subdimensi,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_aggregate_capil.subdimensi');
		$this->databaseDW->from('f_aggregate_capil');
		$this->databaseDW->join('d_aggregate_capil', 'd_aggregate_capil.key_agg = f_aggregate_capil.key_aggregate');
		// $this->databaseDW->like('d_pegawai.nama_pegawai',$isian);
		// $this->databaseDW->group_by('d_aggregate_capil.dimensi,d_aggregate_capil.subdimensi,d_aggregate_capil.kecamatan,d_aggregate_capil.kelurahan')
		$this->databaseDW->where('d_aggregate_capil.tahun', '2019');
		$this->databaseDW->where_not_in('d_aggregate_capil.subdimensi ', 'Persentase' , 'PERSENTASE (%)');


	
		// $this->db->limit($limit, $offset);

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		}

		
		if (!empty($dimensi)) {
			$this->databaseDW->where('d_aggregate_capil.dimensi',$dimensi);
		}

		if (!empty($subdimensi)) {
			$this->databaseDW->where('d_aggregate_capil.subdimensi',$subdimensi);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('d_aggregate_capil.subdimensi',TRUE);
		$query = $this->databaseDW->get()->result_array();
		return $query;
	}

	public function get_kunjungan($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_kunjungan_baru');
		$this->databaseDW->join('d_kunjungan_baru dkb', 'dkb.key_loket  = f_kunjungan_baru.key_kunjungan_loket');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_kunjungan_baru.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_kunjungan_baru.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_kunjungan_baru.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,dtgl.tahun');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}

	public function get_kunjunganJK($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dkb.kelamin as JenisKelamin,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_kunjungan_baru');
		$this->databaseDW->join('d_kunjungan_baru dkb', 'dkb.key_loket  = f_kunjungan_baru.key_kunjungan_loket');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_kunjungan_baru.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_kunjungan_baru.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_kunjungan_baru.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,JenisKelamin,dtgl.tahun');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}

	public function get_kunjunganPoli($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dkb.sub_fasilitas as Poli,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_kunjungan_baru');
		$this->databaseDW->join('d_kunjungan_baru dkb', 'dkb.key_loket  = f_kunjungan_baru.key_kunjungan_loket');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_kunjungan_baru.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_kunjungan_baru.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_kunjungan_baru.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,Poli,dtgl.tahun');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}

	public function get_kunjunganJenisBayar($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dkb.jenis_bayar as JenisBayar,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_kunjungan_baru');
		$this->databaseDW->join('d_kunjungan_baru dkb', 'dkb.key_loket  = f_kunjungan_baru.key_kunjungan_loket');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_kunjungan_baru.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_kunjungan_baru.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_kunjungan_baru.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,JenisBayar,dtgl.tahun');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	

	public function get_kunjunganumur($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dkb.umur as Umur,dkb.kel_umur as KelompokUmur,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_kunjungan_baru');
		$this->databaseDW->join('d_kunjungan_baru dkb', 'dkb.key_loket  = f_kunjungan_baru.key_kunjungan_loket');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_kunjungan_baru.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_kunjungan_baru.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_kunjungan_baru.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,Umur,KelompokUmur,dtgl.tahun');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	

	public function get_kunjunganpenyakit($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dkb.diagnosa as Diagnosa,dkb.penyakit as KelompokPenyakit,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_diagnosa');
		$this->databaseDW->join('d_diagnosa dkb', 'dkb.key_kunjungan  = f_diagnosa.key_kunjungan');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_diagnosa.key_lokasi');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_diagnosa.key_tgl_kunjung');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_diagnosa.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,Diagnosa,KelompokPenyakit,dtgl.tahun');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}		

	public function get_NakesJK($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dsd.status as Status,dsd.gender as JenisKelamin,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_sdm');
		$this->databaseDW->join('d_sdm_kesehatan dsd', 'dsd.key_sdm  = f_sdm.key_sdm');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_sdm.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_sdm.key_tanggal');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_sdm.key_fasilitas');
		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,Status,JenisKelamin,dtgl.tahun');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	

	public function get_NakesJenisTenaga($Kecamatan,$Kelurahan,$Tahun,$Bulan,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,dfaskes.jenis as JenisFaskes,dfaskes.nama as FasKes,dsd.status as Status,djenkes.nama as TenagaKesehatan,djenkes.jenis as JenisTenagaKesehatan,djenkes.subjenis as KelompokTenagaKesehatan,dtgl.tahun,count(*) as jumlah');
		$this->databaseDW->from('f_sdm');
		$this->databaseDW->join('d_sdm_kesehatan dsd', 'dsd.key_sdm  = f_sdm.key_sdm');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = f_sdm.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = f_sdm.key_tanggal');
		$this->databaseDW->join('d_fasilitas dfaskes', 'dfaskes.key_fasilitas  = f_sdm.key_fasilitas');
		$this->databaseDW->join('d_jenis_tenaga djenkes', 'djenkes.key_jenistenaga  = f_sdm.key_jenis');

		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Tahun)) {
			$this->databaseDW->where('dtgl.tahun',$Tahun);
		}

		if (!empty($Bulan)) {
			$this->databaseDW->where('dtgl.bulan',$Bulan);
		}

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dwil.kd_kecamatan,dwil.kecamatan,dwil.kd_kelurahan,dwil.kelurahan,JenisFaskes,FasKes,Status,TenagaKesehatan,JenisTenagaKesehatan,KelompokTenagaKesehatan,dtgl.tahun');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	

	public function get_Sekolah($Kecamatan,$Kelurahan,$Tahun,$Jenjang,$Semester,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dsd.jenjang,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ,count(*) as jumlah');
		$this->databaseDW->from('f_sekolah_dapodik fs');
		$this->databaseDW->join('d_sekolah_dapodik dsd', 'dsd.key_sekolah  = fs.key_sekolah');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = fs.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = fs.key_tanggal');
		$this->databaseDW->join('d_semester dsmester', 'dsmester.key_semester  = fs.key_semester');


		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Jenjang)) {
			$this->databaseDW->where('dsd.jenjang',$Jenjang);
		}
		
		
		if (!empty($Tahun)) {
			$this->databaseDW->where('dsmester.TahunajaranId',$Tahun);
		} else{
			$this->databaseDW->where('dsmester.TahunajaranId = (SELECT MAX(d_semester.TahunajaranId) from d_semester)');

		}
		

		if (!empty($Semester)) {
			$this->databaseDW->where('dsmester.SemesterAjaran',$Semester);
		} 

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dsd.jenjang,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	

	public function get_Sekolah_sum($Kecamatan,$Kelurahan,$Tahun,$Jenjang,$Semester,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('count(*) as jumlah');
		$this->databaseDW->from('f_sekolah_dapodik fs');
		$this->databaseDW->join('d_sekolah_dapodik dsd', 'dsd.key_sekolah  = fs.key_sekolah');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = fs.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = fs.key_tanggal');
		$this->databaseDW->join('d_semester dsmester', 'dsmester.key_semester  = fs.key_semester');


		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Jenjang)) {
			$this->databaseDW->where('dsd.jenjang',$Jenjang);
		}
		
		
		if (!empty($Tahun)) {
			$this->databaseDW->where('dsmester.TahunajaranId',$Tahun);
		} else{
			$this->databaseDW->where('dsmester.TahunajaranId = (SELECT MAX(d_semester.TahunajaranId) from d_semester)');

		}
		

		if (!empty($Semester)) {
			$this->databaseDW->where('dsmester.SemesterAjaran',$Semester);
		} 

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		// $this->databaseDW->group_by('dsd.jenjang,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	
	public function get_SekolahAkreditasi($Kecamatan,$Kelurahan,$Tahun,$Jenjang,$Semester,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dsd.jenjang,dsd.akreditasi,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ,count(*) as jumlah');
		$this->databaseDW->from('f_sekolah_dapodik fs');
		$this->databaseDW->join('d_sekolah_dapodik dsd', 'dsd.key_sekolah  = fs.key_sekolah');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = fs.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = fs.key_tanggal');
		$this->databaseDW->join('d_semester dsmester', 'dsmester.key_semester  = fs.key_semester');


		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Jenjang)) {
			$this->databaseDW->where('dsd.jenjang',$Jenjang);
		}
		
		
		if (!empty($Tahun)) {
			$this->databaseDW->where('dsmester.TahunajaranId',$Tahun);
		} else{
			$this->databaseDW->where('dsmester.TahunajaranId = (SELECT MAX(d_semester.TahunajaranId) from d_semester)');

		}
		

		if (!empty($Semester)) {
			$this->databaseDW->where('dsmester.SemesterAjaran',$Semester);
		} 

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dsd.jenjang,dsd.akreditasi,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	
	public function get_SekolahKepemilikan($Kecamatan,$Kelurahan,$Tahun,$Jenjang,$Semester,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dsd.jenjang,dsd.statuskepemilikan,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ,count(*) as jumlah');
		$this->databaseDW->from('f_sekolah_dapodik fs');
		$this->databaseDW->join('d_sekolah_dapodik dsd', 'dsd.key_sekolah  = fs.key_sekolah');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = fs.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = fs.key_tanggal');
		$this->databaseDW->join('d_semester dsmester', 'dsmester.key_semester  = fs.key_semester');


		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Jenjang)) {
			$this->databaseDW->where('dsd.jenjang',$Jenjang);
		}
		
		
		if (!empty($Tahun)) {
			$this->databaseDW->where('dsmester.TahunajaranId',$Tahun);
		} else{
			$this->databaseDW->where('dsmester.TahunajaranId = (SELECT MAX(d_semester.TahunajaranId) from d_semester)');

		}
		

		if (!empty($Semester)) {
			$this->databaseDW->where('dsmester.SemesterAjaran',$Semester);
		} 

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dsd.jenjang,dsd.statuskepemilikan,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}	
	public function get_SekolahKebutuhanKhusus($Kecamatan,$Kelurahan,$Tahun,$Jenjang,$Semester,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('dsd.jenjang,dsd.kebutuhan_khusus,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan ,count(*) as jumlah');
		$this->databaseDW->from('f_sekolah_dapodik fs');
		$this->databaseDW->join('d_sekolah_dapodik dsd', 'dsd.key_sekolah  = fs.key_sekolah');
		$this->databaseDW->join('d_wilayah dwil', 'dwil.key_wilayah  = fs.key_wilayah');
		$this->databaseDW->join('d_tanggal dtgl', 'dtgl.key_tanggal  = fs.key_tanggal');
		$this->databaseDW->join('d_semester dsmester', 'dsmester.key_semester  = fs.key_semester');


		// $this->databaseDW->where('dtgl.tahun', '2020');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($Kecamatan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kecamatan',$Kecamatan);
		// }

		if (!empty($Kecamatan)) {
			$this->databaseDW->where('dwil.kecamatan',$Kecamatan);
		}

		if (!empty($Kelurahan)) {
			$this->databaseDW->where('dwil.kelurahan',$Kelurahan);
		}

		if (!empty($Jenjang)) {
			$this->databaseDW->where('dsd.jenjang',$Jenjang);
		}
		
		
		if (!empty($Tahun)) {
			$this->databaseDW->where('dsmester.TahunajaranId',$Tahun);
		} else{
			$this->databaseDW->where('dsmester.TahunajaranId = (SELECT MAX(d_semester.TahunajaranId) from d_semester)');

		}
		

		if (!empty($Semester)) {
			$this->databaseDW->where('dsmester.SemesterAjaran',$Semester);
		} 

		// if (!empty($Kelurahan)) {
		// 	$this->databaseDW->where('d_aggregate_capil.kelurahan',$Kelurahan);
		// }

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		$this->databaseDW->group_by('dsd.jenjang,dsd.kebutuhan_khusus,dsmester.SemesterAjaran,dwil.Kecamatan, dwil.Kelurahan');
		$this->databaseDW->order_by('jumlah','DESC');

		$query = $this->databaseDW->get()->result_array();
		return $query;
	}
	
	public function get_siswa_sekolah($npsn,$limit,$offset){
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_JumlahSiswaSekolah($npsn,$thnAjaran,$semester,$limit,$offset){
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->join('d_semester','d_semester.key_semester = f_siswa_dpk.key_tahun_ajaran');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($thnAjaran)) {
			$this->databaseDW->where('d_semester.TahunajaranId',$thnAjaran);
		}
		if (!empty($semester)) {
			$this->databaseDW->where('d_semester.Semester',$semester);
		}
		// if (!empty($limit)) {
		// 	$this->databaseDW->limit($limit,$offset);
		// }

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			// foreach ($query->result_array() as $row) {
			// 	$data[] = $row;
			// }
			// $data[] = $query->num_rows();
		}

		return $query->num_rows();
	}
	public function get_siswa_all($npsn,$nik,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		// $this->databaseDW->where('d_siswa_dpk.nik IS NOT NULL');
		// $this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		// $this->db->limit($limit, $offset);

		if (!empty($nik)) {
			$this->databaseDW->where('d_siswa_dpk.nik',$nik);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit(1,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_siswa_dapodik($nik,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('d_siswa_dpk');
		$this->databaseDW->where('d_siswa_dpk.nik IS NOT NULL');
		// $this->db->limit($limit, $offset);

		if (!empty($nik)) {
			$this->databaseDW->where('d_siswa_dpk.nik',$nik);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit(1,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_siswa_detail($npsn,$nik,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_sekolah_dapodik.npsn, d_sekolah_dapodik.nama_sp,d_sekolah_dapodik.jenjang,d_sekolah_dapodik.status_sekolah,d_semester.SemesterAjaran ');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik', 'd_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->join('d_semester', 'd_semester.key_semester = f_siswa_dpk.key_tahun_ajaran');
		$this->databaseDW->where('d_siswa_dpk.nik IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
	
		// $this->db->limit($limit, $offset);
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($nik)) {
			$this->databaseDW->where('d_siswa_dpk.nik',$nik);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_siswa_nisn($npsn,$nisn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->where('d_siswa_dpk.nisn IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		// $this->db->limit($limit, $offset);

		if (!empty($nisn)) {
			$this->databaseDW->where('d_siswa_dpk.nisn',$nisn);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit(1,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_siswa_nisn_dapodik($nisn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_siswa_dpk.*');
		$this->databaseDW->from('d_siswa_dpk');
		$this->databaseDW->where('d_siswa_dpk.nisn IS NOT NULL');
		
		// $this->db->limit($limit, $offset);

		if (!empty($nisn)) {
			$this->databaseDW->where('d_siswa_dpk.nisn',$nisn);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit(1,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_siswa_detail_nisn($npsn,$nisn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_sekolah_dapodik.npsn, d_sekolah_dapodik.nama_sp,d_sekolah_dapodik.jenjang,d_sekolah_dapodik.status_sekolah,d_semester.SemesterAjaran ');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_siswa_dpk', 'd_siswa_dpk.key_siswa = f_siswa_dpk.key_siswa');
		$this->databaseDW->join('d_sekolah_dapodik', 'd_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->join('d_semester', 'd_semester.key_semester = f_siswa_dpk.key_tahun_ajaran');
		$this->databaseDW->where('d_siswa_dpk.nisn IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
	
		// $this->db->limit($limit, $offset);
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($nisn)) {
			$this->databaseDW->where('d_siswa_dpk.nisn',$nisn);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_guru_sekolah($npsn,$limit,$offset){
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_guru3.*');
		$this->databaseDW->from('f_guru_dapodik');
		$this->databaseDW->join('d_guru3', 'd_guru3.key_guru = f_guru_dapodik.key_guru');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_guru_dapodik.key_sekolah');
		$this->databaseDW->join('d_semester','d_semester.key_semester = f_guru_dapodik.key_semester');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_JumlahGuruSekolah($npsn,$thnAjaran,$semester,$limit,$offset){
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_guru3.*');
		$this->databaseDW->from('f_guru_dapodik');
		$this->databaseDW->join('d_guru3', 'd_guru3.key_guru = f_guru_dapodik.key_guru');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_guru_dapodik.key_sekolah');
		$this->databaseDW->join('d_semester', 'd_semester.key_semester = f_guru_dapodik.key_semester');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		if (!empty($thnAjaran)) {
			$this->databaseDW->where('d_semester.TahunajaranId',$thnAjaran);
		}
		if (!empty($semester)) {
			$this->databaseDW->where('d_semester.Semester',$semester);
		}
		// if (!empty($limit)) {
		// 	$this->databaseDW->limit($limit,$offset);
		// }

		$query = $this->databaseDW->get();
		$data = array();
		// if($query !== FALSE && $query->num_rows() > 0){
		// 	foreach ($query->result_array() as $row) {
		// 		$data[] = $row;
		// 	}
		// }

		return $query->num_rows();
	}
	public function get_guru_all($npsn,$nuptk,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_guru3.*');
		$this->databaseDW->from('f_guru_dapodik');
		$this->databaseDW->join('d_guru3', 'd_guru3.key_guru = f_guru_dapodik.key_guru');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = f_guru_dapodik.key_sekolah');
		$this->databaseDW->where('d_guru3.nuptk IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		// $this->db->limit($limit, $offset);
		if (!empty($nuptk)) {
			$this->databaseDW->where('d_guru3.nuptk',$nuptk);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_guru_dapodik($nuptk,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_guru3.*');
		$this->databaseDW->from('d_guru3');
		$this->databaseDW->where('d_guru3.nuptk IS NOT NULL');
		// $this->db->limit($limit, $offset);
		if (!empty($nuptk)) {
			$this->databaseDW->where('d_guru3.nuptk',$nuptk);
		}

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_guru_detail($npsn,$nuptk,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_sekolah_dapodik.npsn, d_sekolah_dapodik.nama_sp,d_sekolah_dapodik.jenjang,d_sekolah_dapodik.status_sekolah,d_semester.SemesterAjaran ');
		$this->databaseDW->from('f_guru_dapodik');
		$this->databaseDW->join('d_guru3', 'd_guru3.key_guru = f_guru_dapodik.key_guru');
		$this->databaseDW->join('d_sekolah_dapodik', 'd_sekolah_dapodik.key_sekolah = f_guru_dapodik.key_sekolah');
		$this->databaseDW->join('d_semester', 'd_semester.key_semester = f_guru_dapodik.key_semester');
		$this->databaseDW->where('d_guru3.nuptk IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}
		// $this->db->limit($limit, $offset);

		if (!empty($nuptk)) {
			$this->databaseDW->where('d_guru3.nuptk',$nuptk);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_rombel_all($npsn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_rombel_dapodik.*');
		$this->databaseDW->from('f_siswa_dpk');
		$this->databaseDW->join('d_rombel_dapodik','d_rombel_dapodik.key_rombel = f_siswa_dpk.key_rombel');
		$this->databaseDW->join('d_sekolah_dapodik','d_sekolah_dapodik.key_sekolah = d_rombel_dapodik.key_sekolah');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
	
		// $this->db->limit($limit, $offset);

		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}

		

		// if (!empty($limit)) {
		// 	$this->databaseDW->limit(1,$offset);
		// }

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $query->num_rows();
	}
	public function get_sekolah_list($jenjang,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->distinct();
		$this->databaseDW->select('d_sekolah_dapodik.*');
		$this->databaseDW->from('d_sekolah_dapodik');
		// $this->databaseDW->group_by('npsn');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
		$this->databaseDW->where('d_sekolah_dapodik.status_sekolah IS NOT NULL');
	
		// $this->db->limit($limit, $offset);

		// if (!empty($status_sekolah)) {
		// 	$this->databaseDW->where('d.sekolah_dapodik.status_sekolah','Swasta');
		// }
		// $this->databaseDW->where('d_sekolah_dapodik.status_sekolah','Swasta');
		if(!empty($jenjang)){
			$this->databaseDW->where('d_sekolah_dapodik.jenjang',$jenjang);
		}
		// $this->databaseDW->where_not_in('d_sekolah_dapodik.id_kecamatan','00.00.00');
		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}
		
		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	public function get_sekolah_all($npsn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_sekolah_dapodik.*');
		$this->databaseDW->from('d_sekolah_dapodik');
		$this->databaseDW->where('d_sekolah_dapodik.npsn IS NOT NULL');
	
		// $this->db->limit($limit, $offset);

		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.npsn',$npsn);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit(1,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result() as $row) {
				$data = $row;
			}
		}

		return $data;
	}
	public function get_sekolah_detail($npsn,$limit,$offset) {
		
		// $tahun = date("Y");
		$this->databaseDW->select('d_sekolah_dapodik.npsn, d_sekolah_dapodik.nama_sp,d_sekolah_dapodik.jenjang,d_sekolah_dapodik.status_sekolah,d_semester.SemesterAjaran ');
		$this->databaseDW->from('f_sekolah_dapodik');
		$this->databaseDW->join('d_sekolah_dapodik', 'd_sekolah_dapodik.key_sekolah = f_siswa_dpk.key_sekolah');
		$this->databaseDW->join('d_semester', 'd_semester.key_semester = f_siswa_dpk.key_tahun_ajaran');
		$this->databaseDW->where('d_siswa_dpk.nik IS NOT NULL');
	
		// $this->db->limit($limit, $offset);

		if (!empty($npsn)) {
			$this->databaseDW->where('d_sekolah_dapodik.nik',$npsn);
		}

		

		if (!empty($limit)) {
			$this->databaseDW->limit($limit,$offset);
		}

		$query = $this->databaseDW->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}


}
