<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_CMS extends CI_Model {
	public function getSite($siteId)
	{
		$this->db->select('*');
		$this->db->from('site');
		$this->db->where('Id', $siteId);
		$query = $this->db->get();
		return $query->result();
	}
	public function getSitedata($siteId)
	{
		$this->db->select('*');
		$this->db->from('site');
		$this->db->where('Id', $siteId);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getSiteAll()
	{
	$query = $this->db->get('site');
    return $query->result_array();
	}
	public function getRoleAll()
	{
	$query = $this->db->get('role');
    return $query->result_array();
	}
	public function getOrgAll()
	{
	$query = $this->db->get('organization');
    return $query->result_array();
	}
	public function getOrgDist()
	{
		$this->db->distinct();
		$this->db->select('Name');
		$this->db->from('organization');
		// $this->db->where('Id', $siteId);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getUser($id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('Id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	public function checkOrg($org, $site)
	{
			// $this->db->where("(NIK='$nik' OR Nama='$nama')", NULL, FALSE);


			if (!empty($org)) {
				$this->db->where("(Id='$org')", NULL, FALSE);
			}

			if (!empty($site)) {
				$this->db->where('SiteId',$site, NULL, FALSE);
			}
			$query = $this->db->get('organization');
			return $query->num_rows();
	}
	public function checkRole($role, $site,$iduser)
	{
			// $this->db->where("(NIK='$nik' OR Nama='$nama')", NULL, FALSE);


			if (!empty($org)) {
				$this->db->where("(RoleId='$role')", NULL, FALSE);
			}
			if (!empty($iduser)) {
				$this->db->where("(UserId='$iduser')", NULL, FALSE);
			}
			if (!empty($site)) {
				$this->db->where('SiteId',$site, NULL, FALSE);
			}
			$query = $this->db->get('userrole');
			return $query->num_rows();
	}
	public function checkMembers($org, $site,$iduser)
	{
			// $this->db->where("(NIK='$nik' OR Nama='$nama')", NULL, FALSE);


			if (!empty($org)) {
				$this->db->where("(OrganizationId='$org')", NULL, FALSE);
			}
			if (!empty($iduser)) {
				$this->db->where("(UserId='$iduser')", NULL, FALSE);
			}
			if (!empty($site)) {
				$this->db->where('SiteId',$site, NULL, FALSE);
			}
			$query = $this->db->get('member');
			return $query->num_rows();
	}
	public function query_list( $kanal, $site_id, $organisasi_id, $content_id)
	{
		$this->db->select('content.*, tl.typelayanan, c.Description as CategoryName, kanal.*, u.Name as Author');
		$this->db->from('content');
		if (!empty($content_id)) {
			$this->db->where('content_id', $content_id);
		}
		$this->db->where('kanal.Id', $kanal);
		$this->db->where('content.site_id', $site_id);
		if (!empty($organisasi_id)) {
			$this->db->where('organisasi_id', $organisasi_id);
		}
		$this->db->join('user u', 'u.Id = content.author', 'left');
		$this->db->join('category c', 'c.Id = content.category_id', 'left');
		$this->db->join('typelayanan tl', 'tl.Id = content.JenisLayanan', 'left');
		$this->db->join('reference kanal', 'kanal.Id = content.type_id', 'left');
		$this->db->order_by('content_id', 'desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function datauserrole($start, $limit, $search, $filter_by,$idUser)
	{

		$this->db->select('user.Name as Nama,site.Code, site.Name as NamaSite, role.Name as NameRole, userrole.CreateDate');
		$this->db->from('userrole');
		$this->db->join('user', 'user.Id = userrole.UserId', 'left');
		$this->db->join('role', 'role.Id = userrole.RoleId', 'left');
		$this->db->join('site', 'site.Id = userrole.SiteId', 'left');
		$this->db->where('user.Id', $idUser);
		// $this->db->join
		// if (!empty($filter_by)) {
		// 	if (!empty($search)) {
		// 		if ($filter_by == 'Name') {
		// 			$this->db->like('Name', $search);
		// 		}else if ($filter_by == 'Email') {
		// 			$this->db->like('Email', $search);
		// 		}
		// 	}
		// }
		// $this->db->group_by("IdKasus");
		$sql = $this->db->get();
		$lastquery = $this->db->last_query();
		$querydata = $lastquery." Limit ".$limit." OFFSET ".$start;
		$count = $sql->num_rows();
		$query = $this->db->query($querydata)->result_array();
		//$this->db->limit($limit, $start);
		$dataarray = [
			'data' => $query,
			'draw' => '',
			'recordsFiltered' => $count,
			'recordsTotal' => $count,
		];
		return $dataarray;
	}
	public function datauserorg($start, $limit, $search, $filter_by,$idUser)
	{

		$this->db->select('user.Name as Nama,site.Code, site.Name as NamaSite, organization.Name as Org, member.CreateDate');
		$this->db->from('member');
		$this->db->join('user', 'user.Id = member.UserId', 'left');
		$this->db->join('organization', 'organization.Id = member.OrganizationId', 'left');
		$this->db->join('site', 'site.Id = member.SiteId', 'left');
		$this->db->where('user.Id', $idUser);
		$this->db->where('organization.Name IS NOT NULL', NULL, FALSE);
		// $this->db->join
		// if (!empty($filter_by)) {
		// 	if (!empty($search)) {
		// 		if ($filter_by == 'Name') {
		// 			$this->db->like('Name', $search);
		// 		}else if ($filter_by == 'Email') {
		// 			$this->db->like('Email', $search);
		// 		}
		// 	}
		// }
		// $this->db->group_by("IdKasus");
		$sql = $this->db->get();
		$lastquery = $this->db->last_query();
		$querydata = $lastquery." Limit ".$limit." OFFSET ".$start;
		$count = $sql->num_rows();
		$query = $this->db->query($querydata)->result_array();
		//$this->db->limit($limit, $start);
		$dataarray = [
			'data' => $query,
			'draw' => '',
			'recordsFiltered' => $count,
			'recordsTotal' => $count,
		];
		return $dataarray;
	}
	public function datauser($start, $limit, $search, $filter_by)
	{

		$this->db->select('*');
		$this->db->from('user');
		if (!empty($filter_by)) {
			if (!empty($search)) {
				if ($filter_by == 'Name') {
					$this->db->like('Name', $search);
				}else if ($filter_by == 'Email') {
					$this->db->like('Email', $search);
				}
			}
		}
		// $this->db->group_by("IdKasus");
		$sql = $this->db->get();
		$lastquery = $this->db->last_query();
		$querydata = $lastquery." Limit ".$limit." OFFSET ".$start;
		$count = $sql->num_rows();
		$query = $this->db->query($querydata)->result_array();
		//$this->db->limit($limit, $start);
		$dataarray = [
			'data' => $query,
			'draw' => '',
			'recordsFiltered' => $count,
			'recordsTotal' => $count,
		];
		return $dataarray;
	}
	public function datamenu($start, $limit, $search)
	{
		$this->db->select('*');
		$this->db->from('menu');
/*		if (!empty($filter_by)) {
			if (!empty($search)) {
				if ($filter_by == 'Name') {
					$this->db->like('Name', $search);
				}else if ($filter_by == 'Email') {
					$this->db->like('Email', $search);
				}
			}
		}*/
		// $this->db->group_by("IdKasus");
		$sql = $this->db->get();
		$lastquery = $this->db->last_query();
		$querydata = $lastquery." Limit ".$limit." OFFSET ".$start;
		$count = $sql->num_rows();
		$query = $this->db->query($querydata)->result_array();
		//$this->db->limit($limit, $start);
		$dataarray = [
			'data' => $query,
			'draw' => '',
			'recordsFiltered' => $count,
			'recordsTotal' => $count,
		];
		return $dataarray;
	}
	public function datarole($start, $limit, $search)
	{
		$this->db->select('*');
		$this->db->from('role');
/*		if (!empty($filter_by)) {
			if (!empty($search)) {
				if ($filter_by == 'Name') {
					$this->db->like('Name', $search);
				}else if ($filter_by == 'Email') {
					$this->db->like('Email', $search);
				}
			}
		}*/
		// $this->db->group_by("IdKasus");
		$sql = $this->db->get();
		$lastquery = $this->db->last_query();
		$querydata = $lastquery." Limit ".$limit." OFFSET ".$start;
		$count = $sql->num_rows();
		$query = $this->db->query($querydata)->result_array();
		//$this->db->limit($limit, $start);
		$dataarray = [
			'data' => $query,
			'draw' => '',
			'recordsFiltered' => $count,
			'recordsTotal' => $count,
		];
		return $dataarray;
	}
	public function datarolemenu($id='')
	{
		$this->db->select('*');
		$this->db->from('rolemenu');
		$this->db->join('menu','rolemenu.MenuId=menu.Id');
		if (!empty($id)) {
			$this->db->where('RoleId', $id);
		}
		$this->db->group_by('menu.Id');
		$query = $this->db->get();
		return $query;
	}
	public function getMenu()
	{
		$this->db->select('*');
		$this->db->from('menu');
		$query = $this->db->get();
		return $query->result();
	}
	public function updateMenu($table, $data, $where)
	{
		$this->db->where('Id', $where);
		$this->db->update($table, $data);
	}	
	public function hapusMenu($table,$id){
		$this->db->where('Id', $id);
		return $this->db->delete($table);
	}
	public function editMenu($id)
  	{
	    $this->db->where('Id', $id);
	    $data = $this->db->get('menu');
	    return $data;
  	}
  	public function getRole($id)
  	{
	    $this->db->where('Id', $id);
	    $data = $this->db->get('role');
	    return $data;
  	}
  	public function updateRole($table, $data, $where)
	{
		$this->db->where('Id', $where);
		$this->db->update($table, $data);
	}	
	public function hapusRole($table,$id){
		$this->db->where('Id', $id);
		return $this->db->delete($table);
	}
	public function editRole($id)
  	{
	    $this->db->where('Id', $id);
	    $data = $this->db->get('role');
	    return $data;
  	}
	public function getRoleMenu($id)
	{
		$this->db->select('*');
		$this->db->from('rolemenu');
		$this->db->join('menu','rolemenu.MenuId=menu.Id');
		$this->db->where('RoleId',$id);
		$data = $this->db->get();
		return $data;
	}
	public function getView($SiteId, $table, $Id)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('SiteId', $SiteId);
		$this->db->order_by($Id, 'desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_organization($Id)
	{
		$this->db->select('*');
		$this->db->from('organization');
		$this->db->where('Id', $Id);
		$this->db->order_by('Id', 'desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_orgName($Id)
	{
		$this->db->select('*');
		$this->db->from('organization');
		$this->db->where('Name', $Id);
		$this->db->order_by('Id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_org($Id)
	{
		$this->db->select('*');
		$this->db->from('organization');
		$this->db->where('Id', $Id);
		$this->db->order_by('Id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_orgId($Id,$site)
	{
		$this->db->select('*');
		$this->db->from('organization');
		$this->db->where('Id', $Id);
		$this->db->where('SiteId', $site);
		$this->db->order_by('Id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_where($table, $field,  $kanal)
	{
		$this->db->where($field, $kanal);
		$this->db->select('*');
		$this->db->from($table);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}
	public function viewUpdate($where, $table){
		return $this->db->get_where($table, $where);
	}
	public function get($table)
	{
		$this->db->select('*');
		$this->db->from($table);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}

	public function get_parentcontent($SiteId, $typeId)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('site_id', $SiteId);
		$this->db->where('type_id', $typeId);
		$sql = $this->db->get();
		return $sql;
	}
	public function parentcontent($SiteId, $typeId)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('site_id', $SiteId);
		$this->db->where('type_id', $typeId);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}
	public function get_category($SiteId)
	{
		$this->db->where('SiteId', $SiteId);
		$this->db->select('*');
		$this->db->from('category');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}
		public function get_categoryplace($SiteId)
	{
		//$this->db->where('SiteIsd', $SiteId);
		$this->db->select('*');
		$this->db->from('categoryplace');
		// $this->db->where('parent_id !=', 0);
		// $this->db->where('SiteId', $SiteId);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}	public function get_parentcategoryplace($SiteId)
	{
		//$this->db->where('SiteId', $SiteId);
		// $this->db->where('parent_id = ', 0);
		$this->db->select('*');
		$this->db->from('categoryplace');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;

	}
	public function get_site($table, $field,  $id)
	{
		$this->db->where($field, $id);
		$this->db->select('site_name');
		$this->db->from($table);

		$query = $this->db->get()->row();
		return $query;

	}
	public function get_contact($siteId)
	{
		$this->db->select('contact.*,jabatan.Id as jabatanId, jabatan.Jabatan AS nm');
		$this->db->from('contact');
		$this->db->join('jabatan', 'contact.Occupation = jabatan.Id', 'left');
		$this->db->where('contact.SiteId', $siteId);
		$this->db->order_by('Id', 'desc');
   //     $this->db->join('organization', 'organization.Id = contact.OrganizationId', 'left');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}

	public function get_all($table)
	{
		$this->db->select('*');
		$this->db->from($table);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_Reference($kanal)
	{
		$this->db->select('*');
		$this->db->from('reference');
		$this->db->where('Code', $kanal);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
	public function update($field, $id, $data, $table)
	{
		$this->db->where($field, $id);
		$this->db->update($table, $data);
	}
	public function tampil_data($siteId)
	{
		$sql = $this->db->query('SELECT `org`.`Id` , `org`.`Name` AS org_name, `org2`.`Name` AS parent_org FROM `organization` AS `org` LEFT JOIN `organization` AS `org2` ON `org`.`ParentId` = `org2`.`Id` where org.SiteId = '.$siteId.' order by org.Id desc')->result_array();
		return $sql;
	}
	public function tampil_site()
	{
		$sql = $this->db->query('SELECT `site`.`Id` , `site`.`Name` AS Name, `site`.`Domain`,`site`.`Description` ,`site2`.`Name` AS parent, `site`.`Code` FROM `site` AS `site` LEFT JOIN `site` AS `site2` ON `site`.`ParentId` = `site2`.`Id` order by site.Id desc')->result_array();
		return $sql;
	}
	public function hapus_site($id_delete){
		$this->db->where('Id', $id_delete);
		$this->db->delete('site');
		return;

	}
	public function hapus_organizations($id_delete){
		$this->db->where('Id', $id_delete);
		$this->db->delete('organization');
		return;

	}
	public function updateSite($idUpdate)
	{
		$this->db->select('*');
		$this->db->from('site');
		$this->db->where('Id', $idUpdate);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_slider($site)
	{
		$this->db->order_by('CreatedDate','ASC');
		$this->db->select('slider.*,TypeCode.Description as TypeName,StatusCode.Description as StatusName,GrupCode.Description as Code, Cr.Name');
		$this->db->from('slider');
		$this->db->where('SiteId',$site);
		$this->db->join('reference as TypeCode','slider.Type = TypeCode.Code','left');
		$this->db->join('reference as StatusCode','slider.Status = StatusCode.Code','left');
		$this->db->join('reference as GrupCode','slider.GroupSlider = GrupCode.Code','left');
		$this->db->join('user as Cr','slider.Creator = Cr.Id','left');
		$this->db->order_by('slider.Id', 'desc');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_gallery($site)
	{
		$this->db->select('gallery.*, Typecode.Description as MT');
		$this->db->from('gallery');
		$this->db->join('reference as Typecode', 'gallery.MediaType = Typecode.Code', 'left');
		$this->db->where('SiteId', $site);
		$this->db->order_by('Id', 'desc');


		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_place($site)
	{
		$this->db->select('place.*, Typecode.Description as TypeCode');
		$this->db->from('place');
		$this->db->join('reference as Typecode', 'place.TypeId = Typecode.Code', 'left');
		$this->db->where('SiteId', $site);
		$this->db->order_by('place.Id', 'desc');


		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function get_ExternalLink($siteId)
	{
		$sql = $this->db->query('SELECT ex1.*, ref.Description AS TypeName FROM externalLink AS ex1 LEFT JOIN reference AS ref ON ex1.TypeId= ref.Code where ex1.TitleMenu is not null and ex1.SiteId ='.$siteId.' order by Id desc')->result_array();
		return $sql;
	}
	public function get_parent($siteId, $typeId)
	{
		$this->db->select('ex1.*, ref.Description AS TypeName');
		$this->db->from('externalLink ex1');
		$this->db->join('reference ref', 'ex1.TypeId= ref.Code', 'left');
		$this->db->where('ex1.SiteId', $siteId);
		$this->db->where('ex1.TypeId', $typeId);
		$sql = $this->db->get();
		return $sql;
	}
	public function getCatExlink($SiteId, $categoryId)
	{
		$this->db->where('SiteId', $SiteId);
		$this->db->where('Id', $categoryId);
		$this->db->select('*');
		$this->db->from('category');
		$query = $this->db->get();
		return $query;

	}
	public function get_tags($siteId, $contentId)
	{
		$this->db->select('*');
		$this->db->from('tags');
		$this->db->where('SiteId', $siteId);
		$this->db->where('ContentId', $contentId);
		$query = $this->db->get()->result();
		return $query;
		
	}
	public function cekIndex($status,$group,$index, $siteId)
	{
		$this->db->select('*');
		$this->db->from('slider');
		$this->db->where('SiteId', $siteId);
		if (!empty($status)) {
			$this->db->where('Status', $status);
		}
		if (!empty($group)) {
			$this->db->where('GroupSlider', $group);
		}
		if (!empty($index)) {
			$this->db->where('Sorting', $index);
		}
		$query = $this->db->get();
		return $query;
		
	}
	public function countMaxIndex($status,$group, $siteId)
	{
		$this->db->select('count(*) as Sorting');
		$this->db->from('slider');
		$this->db->where('SiteId', $siteId);
		if (!empty($status)) {
			$this->db->where('Status', $status);
		}
		if (!empty($group)) {
			$this->db->where('GroupSlider', $group);
		}
		$query = $this->db->get();
		return $query;
		
	}
	public function getMessage($siteId, $start, $limit, $filter)
	{
		$this->db->select('messaging.*, reference.Description');
		$this->db->from('messaging');
		$this->db->join('reference', 'messaging.Category = reference.code', 'left');
		$this->db->where('SiteId', $siteId);
		if (!empty($filter) && $filter == 'read') {
			$this->db->where('ReadStatus', 1);
		}else if (!empty($filter) && $filter == 'none') {
			$this->db->where('ReadStatus', 0);
		}
		$tempdb = clone $this->db;
		$count = $tempdb->count_all_results();
		$this->db->limit($limit, $start);
		$query = $this->db->get()->result_array();

		$dataRes = [
			'data' => $query, //data query
			'total' => $count // total row
		];
		return $dataRes;
	}
	public function ReadMessage($siteId, $id)
	{
		$this->db->set('ReadStatus', 1);
		$this->db->where('id', $id);
		$this->db->where('SiteId', $siteId);
		$this->db->update('messaging');
	}
	public function DeleteMessage($siteId, $id)
	{
		$this->db->query('Delete from messaging where SiteId ='.$siteId.' and Id in ('.$id.')');
	}
	public function updatePassword($siteId, $Id, $passbaru)
	{
		$this->db->set('Password', $passbaru, FALSE);
		$this->db->where('Id', $Id);
		$this->db->where('SiteId', $siteId);
		$this->db->update('user');

		return true;
	}
	public function get_struktur($siteId)
	{
		$this->db->select('strukturorganisasi.*,jabatan.Id as jabatanId, jabatan.Jabatan AS nmJabatan');
		$this->db->from('strukturorganisasi');
		$this->db->join('jabatan', 'strukturorganisasi.JabatanId = jabatan.Id', 'left');
		$this->db->where('strukturorganisasi.SiteId', $siteId);
		$this->db->order_by('Id', 'desc');
   //     $this->db->join('organization', 'organization.Id = contact.OrganizationId', 'left');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}
	public function getParentOrganisasi($organisasiId)
	{
		$this->db->select('ParentId');
		$this->db->from('organization');
		$this->db->where('Id', $organisasiId);
		$query = $this->db->get()->row()->ParentId;
		return $query;
	}

		public function get_JenisLayanan($siteId)
	{
		$this->db->select('jabatan.Id, jabatan.Jabatan');
		$this->db->from('jabatan');
		$this->db->join('organization', 'organization.ParentId = jabatan.OwnerId', 'left');
		$this->db->where('organization.SiteId', $siteId);
		// $this->db->order_by('Id', 'desc');
   //     $this->db->join('organization', 'organization.Id = contact.OrganizationId', 'left');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[]=$row;
			} 
			$query->free_result();
		}
		else{
			$data=NULL;
		}
		return $data;
	}


}

/* End of file M_CMS.php */
/* Location: ./application/models/M_CMS.php */
