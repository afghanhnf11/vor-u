<?php
defined('BASEPATH') or exit('No direct script access allowed');

class B2B extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    private function load()
    {

        $page = array(
            "head" => $this->load->view('template-view/head', false, true),
            "preloader" => $this->load->view('template-view/preloader', false, true),
            "header" => $this->load->view('template-view/header_b2b', false, true),
            "main_js" => $this->load->view('template-view/main_js', false, true),
            "footer" => $this->load->view('template-view/footer', false, true)
        );
        // var_dump($page); die;
        return $page;
    }

    public function index()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('b2b', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function About()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('about', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function ProyekVorfund()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('proyek_vorfund', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function DetailVorfund()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('detail_vorfund', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function Subscribe()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('subscribe', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function SubscribePayment()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('subscribe_infobayar', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function SubscribePay()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('subscribe_bayar', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function SubscribeSuccess()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('subscribe_sudahbayar', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }

    public function Chat()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('chat', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }
}
