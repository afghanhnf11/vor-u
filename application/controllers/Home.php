<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    private function load()
    {

        $page = array(
            "head" => $this->load->view('template-view/head', false, true),
            "preloader" => $this->load->view('template-view/preloader', false, true),
            "header" => $this->load->view('template-view/header', false, true),
            "main_js" => $this->load->view('template-view/main_js', false, true),
            "footer" => $this->load->view('template-view/footer', false, true)
        );
        // var_dump($page); die;
        return $page;
    }

    public function index()
    {

        $path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('index', false, true)
        );
        $this->load->view('template-view/default_template', $data);
    }
}
