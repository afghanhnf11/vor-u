<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Report extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        // $CI = &get_instance();      // sesuai dengan nama nya get_instance agar variable $CI dapat digunakan secara instan hehehehhe....
       	// $this->sigap = $CI->load->database('sigap', TRUE);
        parent::__construct();
        $this->load->model('M_report'); 
         $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 200; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 100; // 50 requests per hour per user/key
    }

    public function getAllReport_post()
    {
        $reports = $this->M_report->getAllReportsActive();
        // var_dump($reports);
        foreach ($reports as $key => $value) {
            if(!empty($value->img_before)){
                $value->img_before = base_url()."assets/slider/".$value->img_before;
            }else{
                $value->img_before = base_url()."assets/slider/banner_1.jpg";
            }
        }
        $return = [
            'status' => true, 
            'data' => $reports, 
            'message' => ""
        ];
        

        $this->set_response($return, REST_Controller::HTTP_OK);
    } 
}
