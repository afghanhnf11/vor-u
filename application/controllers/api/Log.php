<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Log extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('M_log');
    }

    public function store_logcall_post()
    {
        $user_id = 0;
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
             $user_id =$row->user_id;
        }

        $call_destination = $this->post('call_destination');
        $call_from = $this->post('call_from');
        $call_coordinate = $this->post('call_coordinate');
        $call_duration = $this->post('call_duration');

        $return_status = true;
        $return_message = "";

        if($return_status){
            $data = array(
                "user_id"=>$user_id,
                "call_destination"=>$call_destination,
                "call_from"=>$call_from,
                "call_coordinate"=>$call_coordinate,
                "call_duration"=>$call_duration,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s")
            );

            $insert = $this->M_log->insert_log_call($data);
            if($insert){
                $return_status = true;
                $return_message = "Success";
            }else{
                $return_status = false;
                $return_message = "Failed Insert Log";
            }
        }

        $return = [
            'status' => $return_status,
            'data' => array(),
            'message' => $return_message
        ];


        $this->set_response($return, REST_Controller::HTTP_OK);
    }

    public function store_logactivity_post()
    {
        $user_id = 0;
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
             $user_id =$row->user_id;
        }

        $event_name = $this->post('event_name');
        $lat = $this->post('lat');
        $long = $this->post('long');


        $return_status = true;
        $return_message = "";

        if($return_status){
            $data = array(
                "user_id"=>$user_id,
                "created_by"=>$user_id,
                "event_name"=>$event_name,
                "lat"=>$lat,
                "long"=>$long,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s")
            );

            $insert = $this->M_log->insert_log_activity($data);
            if($insert){
                $return_status = true;
                $return_message = "Success";
            }else{
                $return_status = false;
                $return_message = "Failed Insert Log";
            }
        }

        $return = [
            'status' => $return_status,
            'data' => array(),
            'message' => $return_message
        ];


        $this->set_response($return, REST_Controller::HTTP_OK);
    }

    // public function store_logactivity_post()
    // {
    //     $user_id = 0;
    //     if (!empty($this->_args[$this->config->item('rest_key_name')])
    //         && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
    //          $user_id =$row->user_id;
    //     }
    //
    //     $event_name = $this->post('event_name');
    //
    //     $return_status = true;
    //     $return_message = "";
    //
    //     if($return_status){
    //         $data = array(
    //             "user_id"=>$user_id,
    //             "created_by"=>$user_id,
    //             "event_name"=>$event_name,
    //             "created_at"=>date("Y-m-d H:i:s"),
    //             "updated_at"=>date("Y-m-d H:i:s")
    //         );
    //
    //         $insert = $this->M_log->insert_log_activity($data);
    //         if($insert){
    //             $return_status = true;
    //             $return_message = "Success";
    //         }else{
    //             $return_status = false;
    //             $return_message = "Failed Insert Log";
    //         }
    //     }
    //
    //     $return = [
    //         'status' => $return_status,
    //         'data' => array(),
    //         'message' => $return_message
    //     ];
    //
    //
    //     $this->set_response($return, REST_Controller::HTTP_OK);
    // }



}
