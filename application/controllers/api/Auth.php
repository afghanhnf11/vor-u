<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
       $this->load->library('session');

        $this->load->model('M_users');
        $this->load->helper('string');
        $this->load->helper('url');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 200; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 100; // 50 requests per hour per user/key
    }

    public function login_post()
    {
        $email = $this->post('email');
        $password = _hash($this->post('password'));
        $authenticated = $this->M_users->login($email,$password);
        if($authenticated){
            unset($authenticated->password);
            $message = [
            'status' => true,
            'data' => $authenticated,
            'message' => 'Success'
            ];
        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Username & Password Not Match'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    // public function checkemail_post()
    // {
    //     $email = $this->post('email');
    //     // $password = _hash($this->post('password'));
    //     $authenticated = $this->M_users->chekEmail($email);
    //     if($authenticated){
    //         // unset($authenticated->password);
    //         $message = [
    //         'status' => true,
    //         'data' => $authenticated,
    //         'message' => 'Email Ada Gan'
    //         ];
    //     }else{
    //         $message = [
    //         'status' => false,
    //         'data' => array(),
    //         'message' => 'Email Tidak ada'
    //         ];
    //     }

    //     $this->set_response($message, REST_Controller::HTTP_OK);
    // }





    // public function checkemail_post()
    // {
    //     $email = $this->post('email');
    //     // $password = _hash($this->post('password'));
    //     $authenticated = $this->M_users->chekEmailId($email);
    //     if($authenticated){
    //         // unset($authenticated->password);
    //         $message = [
    //         'status' => true,
    //         'data' => $authenticated,
    //         'message' => 'Email Ada Gan'
    //         ];
    //     }else{
    //         $message = [
    //         'status' => false,
    //         'data' => array(),
    //         'message' => 'Email Tidak ada'
    //         ];
    //     }

    //     $this->set_response($message, REST_Controller::HTTP_OK);
    // }




    public function changePassword_post()
    {

        $user_id = 0;
        $user_email = '';
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {

            $user_id = $row->user_id;

            $old_password = _hash($this->post('old_password'));
            $new_password = _hash($this->post('new_password'));

            $authenticated = $this->M_users->loginById($user_id,$old_password);
            if($authenticated){
                $changePassword = $this->M_users->change_password($user_id,$new_password);
                $message = [
                'status' => true,
                'data' => $changePassword,
                'message' => 'Success'
                ];
            }else{
                $message = [
                'status' => false,
                'data' => array(),
                'message' => 'Old Password Wrong'
                ];
            }

        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Token Missmatch'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);
    }



    public function changePasswordLogin_post()
    {

        $user_id = 0;
        $user_email = '';
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {

            $user_id = $row->user_id;

            $new_password = _hash($this->post('new_password'));

            $authenticated = $this->M_users->idLogin($user_id);
            if($authenticated){
                $changePassword = $this->M_users->change_password($user_id,$new_password);
                $message = [
                'status' => true,
                'data' => $changePassword,
                'message' => 'Success'
                ];
            }else{
                $message = [
                'status' => false,
                'data' => array(),
                'message' => 'Old Password Wrong'
                ];
            }

        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Token Missmatch'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);
    }



        public function changeprofile_post()
    {

        // $user_id = 0;
        // $user_email = '';
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {

            $user_id = $row->user_id;

            $old_password = _hash($this->post('old_password'));
			   $data = array(
	            "phone_number"=>$this->post('phone_number'),
	            "nickname"=>$this->post('nickname'),
	            "name"=>$this->post('name'),
	            "gender"=>$this->post('gender'),
	            "address"=>$this->post('address'),
	            "religion"=>$this->post('religion'),
	            "nik"=>$this->post('nik'),
		        "no_kk"=>$this->post('no_kk'),
		        "nama_ktp"=>$this->post('nama_ktp'),
		        "status_kel"=>$this->post('status_kel'),
		        "Prop_name"=>$this->post('Prop_name'),
		        "kab_name"=>$this->post('kab_name'),
		        "kec_name"=>$this->post('kec_name'),
		        "kel_name"=>$this->post('kel_name'),
		        "no_rw"=>$this->post('no_rw'),
		        "no_rt"=>$this->post('no_rt'),
		        "agama"=>$this->post('agama'),
		        "nama_ayah"=>$this->post('nama_ayah'),
		        "nama_ibu"=>$this->post('nama_ibu'),
		        "jenis_pekerjaan"=>$this->post('jenis_pekerjaan'),
		        "alamat_ktp"=>$this->post('alamat_ktp'),
		        "tempat_lahir"=>$this->post('tempat_lahir'),
		        "pendidikan_akhir"=>$this->post('pendidikan_akhir'),
		        "status_kawin"=>$this->post('status_kawin'),
		        "gol_darah"=>$this->post('gol_darah'),
	            "created_at"=>date("Y-m-d H:i:s"),
	            "updated_at"=>date("Y-m-d H:i:s")
	        );
            // $new_password = _hash($this->post('new_password'));

     	   $authenticated = $this->M_users->chekid($user_id);

            // $authenticated = $this->M_users->loginById($user_id,$old_password);
            if($authenticated){
                $changePassword = $this->M_users->change_profile($user_id,$data);
                $authupdate = $this->M_users->chekid($user_id);

                $message = [
                'status' => true,
                'data' => $authupdate,
                'message' => 'Success Update Profile'
                ];
            }else{
                $message = [
                'status' => false,
                'data' => array(),
                'message' => 'Gagal Update'
                ];
            }

        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Token Missmatch'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);
    }



    public function call_nik_post()
      {
          $call_nik = array();
          $call_nik = array(
              "nik"=>$this->post('nik'),
              "user_id"=> '111201901281andri',
              "password"=> '12345',
              "ip_user"=>'192.168.19.9'
          );


           $message = array();
           if(!empty($call_nik)){
              //API URL
           $url = 'http://172.16.160.43:8080/dukcapil/get_json/32-76/diskominfo_3276/call_nik';
           //create a new cURL resource
           $ch = curl_init($url);
           //setup request to send json via POST

           $nikload = json_encode($call_nik);
           //attach encoded JSON string to the POST fields
           curl_setopt($ch, CURLOPT_POSTFIELDS, $nikload);
           //set the content type to application/json
           curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
           //return response instead of outputting
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           //execute the POST request
           $result = curl_exec($ch);
           //close cURL resource
           curl_close($ch);
           //Output response
           // echo "<pre>$result</pre>";
           //get response
           // $call_nik = json_decode(file_get_contents('php://input'), true);
           //output response
           // echo '<pre>'.$call_nik.'</pre>';
           $message = json_decode($result);
           if (isset($message ->content[0]->RESPONSE_DESC)) {
              $message->status = false;
           }else{
              $message->status = true;
           }
          }else{
              $message = [
              'status' => false,
              'data' => array(),
              'message' => 'nik kosong.'
              ];
          }


          $this->set_response($message, REST_Controller::HTTP_OK);
      }


//     public function keyemail_post()
//     {
        
//            $email = $this->post('email');
       
   
//         // $password = _hash($this->post('password'));
//         $authenticated = $this->M_users->chekEmailId($email);
//         $user = $this->M_users->chekEmailId($email);

//         $data = array();

//         $data = array(
//             "group_id"=>'0',
//             "keyauth"=>random_string('alnum', 5)
            
//         );

//         if($authenticated){
//           $insertToken = $this->M_users->change_keyauth($email,$data);
//            $key['keyauth'] = $this->M_users->chekEmailId($email);


//     $noreply = 'no-reply@smart-city.id';
// //    $receiver_email = 'admin@smart-city.id';

//         // $receiver_email = $this->input->post('email');

//     $receiver_email = 'marketing@smart-city.id';
//       $emailsend = $email;
//         // $receiver_email = $this->input->post('email');
//         // $username = $this->input->post('name');
//         // $phone = $this->input->post('phone');

//         // $subject = $this->input->post('subject');
//         $subject = 'Reset Password';
//         $sender_message = $key['keyauth'] ;
//           $message = "Pastikan Kode ini jangan tersebar ke orang lain ! <br> Kode untuk Mereset Password anda : <br> <h3> " .$data['keyauth']. "</h3>"  ;
//         // $message = "Pastikan Kode ini jangan tersebar ke orang lain ! <br> Kode untuk Mereset Password anda :<br> 
//         //             <br> ""<h3>".$data['keyauth']"</h3>";


//         $ci = get_instance();
//             $ci->load->library('email');
//             $config['protocol']  = "smtp";
//             $config['smtp_host'] = "ssl://smtp.gmail.com";
//             $config['smtp_port'] = "465";
//             $config['smtp_user'] = "aldydhika14@gmail.com";
//             $config['smtp_pass'] = "100396icha";
//             // $config['smtp_host'] = "ssl://mail.smart-city.id";
//             // $config['smtp_port'] = "465";
//             // $config['smtp_user'] = "admin@smart-city.id";
//             // $config['smtp_pass'] = "XR-wntrr?MoO";
//             $config['charset']   = "utf-8";
//             $config['mailtype']  = "html";
//             $config['newline']   = "\r\n";
//             $ci->email->initialize($config);
//             $ci->email->from($noreply, $noreply);
//             $list = array($receiver_email , $emailsend , 'aldydhika14@gmail.com', 'rendygl@ciptadrasoft.com');
//             $ci->email->to($list);
//             // $ci->email->to($emailsend);
//             $ci->email->subject($subject);
//             $ci->email->message($message);
//              $this->email->send();




//            // $insertToken = $this->M_users->keyauth($data);
//            $authenticated2 = $this->M_users->chekEmailId($email);

//             $message = [
//             'status' => true,
//             'data' => $authenticated2,
//             'message' => 'Email Ada Gan'
//             ];
//         }else{
//             $message = [
//             'status' => false,
//             'data' => array(),
//             'message' => 'Email Tidak ada'
//             ];
//         }

//         $this->set_response($message, REST_Controller::HTTP_OK);



// }



    public function keyemail_post()
    {
        
           $email = $this->post('email');
           $merkhp = $this->post('merkhp');
           $lokasititik = $this->post('lokasititik');


         
             if($lokasititik == 'Lokasi tidak terdeteksi'){
            $lokasititik = 'Depok, West Java' ;
          }else if($lokasititik == $lokasititik ){
            $lokasititik = $lokasititik;
          }

   
        // $password = _hash($this->post('password'));
        $authenticated = $this->M_users->chekEmail($email);
        $gettemp = $this->M_users->gettemp('1');

        $user = $this->M_users->chekEmail($email);

        $data = array();

        $data = array(
            "group_id"=>'0',
            "keyauth"=>random_string('alnum', 5)
            
        );

        if($authenticated){
          $insertToken = $this->M_users->change_keyauth($email,$data);
           $key['keyauth'] = $this->M_users->chekEmail($email);


    $noreply = 'no-reply@smart-city.id';
//    $receiver_email = 'admin@smart-city.id';

        // $receiver_email = $this->input->post('email');

    $receiver_email = 'admin@smart-city.id';
      $emailsend = $email;
        // $receiver_email = $this->input->post('email');
        // $username = $this->input->post('name');
        // $phone = $this->input->post('phone');

        // $subject = $this->input->post('subject');
     $subject = "Permintaan Perubahan Passoword [" .$data['keyauth']. "] "  ;

        // $subject = 'Reset Password - ';
        $sender_message = $key['keyauth'] ;
          // $message = "Pastikan Kode ini jangan tersebar ke orang lain ! <br> Kode untuk Mereset Password anda : <br> <h3> " .$data['keyauth']. "</h3>"  ;

//           $message = "<h3 style='text-align: center;'><strong>Ubah Password Akun Depok Single Window</strong></h3>
// <h3 style='text-align: center;'><strong><img src='https://dsw.depok.go.id/assets1/images/logo_login.png' alt='' width='278' height='117'><br></strong></h3>
// <h3>&nbsp;</h3>
// <h3><span class='example1'><strong>Terlampir Email dan Kode anda :</strong></span></h3>
// <div>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>".$email."</strong></div>
// <div>Kode Auth&nbsp; &nbsp; &nbsp;: <strong>".$data['keyauth']. "</strong></div>
// <div>Masukan Kode Auth di atas di dalam Aplikasi DSW ( Depok Single Window ) ,Dan pastikan tidak menyebar luaskan Kode Auth anda ke orang lain !</div><div><br></div>
// <div><strong>Smartcity Kota Depok</strong></div>
// <div><strong><img src='http://103.113.31.29/smartcity-dev/img/logo_menuju_smartcity.png' alt='' width='256' height='51'></strong></div>
// <div><strong>Unggul, Nyaman&nbsp; dan Relegius</strong></div>"  ;

         $message = '<table style="background-color: #f5f8fa; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F5F8FA">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table id="m_410050702250248573header" style="width: 448px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr>
<td style="min-width: 448px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;"><img class="CToWUd" style="min-width: 448px; height: 1px; margin: 0; padding: 0; display: block; border: none; outline: none;" src="https://ci4.googleusercontent.com/proxy/olz0jOQiwOKs810-_40TL7hjkyoDHhAfffrsDfXjBi0eH9ZPtuN9ozbJ4qp_3K3T216fZnFIGE-W22oQ9ZkivsZ3VC01TZckhPfMCfFHO9lUw_hkgTJi=s0-d-e1-ft#https://ea.twimg.com/email/self_serve/media/spacer-1402696023930.png" alt="" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table id="m_410050702250248573header" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr>
<td style="height: 24px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="4" height="24">&nbsp;</td>
</tr>
<tr align="right">
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="right"><a style="text-decoration: none; border-style: none; border: 0; padding: 0; margin: 0;" > <img class="CToWUd" style="width: 177px; margin: 0px; padding: 0px; display: block; border: none; outline: none;" src="https://lh3.googleusercontent.com/5OB5CP-Hseg9bk1tEUUij89ZqUKV6PlIg_zgBq-K2oMlq8kL1E1lnA4mUMrDIoqweR8aCFQB4wrpDXV7gyCRz2Ozq7dyxtWU3Ft4zfWn7lkLGPjDXRVQ8LdhIvGpZTFYF9eJ5cFjWMl7lN-theBJuGBk3bqbFiQQEz-ySDJVflWhTBoNxv-K9UDCx7-4RTriu-g_oNIF9XmiMrMh4HLfrDRWB9ngbRWunhQVTNE01JEPC8KIsj5PqytOrtaH5TuCo5PQ0Nw5nXLGzKukNT7_ytXP_q-m_OOFdo0NXYz9qGPp9OAyT-tRoORmB-GMoR7BF_Cdy65C5non-IYQRboXTqjOUDzbH8HsDSvKRXJChZRDRN3EjMtYnXyXBYXQW7K3GovxrMAQK-rBIUspCLJEkh9KKKd8frl8DKQEouEYb52PuNT3J3a0JL2ptdbwbVF2IO5fTvRdrkciwgS6LCyHWKHP7OEZlnqGLubHhUIks5Yk_RL8KNIX4AI2GtVeRY1XjiXbUHuaVtw2WSiUE8VifafvRd5sylABudPENqAz8_iVbRv0e3YESBs3sfCbQhHrvDxvYWxL4AAyAacmv0JcekfiTC54NhxRgi4ZBUWmDrEy86h8aTLXxqZUZgEWc_Fbso5QFDXgjWCv1iqdpFVVmkXcGHHwKwVO1lMDCifnqyJw1zbT=w620-h220-no" alt="" width="60" height="61" align="right" /> </a></td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
<tr>
<td style="height: 24px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="3" height="24"><img class="CToWUd" style="display: block; margin: 0; padding: 0; border: none; outline: none;" src="https://ci3.googleusercontent.com/proxy/1xMgbAhYM38k7OyDJcffttUiCUFAtTF9GAuLGT9YLbzkvuQT1rpb8Bjz3JOlbCVAsHAxYDvZsitvDqQ0ZdLVO1N695_pXH8XYqnGCj6dXpS9mxAht0RQJF36Y__Q78_0XGGCEfMoXXEOOzdhyyBTAUaoKPhJMXx2oZJv2cUJ2C-h0sAx9xOTTMKMRkie81_5I_E=s0-d-e1-ft#https://twitter.com/scribe/ibis?t=1&amp;cn=cGFzc3dvcmRfcmVzZXRfdjI%3D&amp;iid=b9f301fbcb5e45baafb033f98edd2efd&amp;uid=110085036&amp;nid=248+20" alt="" width="1" height="1" /></td>
</tr>
</tbody>
</table>
<table id="m_410050702250248573header" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr align="left;">
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="left;">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 32px; font-size: 24px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: bold; color: #292f33; text-align: left; text-decoration: none;" align="left;">Permintaan Perubahan Password</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="12">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0px; margin: 0px; line-height: 20px; font-size: 16px; font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-weight: 400; color: #292f33; text-decoration: none; text-align: justify;" align="left;">Halo '.$user->name.', kami menerima permintaan perubahan password anda. Masukan kode di bawah ini pada aplikasi Depok Single Window (DSW).</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="24">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="left;">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;">
<table style="padding: 0px; margin: 0px; line-height: 1px; font-size: 1px; height: 34px;" border="0" width="383" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 12px; font-size: 1px; border-radius: 4px;" align="center" bgcolor="#1DA1F2"><a style="text-decoration: none; border-style: none; border: 1px solid #1da1f2; padding: 8px 17px; margin: 0; font-size: 24px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; color: #ffffff; border-radius: 4px; display: inline-block; font-weight: bold;"> '.$data['keyauth'].' </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="height: 36px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="36">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 20px; font-size: 16px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; color: #292f33; text-align: left; text-decoration: none;" align="left;">
<p style="text-align: justify;">Pastikan anda tidak memberitahukan Kode Rahasia di atas kepada siapapun.</p>
<p style="text-align: justify;">Permintaan ini di lakukan melalui :</p>
<p><strong>Perankat : '.$merkhp.'</strong></p>
<p><strong>Lokasi&nbsp; &nbsp; &nbsp;: '.$lokasititik.'&nbsp;</strong></p>
<p>____________________________________________</p>
<p style="text-align: justify;"><strong>Perhatian : Jika Anda Merasa Tidak Mengirim Permintaan Ini</strong></p>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 20px; font-size: 16px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; color: #292f33; text-align: left; text-decoration: none;" align="left;">
<p>Silahkan menghubungi :</p>
<p><strong><a href="https://helpdesk.depok.go.id/">Dinas Komunikasi dan Informatika Kota Depok</a></strong></p>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="36">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
<tr>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px; background-color: #ffffff; border-radius: 5px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="3" height="24">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px; background-color: #ffffff; border-radius: 5px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#F5F8FA">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="left">
<table style="padding: 0px; margin: 0px; line-height: 1px; font-size: 1px; height: 118px;" border="0" width="365" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 22px; font-size: 16px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; text-align: left; color: #8899a6;"><span style="color: #000000;"><strong>Team Smartcity Kota Depok</strong></span></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 19px; font-size: 14px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; text-align: left; color: #8899a6;"><img src="https://ci5.googleusercontent.com/proxy/5fhETvrvh77G5Pzp_Gw77PiLgUMdLhc3_fof__mX4exNwOJjG3gxSf5K62OCngqwFWJ7FM3jw-T6JBqyZJjlnDvT7K5h3jWYpZy2a3-a07iONKU=s0-d-e1-ft#https://lh3.googleusercontent.com/WtHPdruQ-owXiIDOF7JljEx-wQjMPNeNp2R2dj3i6Wm95lZuIf6ayRyYHYXpRU2-2AGgm1tQDuiWPOfTsMdS10wUDBGsKTi1G9pU6K7XVvaNnikOsFtH96AVrYUW7APo6dON0_IT26QWVJ0RmBEqctPkye7zrGhh0YPl5cqkddzhkzdlrfecPGpBlKIFMeyJNQohEJorZEnXOb-WNCgBnvXBdQc3M_wYjafElsSOIlMiHfj3TWaoOjAR961JTsfROkGJS0uCX9FzWPHOinaUounCn2WFl1lW8olZQO8gu1ZSZ5q6pl4iIyoDhr10OtwR0FBHfxdwuHGTXIBid1xQ7tSX4xC51BtTvam-27UdsTiB1LL6vCmlhKdZddetH12YPvE_sJ0zeYCImWJT2LCT16QIt3jsp2ym0K7tcx9sBkb9RtZWewfZU8aNUjWFAaif82UobFfRBFQNklB21QmwYBdpcwsiklwEozUQLcNQbCcYSVDUWW13F7CHjYdhirXOv8WQTVyIN8DkIdhZ7bOQAiIl7uAiTExWas33JOVfHJcLddu05ESGSnehs947eHaUs3kOhIKjnsSjEIuhjOvvLK22T4osdrmzIkpvf2NT8iLbqJXc6czfIrlSLaFnb7wRqlikji_gfSC13xVMkutkiLy5ViOSx2Tdt9UFKswEHzb-FZYK=w1361-h271-no" alt="" width="256" height="51" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="m_410050702250248573footer" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="height: 36px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="36">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><strong><span style="font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; color: #000000; text-align: left; text-decoration: none;">UNGGUL&nbsp; |&nbsp; NYAMAN&nbsp; |&nbsp;&nbsp;RELIGIUS</span></strong></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><span style="font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; font-weight: 400; color: #333333; text-align: left; text-decoration: none;"> This email was meant for '.$user->email.' </span></td>
</tr>
<tr>
<td style="height: 6px; line-height: 1px; font-size: 1px; padding: 0; margin: 0;" height="6"><span style="color: #333333;">&nbsp;</span></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><span style="color: #333333;"><a style="text-decoration: none; border: 0px; padding: 0px; margin: 0px; font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; color: #333333; font-size: 12px; font-weight: normal; line-height: 12px;" >DSW, Inc.&nbsp;2017 Jl. Margonda Raya - Kantor Walikota Depok</a></span></td>
</tr>
<tr>
<td style="height: 72px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="72">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>'  ;



         // $pesan = str_replace($gettemp->layout);
      
      // var_dump($pesan);

        $ci = get_instance();
            $ci->load->library('email');
            $config['protocol']  = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "smartcitydepok@gmail.com";
            $config['smtp_pass'] = "smartcity@D3p0k";
            $config['SMTPSecure'] ="ssl";
            $config['SMTPAuth'] = true;
            // $config['smtp_host'] = "ssl://mail.smart-city.id";
            // $config['smtp_port'] = "465";
            // $config['smtp_user'] = "admin@smart-city.id";
            // $config['smtp_pass'] = "XR-wntrr?MoO";
            $config['charset']   = "utf-8";
            $config['mailtype']  = "html";
            $config['newline']   = "\r\n";
            $ci->email->initialize($config);
            $ci->email->from($noreply, $noreply);
            $list = array($receiver_email , $emailsend , 'smartcitydepok@gmail.com'  );
            $ci->email->to($list);
            // $ci->email->to($emailsend);
            $ci->email->subject($subject);
            $ci->email->message($message);
             $this->email->send();




           // $insertToken = $this->M_users->keyauth($data);
           $authenticated2 = $this->M_users->chekEmail($email);

            $message = [
            'status' => true,
            'data' => $authenticated2,
            'message' => 'Email Ada Gan'
            ];
        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Email Tidak ada'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);



}



    public function checkemail_post()
    {
        $email = $this->post('email');
        // $password = _hash($this->post('password'));
        $authenticated = $this->M_users->getIdTok($email);
        $auth2 = $this->M_users->chekEmail($email);

        // var_dump($authenticated2);
        $authid = $this->M_users->getId($email);
// var_dump($authid);
        $tokenid = $this->M_users->getIdkey($authid->id);

        $authid2 = $authid->id;
// var_dump($authid2);
        

        if(!$tokenid){

        	$dataToken = array(
                    "key"=>substr(_hash($authenticated->email.$authenticated->name), 0, config_item('rest_key_length')),
                    "user_id"=>$authid2
                );

              $insertToken = $this->M_users->addToken($dataToken);
              $auth3 = $this->M_users->chekEmail($email);

   	     
            $message = [
            'status' => false,
            'data' => $auth3,
            'message' => 'Sukses'
            ];
        }else{
            $message = [
            'status' => true,
            'data' => $auth2,
            'message' => 'Token Ready!'
            ];
        }
        // var_dump($message);
        $this->set_response($message, REST_Controller::HTTP_OK);
    }


  public function login2_post()
    {
        date_default_timezone_set("Asia/Jakarta");
        $time_ini = date('Y/m/d H:i:s');
        $time = date('YmdHis');



        $email = $this->post('email');
        $merkhp = $this->post('merkhp');
        $lokasititik = $this->post('lokasititik');
        $password = _hash($this->post('password'));
        $authenticated = $this->M_users->login($email,$password);

         if($merkhp == null){
            $merkhp = 'Mobile Apps';
          }else{
            $merkhp();
          }

         if($lokasititik == null){
            $lokasititik = 'Depok, Jawa Barat' ;
          }else if($lokasititik == $lokasititik ){
            $lokasititik = $lokasititik;
          }


        if($authenticated){
            unset($authenticated->password);


 $noreply = 'no-reply@smart-city.id';


    $receiver_email = 'marketing@smart-city.id';
      $emailsend = $email;
     $subject = "Berhasil Login [DSW-$time]"  ;

        $sender_message = $key['keyauth'] ;
          // $message = "Pastikan Kode ini jangan tersebar ke orang lain ! <br> Kode untuk Mereset Password anda : <br> <h3> " .$data['keyauth']. "</h3>"  ;

//           $message = "<h3 style='text-align: center;'><strong>Ubah Password Akun Depok Single Window</strong></h3>
// <h3 style='text-align: center;'><strong><img src='https://dsw.depok.go.id/assets1/images/logo_login.png' alt='' width='278' height='117'><br></strong></h3>
// <h3>&nbsp;</h3>
// <h3><span class='example1'><strong>Terlampir Email dan Kode anda :</strong></span></h3>
// <div>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>".$email."</strong></div>
// <div>Kode Auth&nbsp; &nbsp; &nbsp;: <strong>".$data['keyauth']. "</strong></div>
// <div>Masukan Kode Auth di atas di dalam Aplikasi DSW ( Depok Single Window ) ,Dan pastikan tidak menyebar luaskan Kode Auth anda ke orang lain !</div><div><br></div>
// <div><strong>Smartcity Kota Depok</strong></div>
// <div><strong><img src='http://103.113.31.29/smartcity-dev/img/logo_menuju_smartcity.png' alt='' width='256' height='51'></strong></div>
// <div><strong>Unggul, Nyaman&nbsp; dan Relegius</strong></div>"  ;

         $message = '<table style="background-color: #f5f8fa; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F5F8FA">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table id="m_410050702250248573header" style="width: 448px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr>
<td style="min-width: 448px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;"><img class="CToWUd" style="min-width: 448px; height: 1px; margin: 0; padding: 0; display: block; border: none; outline: none;" src="https://ci4.googleusercontent.com/proxy/olz0jOQiwOKs810-_40TL7hjkyoDHhAfffrsDfXjBi0eH9ZPtuN9ozbJ4qp_3K3T216fZnFIGE-W22oQ9ZkivsZ3VC01TZckhPfMCfFHO9lUw_hkgTJi=s0-d-e1-ft#https://ea.twimg.com/email/self_serve/media/spacer-1402696023930.png" alt="" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table id="m_410050702250248573header" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr>
<td style="height: 24px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="4" height="24">&nbsp;</td>
</tr>
<tr align="right">
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="right">&nbsp;&nbsp;<a style="text-decoration: none; border-style: none; border: 0; padding: 0; margin: 0;" href="https://play.google.com/store/apps/details?id=id.depok.depoksinglewindow&amp;hl=in"> <img class="CToWUd" style="width: 177px; margin: 0px; padding: 0px; display: block; border: none; outline: none;" src="https://dsw.depok.go.id/assets1/images/dswnew2.png" alt="" width="60" height="61" align="right" /> </a></td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</td>
</tr>
<tr align="right">
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="right">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
<tr>
<td style="height: 24px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="3" height="24"><img class="CToWUd" style="display: block; margin: 0; padding: 0; border: none; outline: none;" src="https://ci3.googleusercontent.com/proxy/1xMgbAhYM38k7OyDJcffttUiCUFAtTF9GAuLGT9YLbzkvuQT1rpb8Bjz3JOlbCVAsHAxYDvZsitvDqQ0ZdLVO1N695_pXH8XYqnGCj6dXpS9mxAht0RQJF36Y__Q78_0XGGCEfMoXXEOOzdhyyBTAUaoKPhJMXx2oZJv2cUJ2C-h0sAx9xOTTMKMRkie81_5I_E=s0-d-e1-ft#https://twitter.com/scribe/ibis?t=1&amp;cn=cGFzc3dvcmRfcmVzZXRfdjI%3D&amp;iid=b9f301fbcb5e45baafb033f98edd2efd&amp;uid=110085036&amp;nid=248+20" alt="" width="1" height="1" /></td>
</tr>
</tbody>
</table>
<table id="m_410050702250248573header" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody>
<tr align="left;">
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="left;">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0px; margin: 0px; line-height: 32px; font-size: 24px; font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-weight: bold; color: #292f33; text-decoration: none; text-align: center;" align="left;">Masuk&nbsp;Akun Berhasil</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="12">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0px; margin: 0px; line-height: 20px; font-size: 16px; font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-weight: 400; color: #292f33; text-decoration: none; text-align: justify;" align="left;">
<p>Halo '.$authenticated->name.', Seseorang berhasil masuk ke akun anda, melalui :</p>
<p>Perangkat&nbsp; &nbsp; :&nbsp; '.$merkhp.'</p>
<p>Lokasi&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : '.$lokasititik.'</p>
<p>Waktu&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : '.$time_ini.'</p>
<p>Jika tidak merasa masuk akun melalui perangkat di atas , harap menganti password anda. Terima Kasih&nbsp;</p>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 20px; font-size: 16px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; color: #292f33; text-align: left; text-decoration: none;" align="left;">
<p><strong>Perhatian :&nbsp;Jika anda mengalami gangguan login pada Aplikasi, silahkah Gunakan fitur "Lupa Password" pada Aplikasi DSW</strong></p>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 20px; font-size: 16px; font-family: HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; color: #292f33; text-align: left; text-decoration: none;" align="left;">
<p>Informasi Lebih Lanjut Silahkan Menghubungi :</p>
<p><strong><a>Dinas Komunikasi dan Informatika Kota Depok</a></strong></p>
</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="36">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
<tr>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px; background-color: #ffffff; border-radius: 5px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="3" height="24">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">
<table style="padding: 0; margin: 0; line-height: 1px; font-size: 1px; background-color: #ffffff; border-radius: 5px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#F5F8FA">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="left">
<table style="padding: 0px; margin: 0px; line-height: 1px; font-size: 1px; height: 118px;" border="0" width="365" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; margin: 0; line-height: 22px; font-size: 16px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; text-align: left; color: #8899a6;"><span style="color: #000000;"><strong>Team Smartcity Kota Depok</strong></span></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 19px; font-size: 14px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-weight: 400; text-align: left; color: #8899a6;"><img src="https://ci5.googleusercontent.com/proxy/5fhETvrvh77G5Pzp_Gw77PiLgUMdLhc3_fof__mX4exNwOJjG3gxSf5K62OCngqwFWJ7FM3jw-T6JBqyZJjlnDvT7K5h3jWYpZy2a3-a07iONKU=s0-d-e1-ft#http://103.113.31.29/smartcity-dev/img/logo_menuju_smartcity.png" alt="" width="206" height="41" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" width="24">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" colspan="3" height="24">&nbsp;</td>
</tr>
<tr>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center">&nbsp;</td>
<td style="line-height: 1px; display: block; height: 1px; background-color: #f5f8fa; padding: 0; margin: 0; font-size: 1px;" height="1">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="m_410050702250248573footer" style="width: 448px; background-color: #ffffff; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" border="0" width="448" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="height: 36px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="36">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><strong><span style="font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; color: #000000; text-align: left; text-decoration: none;">UNGGUL&nbsp; |&nbsp; NYAMAN&nbsp; |&nbsp;&nbsp;RELIGIUS</span></strong></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><span style="font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; font-weight: 400; color: #333333; text-align: left; text-decoration: none;"> This email was meant for '.$user->email.' </span></td>
</tr>
<tr>
<td style="height: 6px; line-height: 1px; font-size: 1px; padding: 0; margin: 0;" height="6"><span style="color: #333333;">&nbsp;</span></td>
</tr>
<tr>
<td style="padding: 0; margin: 0; line-height: 1px; font-size: 1px;" align="center"><span style="color: #333333;"><a style="text-decoration: none; border: 0px; padding: 0px; margin: 0px; font-family: HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif; color: #333333; font-size: 12px; font-weight: normal; line-height: 12px;" href="https://www.google.co.id/maps/place/Kantor+Walikota+Depok/@-6.3944475,106.8213664,17z/data=!4m5!3m4!1s0x2e69ebe3bdf34d09:0x57e78d4e0c142a20!8m2!3d-6.3944475!4d106.8235551">DSW, Inc.&nbsp;2017 Jl. Margonda Raya - Kantor Walikota Depok</a></span></td>
</tr>
<tr>
<td style="height: 72px; padding: 0; margin: 0; line-height: 1px; font-size: 1px;" height="72">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>'  ;



         // $pesan = str_replace($gettemp->layout);
      
      // var_dump($pesan);

        $ci = get_instance();
            $ci->load->library('email');
            $config['protocol']  = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "smartcitydepok@gmail.com";
            $config['smtp_pass'] = "smartcity@D3p0k";
            // $config['smtp_host'] = "ssl://mail.smart-city.id";
            // $config['smtp_port'] = "465";
            // $config['smtp_user'] = "admin@smart-city.id";
            // $config['smtp_pass'] = "XR-wntrr?MoO";
            $config['charset']   = "utf-8";
            $config['mailtype']  = "html";
            $config['newline']   = "\r\n";
            $ci->email->initialize($config);
            $ci->email->from($noreply, $noreply);
            $list = array($receiver_email , $emailsend , 'aldydhika14@gmail.com', 'rendygl@ciptadrasoft.com');
            $ci->email->to($list);
            // $ci->email->to($emailsend);
            $ci->email->subject($subject);
            $ci->email->message($message);
             $this->email->send();


            $message = [
            'status' => true,
            'data' => $authenticated,
            'message' => 'Success'
            ];
        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Username & Password Not Match'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK);
    }


    public function register_post()
    {


        $data = array();
        $data = array(
            "group_id" => 0,
            "email"=>$this->post('email'),
            "password"=>_hash($this->post('password')),
            "phone_number"=>$this->post('phone_number'),
            "is_depok_citizen"=>$this->post('is_depok_citizen'),
            "nickname"=>$this->post('nickname'),
            "name"=>$this->post('name'),
            "birthplace"=>$this->post('birthplace'),
            "birthdate"=>$this->post('birthdate'),
            "gender"=>$this->post('gender'),
            "address"=>$this->post('address'),
            "zipcode"=>$this->post('zipcode'),
            "province_id"=>$this->post('province_id'),
            "kab_id"=>$this->post('kab_id'),
            "kec_id"=>$this->post('kec_id'),
            "kel_id"=>$this->post('kel_id'),
            "religion"=>$this->post('religion'),
            "marital_status"=>$this->post('marital_status'),
            "created_at"=>date("Y-m-d H:i:s"),
            "updated_at"=>date("Y-m-d H:i:s")
        );
        $this->db->trans_start();

        $isEmailExist = $this->M_users->checkEmail($this->post('email'));
        if(!$isEmailExist){
             $insert = $this->M_users->register($data);
            if($insert){
                $dataToken = array(
                    "key"=>substr(_hash($insert), 0, config_item('rest_key_length')),
                    "user_id"=>$insert
                );
                $insertToken = $this->M_users->addToken($dataToken);

                if($insertToken){
                    $this->db->trans_complete();

                    unset($data['password']);
                    $data['token'] =substr(_hash($insert), 0, config_item('rest_key_length'));

                    $message = [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Success'
                    ];
                }else{
                     $message = [
                    'status' => false,
                    'data' => $data,
                    'message' => 'Failed Insert to Token'
                    ];
                }

            }else{
                $message = [
                'status' => false,
                'data' => array(),
                'message' => 'Failed Insert to Users'
                ];
            }
        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Email is Exist'
            ];
        }

        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }


    public function log_post()
    {
        // $email = $this->post('email');
        $password = 'aa';
        // $authenticated = $this->M_users->login($email,$password);
        // if($email){
        //     unset($email);
        //     $message = [
        //     'status' => true,
        //     'data' => $email,
        //     'message' => 'Success'
        //     ];
        // }else{
        //     $message = [
        //     'status' => false,
        //     'data' => array(),
        //     'message' => 'Username & Password Not Match'
        //     ];
        // }

        $this->set_response($password, REST_Controller::HTTP_OK);
    }


}
