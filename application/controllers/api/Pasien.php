<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

use Restserver\Libraries\REST_Controller;

class Pasien extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
       $this->load->library('session');

        $this->load->model('M_Log');
        $this->load->helper('text');

        $this->load->helper('string');
        $this->load->helper('url');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 200; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 100; // 50 requests per hour per user/key
    }
 public function log_post()
    {
        // $email = $this->post('email');

        $data = array();
        $data = array(
            "Alamat"=>'Alamat',
            "Lokasi"=>'Alamat',
            "Lat"=>$this->post('Lat'),
            "Long"=>$this->post('Long'),
            "CreateDate"=>date("Y-m-d H:i:s"),
            "PasienId"=>$this->post('name')
        );
        $insert = $this->M_log->addLog($data);

        // var_dump($insert );
        // $authenticated = $this->M_log->login($email,$password);
        if($insert){
           
            $message = [
            'status' => true,
            'data' => $insert,
            'message' => 'Success'
            ];
        }else{
            $message = [
            'status' => false,
            'data' => array(),
            'message' => 'Username & Password Not Match'
            ];
        }
        echo json_encode($message);
        // $this->set_response($message, REST_Controller::HTTP_OK);
    }


}

