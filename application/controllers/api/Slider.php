<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Slider extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('M_slider'); 
    }

    public function getAll_get()
    {
        $sliders = $this->M_slider->getAllSliderActive();
        foreach ($sliders as $key => $value) {
            if(!empty($value->image)){
                $value->image = base_url()."assets/slider/".$value->image;
            }else{
                $value->image = base_url()."assets/slider/banner_1.jpg";
            }
        }
        $return = [
            'status' => true, 
            'data' => $sliders, 
            'message' => ""
        ];
        

        $this->set_response($return, REST_Controller::HTTP_OK);
    } 
}
