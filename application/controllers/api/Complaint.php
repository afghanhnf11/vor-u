 <?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Complaint extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('M_complaint'); 
    }

    public function complaint_post()
    {
        $user_id = 0;
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
             $user_id =$row->user_id;
        } 
 
        $name = $this->post('name');
        $email = $this->post('email');
        $phone = $this->post('phone');
        $category = $this->post('category');
        $message = $this->post('message');
        $file = $this->post('file');
        $image = $this->post('image');
        $user_coordinate_lat = $this->post('user_coordinate_lat');
        $user_coordinate_long = $this->post('user_coordinate_long'); 
        $insident_coordinate_lat = $this->post('insident_coordinate_lat'); 
        $insident_coordinate_long = $this->post('insident_coordinate_long'); 

        $file_name = "";
        $image_name = ""; 

        $return_status = true;
        $return_message = "";
        if(!empty($_FILES['file'])){

            $dir = './assets/pengaduan';
            if (!is_dir($dir)) {
                if(!mkdir($dir, 0777, true)){
                    $return_status = false;
                    $return_message = "";
                }
            } 
   
            $config['upload_path']          = './assets/pengaduan';
            $config['allowed_types']        = '*'; 
            $types = explode("/", $_FILES['file']['type']);
            $config['file_name'] = time().".".$types[1];

            $this->load->library('upload',$config); 
            $this->upload->initialize($config);

            if (!$this->upload->do_upload("file")){  
                $return_status = false;
                $return_message = $this->upload->display_errors(); 
            }else{
                $file_name = $config['file_name'];
            }
        }

        if(!empty($_FILES['image'])){
            $dir = './assets/pengaduan';
            if (!is_dir($dir)) {
               if(!mkdir($dir, 0777, true)){
                    $return_status = false;
                    $return_message = "";
               }
            } 
 
            $config_image['upload_path']          = './assets/pengaduan';
            $config_image['allowed_types']        = '*'; 
            $types_image = explode("/", $_FILES['image']['type']);
            $config_image['file_name'] = time().".".$types_image[1];

            $this->load->library('upload',$config_image); 
            $this->upload->initialize($config_image);

            if (!$this->upload->do_upload("image")){   
                $return_status = false;
                $return_message = $this->upload->display_errors(); 
            }else{
                $image_name = $config_image['file_name'];
            }
        }

       
        if($return_status){
            $data = array(
                "name"=>$name,
                "email"=>$email,
                "phone"=>$phone,
                "category"=>$category,
                "message"=>$message,
                "file"=>$file_name,
                "image"=>$image_name,
                "user_coordinate_lat"=>$user_coordinate_lat,
                "user_coordinate_long"=>$user_coordinate_long,
                "insident_coordinate_lat"=>$insident_coordinate_lat,
                "insident_coordinate_long"=>$insident_coordinate_long,
                "status"=>"New",
                "created_by"=>$user_id, 
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s")
            );

            $insertComplaint = $this->M_complaint->insert($data);
            if($insertComplaint){ 
                $return_status = true;
                $return_message = "Success"; 
            }else{
                $return_status = false;
                $return_message = "Failed Insert Complaint"; 
            }
        }

        $return = [
            'status' => $return_status, 
            'data' => array(), 
            'message' => $return_message
        ];
        

        $this->set_response($return, REST_Controller::HTTP_OK);
    } 

     //mengambil data kategori pengaduan
    public function getComplaintCategory_get()
    {
        
        $complaintCategories = $this->M_complaint->getComplaintCategories(); 
        $return = [
            'status' => true, 
            'data' => $complaintCategories, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 


    public function getQuestionCategory_get()
    {
        
        $complaintCategories = $this->M_complaint->getQuestionCategories(); 
        $return = [
            'status' => true, 
            'data' => $complaintCategories, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 

    public function getComplaint_get(){
        $user_id = 0;
        $complaints = array();
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
             $user_id =$row->user_id;

            $complaintCategories = $this->M_complaint->getComplaintCategories(); 
            $categories = array();
            foreach($complaintCategories as $key => $value) {
               array_push($categories, $value->id);
            }

            $complaints = $this->M_complaint->getComplaintByUserId($user_id,$categories); 
            foreach ($complaints as $key => $value) {
                if(!empty($value->image)){
                    $value->image = base_url()."/assets/pengaduan/".$value->image;
                }

                if(!empty($value->file)){
                    $value->file = base_url()."/assets/pengaduan/".$value->file;
                }
            }
        } 

        $return = [
            'status' => true, 
            'data' => $complaints, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);

    }

    public function getAspiration_get(){
       $user_id = 0;
        $complaints = array();
        if (!empty($this->_args[$this->config->item('rest_key_name')])
            && $row = $this->rest->db->where('key', $this->_args[$this->config->item('rest_key_name')])->get($this->config->item('rest_keys_table'))->row()) {
             $user_id =$row->user_id;

            $complaintCategories = $this->M_complaint->getQuestionCategories(); 
            $categories = array();
            foreach($complaintCategories as $key => $value) {
               array_push($categories, $value->id);
            }

            $complaints = $this->M_complaint->getComplaintByUserId($user_id,$categories); 
            foreach ($complaints as $key => $value) {
                if(!empty($value->image)){
                    $value->image = base_url()."/assets/pengaduan/".$value->image;
                }

                if(!empty($value->file)){
                    $value->file = base_url()."/assets/pengaduan/".$value->file;
                }
            }
        } 

        $return = [
            'status' => true, 
            'data' => $complaints, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    }
}
