<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Country extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('M_country'); 
    }
    //mengambil data provinsi
    public function getProvince_get()
    {
        $province_id =  $this->input->get('province_id');

        $where = array();

        if(isset($province_id) && !empty($province_id)){
            $where = array("id"=>$province_id);
        }
        $provinces = $this->M_country->getProvince($where); 
        $return = [
            'status' => true, 
            'data' => $provinces, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 

    //mengambil data kabupaten
    public function getRegencies_get()
    {
        $province_id =  $this->input->get('province_id');

        $where = array();

        if(isset($province_id) && !empty($province_id)){
            $where = array("province_id"=>$province_id);
        }
        $regencies = $this->M_country->getRegencies($where); 
        $return = [
            'status' => true, 
            'data' => $regencies, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 

    //mengambil data kecamatan
    public function getDistricts_get()
    {
        $regency_id =  $this->input->get('regency_id');

        $where = array();

        if(isset($regency_id) && !empty($regency_id)){
            $where = array("regency_id"=>$regency_id);
        }
        $districts = $this->M_country->getDistricts($where); 
        $return = [
            'status' => true, 
            'data' => $districts, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 

     //mengambil data kelurahan/desa
    public function getVillages_get()
    {
        $district_id =  $this->input->get('district_id');

        $where = array();

        if(isset($district_id) && !empty($district_id)){
            $where = array("district_id"=>$district_id);
        }
        $villages = $this->M_country->getVillages($where); 
        $return = [
            'status' => true, 
            'data' => $villages, 
            'message' => ""
        ];
        
        $this->set_response($return, REST_Controller::HTTP_OK);
    } 
}
